<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Delegates
 *
 * @author Amna
 */
class Delegates extends Model {
    protected $table = 'delegates';
    protected $fillable = ['name_en', 'name_ar', 'mobile', 'phone', 'email','password',
        'status_id','address','bank_account','image','loc_lat', 'loc_long'];
    
    public function Orders() {
        return $this->hasMany(\App\Models\Orders::class, 'delegate_id', 'id');
    }
        
    public function PaymentWays() {
        return $this->hasMany(\App\Models\delegatePaymentWays::class, 'delegate_id', 'id');
    }
}
