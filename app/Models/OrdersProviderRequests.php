<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Orders
 *
 * @author Amna
 */
class OrdersProviderRequests extends Model {

    protected $table = 'orders_provider_requests';
    protected $fillable = ['order_id', 'provider_id', 'price', 'notes', 'status_id'];


    public function Provider() {
        return $this->belongsTo(\App\Models\Providers::class, 'provider_id', 'id');
    }
    
    public function Order() {
        return $this->belongsTo(\App\Models\Orders::class, 'order_id', 'id');
    }

}
