<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Items
 *
 * @author Amna
 */
class Items extends Model {
    protected $table = 'items';
    protected $fillable = ['name', 'item_order', 'item_type', 'image'];
    
    public function Type() {
        return $this->belongsTo(\App\Models\ConstantsTable::class, 'item_type', 'id');
    }

}
