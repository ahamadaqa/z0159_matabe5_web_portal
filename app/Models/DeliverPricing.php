<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of DeliverPricing
 *
 * @author Amna
 */
class DeliverPricing extends Model {
    
    protected $table = 'deliver_pricing';
    protected $fillable = ['dis_from', 'dis_to', 'price'];
}
