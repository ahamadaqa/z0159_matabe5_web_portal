<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of ProviderFiles
 *
 * @author Hp
 */
class ProviderFiles extends Model{
    protected $table = 'provider_files';
    protected $fillable = ['provider_id', 'image'];

}
