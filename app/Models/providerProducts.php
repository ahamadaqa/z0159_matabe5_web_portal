<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Admins
 *
 * @author Amna
 */
class providerProducts extends Model {
     protected $table = 'provider_products';
    protected $fillable = ['name', 'image', 'item_id', 'price', 'quantity','notes',
        'delivered_date','provider_id', 'active'];
    
    public function Item() {
        return $this->belongsTo(\App\Models\Items::class, 'item_id', 'id');
    }
    
    public function Provider() {
        return $this->belongsTo(\App\Models\Providers::class, 'provider_id', 'id');
    }
    
    
    
}
