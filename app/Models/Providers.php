<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Providers
 *
 * @author Amna
 */
class Providers extends Model {
    protected $table = 'providers';
    protected $fillable = ['name', 'idno', 'mobile', 'phone', 'email','password',
        'status_id','address','bank_account','image','loc_lat', 'loc_long', 'country_id', 'city_id', 'type',
        'gender', 'birthdate', 'car_type', 'car_model', 'car_year'];
    
    public function Orders() {
        return $this->hasMany(\App\Models\Orders::class, 'provider_id', 'id');
    }
    
    public function Products() {
        return $this->hasMany(\App\Models\providerProducts::class, 'provider_id', 'id');
    }
    
    public function PaymentWays() {
        return $this->hasMany(\App\Models\providerPaymentWays::class, 'provider_id', 'id');
    }
    
    public function Financial() {
        return $this->hasMany(\App\Models\Financial::class, 'provider_id', 'id');
    }
    
    public function City() {
        return $this->belongsTo(\App\Models\City::class, 'city_id', 'id');
    }
    
    public function Country() {
        return $this->belongsTo(\App\Models\Country::class, 'country_id', 'CountryId');
    }
    
    public function Items() {
        //return $this->belongsTo(\App\Models\providerItems::class, 'country_id', 'id');
        return $this->belongsToMany(\App\Models\Items::class, 'provider_items', 'provider_id', 'item_id');
    }
}
