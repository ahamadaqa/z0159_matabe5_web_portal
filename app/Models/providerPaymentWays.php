<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of providerPaymentWays
 *
 * @author Amna
 */
class providerPaymentWays extends Model {

    protected $table = 'provider_payment_ways';
    protected $fillable = ['payment_way_id', 'provider_id'];

}
