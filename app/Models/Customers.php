<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Customers
 *
 * @author Amna
 */
class Customers extends Model {
     protected $table = 'customers';
    protected $fillable = ['full_name', 'id_no', 'mobile','password', 'email', 'country_id', 'city_id', 'gender_id', 'birth_date',
        'id_image', 'image'];
    
    public function User() {
        return $this->belongsTo(\App\Models\Users::class, 'user_id', 'id');
    }
    
    public function Status() {
        return $this->belongsTo(\App\Models\ConstantsTable::class, 'status_id', 'id');
    }

    public function Country() {
        return $this->belongsTo(\App\Models\Country::class, 'country_id', 'CountryId');
    }
    
    public function City() {
        return $this->belongsTo(\App\Models\City::class, 'city_id', 'id');
    }
    
    public function Orders() {
        return $this->hasMany(\App\Models\Orders::class, 'customer_id', 'id');
    }
}
