<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of CustomerEvaluations
 *
 * @author Amna
 */
class CustomerEvaluations extends Model {
    protected $table = 'customer_evaluations';
    protected $fillable = ['customer_id', 'customer_id', 'customer_id', 'points', 'notes'];
}
