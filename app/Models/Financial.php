<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Financial
 *
 * @author Amna
 */
class Financial extends Model {
    protected $table = 'financial';
    protected $fillable = ['order_id', 'provider_id', 'payment_way_id','status_id','price','date', 'bill', 'order_payment_way_id','pay_date'];
    
    public function Order() {
        return $this->belongsTo(\App\Models\Orders::class, 'order_id', 'id');
    }
    
    public function Provider() {
        return $this->belongsTo(\App\Models\Providers::class, 'provider_id', 'id');
    }
}
