<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of evaluations
 *
 * @author ِAMna
 */
class EvaluationsCustomers extends Model {
    protected $table = 'evaluations_customers';
    protected $fillable = ['user_id', 'notes', 'evaluation', 'date', 'customer_id'];
    
    public function User() {
        return $this->belongsTo(\App\Models\Users::class, 'user_id', 'id');
    }

}
