<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of CustomerPayments
 *
 * @author Amna
 */
class CustomerPayments extends Model {
    protected $table = 'customer_payments';
    protected $fillable = ['order_id', 'customer_id', 'date', 'amount'];
    
    public function Order() {
        return $this->belongsTo(\App\Models\Orders::class, 'order_id', 'id');
    }
    
    public function Customer() {
        return $this->belongsTo(\App\Models\Customers::class, 'customer_id', 'id');
    }

}
