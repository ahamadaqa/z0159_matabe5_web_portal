<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Orders
 *
 * @author Amna
 */
class Orders extends Model {

    protected $table = 'orders';
    protected $fillable = ['order_id', 'customer_id', 'delegate_id', 'provider_id', 'order_date', 'product_id', 'quantity',
        'order_accept_time', 'order_type', 'arraival_time', 'order_status', 'payment_way_id', 'total_amount', 'added_value',
        'price', 'item_id', 'suggested_price', 'description'];

    public function Customer() {
        return $this->belongsTo(\App\Models\Customers::class, 'customer_id', 'id');
    }

    public function Provider() {
        return $this->belongsTo(\App\Models\Providers::class, 'provider_id', 'id');
    }

    public function Delegate() {
        return $this->belongsTo(\App\Models\Providers::class, 'delegate_id', 'id');
    }

    public function Status() {
        return $this->belongsTo(\App\Models\ConstantsTable::class, 'order_status', 'id');
    }
    
    public function Product() {
        return $this->belongsTo(\App\Models\providerProducts::class, 'product_id', 'id');
    }

    public static function distance($lat1, $lon1, $lat2, $lon2, $unit) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }

}
