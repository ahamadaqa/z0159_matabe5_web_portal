<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of providerPaymentWays
 *
 * @author Amna
 */
class delegatePaymentWays extends Model {
    protected $table = 'delegate_payment_ways';
    protected $fillable = ['payment_way_id', 'delegate_id'];
}
