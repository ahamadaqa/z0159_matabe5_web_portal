<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Settings
 *
 * @author Amna
 */

class Settings extends Model {
    
    protected $table = 'settings';
    protected $fillable = ['general_percentage', 'max_distance'];
}
