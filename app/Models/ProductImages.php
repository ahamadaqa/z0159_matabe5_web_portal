<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of ProductImages
 *
 * @author aymh_
 */
class ProductImages extends Model {
    protected $table = 'product_images';
    protected $fillable = ['product_id', 'image'];
    
    public function Product() {
        return $this->belongsTo(\App\Models\providerProducts::class, 'product_id', 'id');
    }
}
