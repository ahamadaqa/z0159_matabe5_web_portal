<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of providerItems
 *
 * @author aymh_
 */
class providerItems extends Model {

    protected $table = 'provider_items';
    protected $fillable = ['provider_id', 'item_id'];

    public function Item() {
        return $this->belongsTo(\App\Models\Items::class, 'item_id', 'id');
    }
    
    public function Provider() {
        return $this->belongsTo(\App\Models\Providers::class, 'provider_id', 'id');
    }

}
