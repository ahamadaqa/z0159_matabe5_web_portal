<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Notifications
 *
 * @author Amna
 */
class Notifications extends Model {
    protected $table = 'notifications';
    protected $fillable = ['notify_text', 'admin_id', 'date', 'target', 'customer_id', 'provider_id'];
    
    public function Admin() {
        return $this->belongsTo(\App\Models\Admins::class, 'admin_id', 'id');
    }
}
