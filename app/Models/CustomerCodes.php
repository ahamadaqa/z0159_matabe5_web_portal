<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class CustomerCodes extends Model {
    protected $table = 'customer_codes';
    protected $fillable = ['customer_id', 'order_id', 'code_id'];
    
    public function Code() {
        return $this->belongsTo(\App\Models\Codes::class, 'code_id', 'id');
    }
    
    public function Customer() {
        return $this->belongsTo(\App\Models\Customers::class, 'customer_id', 'id');
    }
}
