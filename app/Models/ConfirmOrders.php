<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of ConfirmOrders
 *
 * @author Amna
 */
class ConfirmOrders extends Model {
    protected $table = 'confirm_order';
    protected $fillable = ['customer_id', 'order_date', 'order_type', 'status_id', 'refuse_text'];
    
    public function Customer() {
        return $this->belongsTo(\App\Models\Customers::class, 'customer_id', 'id');
    }
}
