<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of ProviderEvaluations
 *
 * @author Amna
 */
class ProviderEvaluations extends Model {
    protected $table = 'provider_evaluations';
    protected $fillable = ['provider_id', 'order_id', 'customer_id', 'points', 'notes'];
    
    public function customer(){
        return $this->belongsTo(\App\Models\Customers::class, 'customer_id', 'id');
    }
}
