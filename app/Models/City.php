<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of City
 *
 * @author Hp
 */
class City extends Model {
    protected $table = 'city';
    protected $fillable = ['Name', 'EnglishName', 'active', 'CountryId'];
}
