<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Orders;
use App\Models\City;
use App\Models\Customers;
use App\Models\Providers;
use Illuminate\Http\Request;

/**
 * Description of DashboardController
 *
 * @author Amna
 */
class DashboardController extends Controller {

    public function index(Request $request) {
        $orders = new Orders();
        if (isset($request->from_date)) {
            $orders = $orders->whereDate('order_date', '>=', $request->from_date );
        }
        if (isset($request->to_date)) {
            $orders = $orders->whereDate('order_date', '<=', $request->to_date);
        }
        $ordersCount = $orders->where('order_status', '!=', 6)->where('order_status', '!=', 7)->count();
        $ordersCloseCount = $orders->where('order_status', 6)->count();
        $ordersCancelCount = $orders->where('order_status', 7)->count();
        $cities = City::get();
        $cityCustomersCount = $cityDelegatesCount = $cityProvidersCount = $cityOrdersCount = [];
        foreach ($cities as $city) {
            $customersCount = new Customers();
            $providers = new Providers();
            $delegates = new Providers();
            
            if (isset($request->from_date)) {
            $customersCount = $customersCount->whereDate('register_date', '>=', $request->from_date );
            $providers = $providers->whereDate('created_at', '>=', $request->from_date );
            $delegates = $delegates->whereDate('created_at', '>=', $request->from_date );
        }
        if (isset($request->to_date)) {
            $customersCount = $customersCount->whereDate('register_date', '<=', $request->to_date);
            $providers = $providers->whereDate('created_at', '<=', $request->to_date);
            $delegates = $delegates->whereDate('created_at', '<=', $request->to_date);
        }
            $customersCount = $customersCount->where('city_id', $city->id)->count();
            $delegatesCount = $delegates->where('city_id', $city->id)->where('type', 2)->count();
            $providersCount = $providers->where('city_id', $city->id)->where('type', 1)->count();
            $allOrdersCount = $orders->whereHas('Customer', function($q) use ($city) {
                        $q->where('city_id', $city->id);
                    })->count();
            $cityCustomerChart = [
                "c" => [[
                "v" => $city->Name
                    ], [
                        "v" => $customersCount
                    ]]
            ];

            $cityDelegateChart = [
                "c" => [[
                "v" => $city->Name
                    ], [
                        "v" => $delegatesCount
                    ]]
            ];

            $cityProviderChart = [
                "c" => [[
                "v" => $city->Name
                    ], [
                        "v" => $providersCount
                    ]]
            ];

            $cityOrdersChart = [
                "c" => [[
                "v" => $city->Name
                    ], [
                        "v" => $allOrdersCount
                    ]]
            ];
            array_push($cityCustomersCount, $cityCustomerChart);
            array_push($cityDelegatesCount, $cityDelegateChart);
            array_push($cityProvidersCount, $cityProviderChart);
            array_push($cityOrdersCount, $cityOrdersChart);
        }


        return response()->json(['ordersCount' => $ordersCount, 'ordersCloseCount' => $ordersCloseCount,
                    'ordersCancelCount' => $ordersCancelCount,
                    'cityCustomersCount' => $cityCustomersCount,
                    'cityDelegatesCount' => $cityDelegatesCount,
                    'cityProvidersCount' => $cityProvidersCount,
                    'cityOrdersCount' => $cityOrdersCount]);
    }

}
