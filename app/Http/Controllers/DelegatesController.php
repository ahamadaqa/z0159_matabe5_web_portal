<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\Delegates;
use Illuminate\Http\Request;

/**
 * Description of DelegatesController
 *
 * @author Amna
 */
class DelegatesController extends Controller {
    
    public function index(Request $request) {
        $Delegates = new Delegates();
        if (isset($request->name)) {
            $Delegates = $Delegates->where('name', 'like', '%' . $request->name . '%');
        }
        if (isset($request->mobile)) {
            $Delegates = $Delegates->where('mobile', 'like', '%' . $request->mobile . '%');
        }
        if (isset($request->email)) {
            $Delegates = $Delegates->where('email', $request->email);
        }
        return response()->json($Delegates->paginate($request->perPage));
    }
    
        public function show($id) {

        return response()->json(Delegates::with(['Orders', 'PaymentWays'])->find($id));
    }
    
    public function update(Request $request, $id) {
        $data = $request->data;
        $provider = Delegates::find($id);
        $provider->name_en = $data['name_en'];
        $provider->name_ar = $data['name_ar'];
        $provider->mobile = $data['mobile'];
        $provider->phone = $data['phone'];
        $provider->email = $data['email'];
        $provider->address = $data['address'];
        $provider->bank_account = $data['bank_account'];
        if($data['password'] != '')
        $provider->password = Hash::make($data['password']);
        
        $provider->save();        
        $response["status"] = true;
        $response["message"] = 'تم التعديل بنجاح';
        return response()->json($response);

    }
    
    public function destroy($id) {
        Delegates::destroy($id);
        $response["status"] = true;
        $response["message"] = 'تم الحذف بنجاح';
        return response()->json($response);
    }
}
