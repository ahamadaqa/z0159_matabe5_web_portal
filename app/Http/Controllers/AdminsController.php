<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\Admins;
use Illuminate\Http\Request;

/**
 * Description of AdminsController
 *
 * @author Amna
 */
class AdminsController extends Controller {

    public function index(Request $request) {
        $Admins = new Admins();
        if (isset($request->name)) {
            $Admins = $Admins->where('name', 'like', '%' . $request->name . '%');
        }
        if (isset($request->mobile)) {
            $Admins = $Admins->where('mobile', 'like', '%' . $request->mobile . '%');
        }
        if (isset($request->email)) {
            $Admins = $Admins->where('email', $request->email);
        }
        return response()->json($Admins->paginate($request->perPage));
    }

    public function store(Request $request) {

        $data = $request->toArray();
        $admins = Admins::where('username', $data['username'])->count();
        if ($admins>0) {
            $response["status"] = false;
            $response["message"] = 'اسم المستخدم موجود مسبقاً';
            return response()->json($response);
        }
        
        $adminss = Admins::where('email', $data['email'])->count();
        if ($adminss>0) {
            $response["status"] = false;
            $response["message"] = 'البريد الالكتروني موجود مسبقاً ';
            return response()->json($response);
        }
        
        $data['password'] = Hash::make($data['password']);
        Admins::create($data);
        $response["status"] = true;
        $response["message"] = 'تم الإضافة بنجاح';
        return response()->json($response);
    }

    public function show($id) {

        return response()->json(Admins::find($id));
    }

    public function update(Request $request, $id) {
        $data = $request->data;
        $type = Admins::find($id);
        
        $adminss = Admins::where('email', $data['email'])->where('id','!=', $id)->count();
        if ($adminss>0) {
            $response["status"] = false;
            $response["message"] = 'البريد الالكتروني موجود مسبقاً ';
            return response()->json($response);
        }

        $type->name = $data['name'];
        $type->username = $data['username'];
        $type->mobile = $data['mobile'];
        $type->email = $data['email'];
        if ($data['password'])
            $type->password = Hash::make($data['password']);

        
        $type->save();
        $response["status"] = true;
        $response["message"] = 'تم التعديل بنجاح';
        return response()->json($response);
    }

    public function destroy($id) {
        Admins::destroy($id);
        $response["status"] = true;
        $response["message"] = 'تم الحذف بنجاح';
        return response()->json($response);
    }

}
