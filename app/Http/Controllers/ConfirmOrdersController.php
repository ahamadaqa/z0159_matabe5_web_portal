<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\ConfirmOrders;
use App\Models\Providers;
use App\Models\Customers;
use Illuminate\Http\Request;

class ConfirmOrdersController extends Controller {
    public function index(Request $request) {
        $ConfirmOrders = ConfirmOrders::with(['Customer']);
        if (isset($request->name)) {
            $ConfirmOrders = $ConfirmOrders->whereHas('Customer', function($x) use ($request) {
                $x->where('full_name', 'like', '%' . $request->name . '%');
            });
        }
        if (isset($request->mobile)) {
            $ConfirmOrders = $ConfirmOrders->whereHas('Customer', function($x) use ($request) {
                $x->where('mobile', 'like', '%' . $request->mobile . '%');
            });
        }
        if (isset($request->order_id)) {
            $ConfirmOrders = $ConfirmOrders->where('id', $request->order_id);
        }
        if (isset($request->order_type)) {
            $ConfirmOrders = $ConfirmOrders->where('order_type', $request->order_type);
        }
        if (isset($request->status_id)) {
            $ConfirmOrders = $ConfirmOrders->where('status_id', $request->status_id);
        }
        return response()->json($ConfirmOrders->paginate($request->perPage));
    }
    
    public function accept($id) {
       $order =  ConfirmOrders::find($id);
       $order->status_id = 2;
       $order->save();
       $customer = Customers::find($order->customer_id);
       $data['name'] = $customer->full_name;
       $data['idno'] = $customer->id_no;
       $data['mobile'] = $customer->mobile;
       $data['email'] = $customer->email;
       $data['status_id'] = 1;
       $data['image'] = $customer->image;
       $data['country_id'] = $customer->country_id;
       $data['city_id'] = $customer->city_id;
       $data['type'] = $order->order_type;
       $data['gender'] = $customer->gender_id;
       $data['birthdate'] = $customer->birth_date;
       Providers::create($data);
       $response["status"] = true;
        $response["message"] = 'تم قبول الطلب  بنجاح';
        return response()->json($response);
    }
    
    public function refuse(Request $request) {
        $data = $request['data'];
       $order =  ConfirmOrders::find($data['orderId']);
       $order->status_id = 3;
       $order->refuse_text = $data['refuse_text'];
       $order->save();
       $response["status"] = true;
        $response["message"] = 'تم رفض الطلب  بنجاح';
        return response()->json($response);
    }
    
    public function show($id){
       return response()->json(ConfirmOrders::with(['Customer'])->find($id)); 
    }
}
