<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\Orders;
use App\Models\CustomerCodes;
use App\Models\CustomerEvaluations;
use App\Models\ProviderEvaluations;
use Illuminate\Http\Request;

/**
 * Description of OrdersController
 *
 * @author Amna
 */
class OrdersController extends Controller {

    public function index(Request $request) {
        $Orders = Orders::with(['Customer', 'Provider', 'Delegate', 'Status']);
        if (isset($request->order_id)) {
            $Orders = $Orders->where('order_id', $request->order_id);
        }
        if (isset($request->customer)) {
            $Orders = $Orders->whereHas('Customer', function($x) use ($request) {
                $x->where('full_name', 'like', '%' . $request->customer . '%');
                $x->orWhere('mobile', 'like', '%' . $request->customer . '%');
            });
        }
        if (isset($request->provider)) {
            $Orders = $Orders->whereHas('Provider', function($x) use ($request) {
                $x->where('name', 'like', '%' . $request->provider . '%');
                $x->orWhere('mobile', 'like', '%' . $request->provider . '%');
            });
        }
        if (isset($request->delegate)) {
            $Orders = $Orders->whereHas('Delegate', function($x) use ($request) {
                $x->where('name', 'like', '%' . $request->delegate . '%');
                $x->orWhere('mobile', 'like', '%' . $request->delegate . '%');
            });
        }
        if (isset($request->order_status)) {
            $Orders = $Orders->where('order_status', $request->order_status);
        }
        if (isset($request->order_type)) {
            $Orders = $Orders->where('order_type', $request->order_type);
        }

        if (isset($request->from_order_date)) {
            $Orders = $Orders->where('order_date', '>=', $request->from_order_date);
        }
        if (isset($request->to_order_date)) {
            $Orders = $Orders->where('order_date', '<=', $request->to_order_date);
        }
        return response()->json($Orders->paginate($request->perPage));
    }

    public function show($id) {

        $order = Orders::with(['Customer', 'Provider', 'Delegate', 'Status'])->find($id);
        if(!$order){
        $response['code'] = -1;
                $response['result_num'] = 0;
                $response['result_msg'] = "الطلب غير موجود";
                $response['result_object'] = [];
                return response()->json($response);    
        }
        $code = CustomerCodes::with('Code')->where('customer_id', $order->customer_id)->where('order_id', $order->id)->first();
        if($code)
        $discount = $code->code->percentage;
        else
           $discount = 0; 
        $order->discount = ($order->total_amount * $discount) / 100;

        $CustomerEvaluationsSum = CustomerEvaluations::where('customer_id', $order->customer_id)->sum('points');
        $CustomerEvaluationscount = CustomerEvaluations::where('customer_id', $order->customer_id)->count();

        if ($CustomerEvaluationscount == 0)
            $order->customer->evaluation = 0;
        else
            $order->customer->evaluation = $CustomerEvaluationsSum / $CustomerEvaluationscount;

    if ($order->provider_id != null) {
        $ProviderEvaluationsSum = ProviderEvaluations::where('provider_id', $order->provider_id)->sum('points');
        $ProviderEvaluationscount = ProviderEvaluations::where('provider_id', $order->provider_id)->count();

        if ($ProviderEvaluationscount == 0)
            $order->provider->evaluation = 0;
        else
            $order->provider->evaluation = $ProviderEvaluationsSum / $ProviderEvaluationscount;
        
    }

        if ($order->delegate_id != null) {
            $ProviderEvaluationsSum = ProviderEvaluations::where('provider_id', $order->delegate_id)->sum('points');
            $ProviderEvaluationscount = ProviderEvaluations::where('provider_id', $order->delegate_id)->count();

            if ($ProviderEvaluationscount == 0)
                $order->delegate->evaluation = 0;
            else
                $order->delegate->evaluation = $ProviderEvaluationsSum / $ProviderEvaluationscount;
        }
        
        if($order->deliver == 1){
           $lat_deliver =  $order->latitude;
            $log_deliver =  $order->longitude;
        } else {
            if($order->provider != null){
            $lat_deliver =  $order->provider->loc_lat;
            $log_deliver =  $order->provider->loc_long;
            } else {
            $lat_deliver =  null;
            $log_deliver =  null;    
            }
        }
        
        if($order->provider != null) {
        $distance = Orders::distance($order->provider->loc_lat, $order->provider->loc_long, $lat_deliver, $log_deliver, "K");
        //return $distance;
        $order->distance = round($distance,3);
        } else {
        $order->distance = null;    
        }
        return response()->json($order);
    }

}
