<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Providers;
use App\Models\Orders;
use App\Models\Customers;
use App\Models\CustomerEvaluations;
use App\Models\ProviderFiles;
use App\Models\Notifications;
use App\Models\Financial;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\JWTAuthExceptions\JWTException;
use Hash;

/**
 * Description of DelegateApiController
 *
 * @author amna
 */
class DelegateApiController extends Controller {

    function __construct() {
        \Config::set('jwt.user', 'App\Models\Providers');
        \Config::set('auth.providers.users.table', 'providers');
    }

    public function login(Request $request) {
        $credentials = $request->only('mobile', 'password');
        try {
            // verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                $response["code"] = -1;
                $response['result_num'] = 0;
                $response["result_msg"] = 'اسم المستخدم أو كلمة المرور خطأ';
                $response['result_object'] = [];
                return response()->json($response, 200);
            }
        } catch (JWTException $e) {
            // something went wrong
            $response["code"] = -2;
            $response['result_num'] = 0;
            $response["result_msg"] = 'حدث خطأ ما، لم يتم انشاء التوكن';
            $response['result_object'] = [];
            return response()->json($response, 200);
        }

        $user = Providers::where('mobile', $request->mobile)->where('type', 2)->first();
        if ($user) {
            unset($user->password);

            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = 'تم تسجيل الدخول بنجاح';
            $response['result_object'] = $user;
            $response['result_object']['token'] = $token;
        } else {
            $response["code"] = -1;
            $response['result_num'] = 0;
            $response["result_msg"] = 'اسم المستخدم أو كلمة المرور خطأ';
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function myOrders(Request $request) {
        $all_orders = [];
        $data = json_decode($request->getContent(), true);
        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;

        $orders = Orders::with(['Customer', 'Provider', 'Status', 'Product', 'Product.Item.Type']);
        if (isset($data['status_id'])) {
            $orders = $orders->where('order_status', $data['status_id']);
        }
        if (isset($data['order_id'])) {
            $orders = $orders->where('order_id', $data['order_id']);
        }
        if (isset($data['customer_name'])) {
            $orders = $orders->whereHas('Customer', function($q) use ($data) {
                $q->where('full_name', 'like', '%' . $data['customer_name'] . '%');
            });
        }
        if (isset($data['product_name'])) {
            $orders = $orders->whereHas('Product', function($q) use ($data) {
                $q->where('name', 'like', '%' . $data['product_name'] . '%');
            });
        }
        if (isset($data['category_id'])) {
            $orders = $orders->whereHas('Product.Item', function($q) use ($data) {
                $q->where('item_type', $data['category_id']);
            });
        }
        $orders = $orders->where('delegate_id', $provider_id)->orderBy('id', 'desc')->paginate($request['per_page']);
        $i = 0;
        foreach ($orders as $order) {
            if ($order->Customer->image != null) {
                $storage = storage_path(ltrim($order->Customer->image, '/'));
                $storage = explode('public_html', $storage);
                $profileImg = 'http://' . $request->getHttpHost() . $storage[1];
            } else {
                $profileImg = null;
            }
            $all_orders[$i] = [
                'order_id' => $order->id,
                'product_name' => $order->Product->name,
                'customer_name' => $order->Customer->full_name,
                'order_date' => $order->order_date,
                'order_no' => $order->order_id,
                'order_status' => $order->Status->name,
                'order_category' => $order->Product->Item->Type->name,
                'customer_image' => $profileImg
            ];
            $i++;
        }
        $count = count($orders);
        if ($count > 0) {
            $response["code"] = 1;
            $response['result_num'] = $count;
            $response["result_msg"] = "تم العثور على البيانات المطلوبة";
            $response['result_object'] = $all_orders;
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function acceptCancelOrder(Request $request) {
        $data = json_decode($request->getContent(), true);
        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;
        $order = Orders::find($data['order_id']);
        if ($order) {
            if ($provider_id != $order->delegate_id) {
                $response['code'] = -2;
                $response['result_num'] = 0;
                $response['result_msg'] = "غير مسموح لك بقبول أو رفض الطلب";
                $response['result_object'] = [];
                return response()->json($response);
            }
            if ($data['status_id'] != 4 && $data['status_id'] != 7) {
                $response['code'] = -3;
                $response['result_num'] = 0;
                $response['result_msg'] = "الحالة المدخلة غير صحيحة";
                $response['result_object'] = [];
                return response()->json($response);
            }
            if ($order->order_status != 3) {
                $response['code'] = -4;
                $response['result_num'] = 0;
                $response['result_msg'] = "يجب أن تكون حالة الطلب تم التحضير حتى يمكن قبوله أو رفضه";
                $response['result_object'] = [];
                return response()->json($response);
            }
            $order->order_status = $data['status_id'];
            $order->save();
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = "تم تغيير الحالة بنجاح";
            $response['result_object'] = [$order];
            return response()->json($response);
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "الطلب غير موجود";
            $response['result_object'] = [];
            return response()->json($response);
        }
    }

    public function deliverOrder(Request $request) {
        $data = json_decode($request->getContent(), true);
        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;
        $order = Orders::find($data['order_id']);
        if ($order) {
            if ($provider_id != $order->delegate_id) {
                $response['code'] = -2;
                $response['result_num'] = 0;
                $response['result_msg'] = "غير مسموح لك بتغيير حالة الطلب";
                $response['result_object'] = [];
                return response()->json($response);
            }
            if ($data['status_id'] != 5) {
                $response['code'] = -3;
                $response['result_num'] = 0;
                $response['result_msg'] = "الحالة المدخلة غير صحيحة";
                $response['result_object'] = '';
                return response()->json($response);
            }
            if ($order->order_status != 4) {
                $response['code'] = -4;
                $response['result_num'] = 0;
                $response['result_msg'] = "يجب أن تكون حالة الطلب جاري التوصيل";
                $response['result_object'] = '';
                return response()->json($response);
            }
            $order->order_status = $data['status_id'];
            $order->save();
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = "تم تغيير الحالة بنجاح";
            $response['result_object'] = [$order];
            return response()->json($response);
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "الطلب غير موجود";
            $response['result_object'] = [];
            return response()->json($response);
        }
    }
    
    public function evaluateCustomer(Request $request) {
        $data = json_decode($request->getContent(), true);
        $userAuth = JWTAuth::parseToken()->authenticate();
        $delegate_id = $userAuth->id;
        
        if(!$userAuth){
            $response['code'] = -2;
            $response['result_num'] = 0;
            $response['result_msg'] = "التوكن غير صحيح، يرجى إعادة تسجيل الدخول";
            $response['result_object'] = [];
            return response()->json($response);
        }
        
        if($data['customer_id']){
            $provider = Customers::find($data['customer_id']);
            if(!$provider){
                $response['code'] = -3;
                $response['result_num'] = 0;
                $response['result_msg'] = "العميل غير موجود يرجى التأكد من المدخلات";
                $response['result_object'] = [];
                return response()->json($response);
            }
        }

        $data['provider_id'] = $delegate_id;
        $evaluation = CustomerEvaluations::create($data);
        if ($evaluation) {
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = "تم تقييم العميل بنجاح";
            $response['result_object'] = [$evaluation];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "حدثت مشكلة أثناء التقييم، يرجى إعادة التقييم";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function notifications(Request $request) {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $delegate_id = $userAuth->id;

        $myNotifications = Notifications::where('provider_id', $delegate_id)
                        ->orderBy('date', 'desc')->paginate($request['per_page']);

        if (count($myNotifications) > 0) {
            $response["code"] = 1;
            $response['result_num'] = count($myNotifications);
            $response["result_msg"] = "تم العثور على البيانات المطلوبة";
            $response['result_object'] = $myNotifications;
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function financial() {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $delegate_id = $userAuth->id;

        $financial = Financial::where('provider_id', $delegate_id)->get();

        if (count($financial) > 0) {
            $response["code"] = 1;
            $response['result_num'] = count($financial);
            $response["result_msg"] = "تم العثور على البيانات المطلوبة";
            $response['result_object'] = $financial;
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function myAccount(Request $request) {
        $data = json_decode($request->getContent(), true);

        $userAuth = JWTAuth::parseToken()->authenticate();
        $delegate_id = $userAuth->id;

        $delegate = Providers::where('type', 2)->where('mobile', $data['mobile'])->first();
        if ($delegate) {
            $response['code'] = -2;
            $response['result_num'] = 0;
            $response['result_msg'] = "رقم الجوال مدخل مسبقاً";
            $response['result_object'] = [];
            return response()->json($response);
        }

        $myAccount = Providers::find($delegate_id);
        $myAccount->name = $data['name'];
        $myAccount->idno = $data['idno'];
        $myAccount->mobile = $data['mobile'];
        $myAccount->email = $data['email'];
        $myAccount->country_id = $data['country'];
        $myAccount->city_id = $data['city'];
        $myAccount->gender = $data['gender'];
        $myAccount->birthdate = $data['birthdate'];
        $myAccount->car_type = $data['car_type'];
        $myAccount->car_model = $data['car_model'];  
        $myAccount->car_year = $data['car_year']; 
        
        $myAccount->save();
        if($myAccount){
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = "تم التعديل بنجاح";
            $response['result_object'] = [$myAccount];
            
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "الحساب غير موجود";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }
    
    public function profile(Request $request){
       $userAuth = JWTAuth::parseToken()->authenticate();
       $delegate_id = $userAuth->id; 
       $delegate = Providers::find($delegate_id);
       
       if($delegate){
           unset($delegate->password);
           if ($delegate->image != null && $delegate->image != "") {
                $storage = storage_path(ltrim($delegate->image, '/'));
                $storage = explode('public_html', $storage);
                $delegate->image = 'http://' . $request->getHttpHost() . $storage[1];
            }
          
           $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = 'تم العثور على البيانات المطلوبة';
            $response['result_object'] = [$delegate]; 
       } else {
           $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'لا توجد نتائج';
            $response['result_object'] = [];
           
       }
       
       return response()->json($response);
    }
    
    public function addDelegate(Request $request) {
        $data = json_decode($request->getContent(), true);
    
        $delegate = Providers::where('type', 2)->where('mobile', $data['mobile'])->first();
        if ($delegate) {
            $response['code'] = -2;
            $response['result_num'] = 0;
            $response['result_msg'] = "رقم الجوال مدخل مسبقاً";
            $response['result_object'] = [];
            return response()->json($response);
        }
        $data['type'] = 2; 
        $myAccount = Providers::create($data);
               
        if($myAccount){
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = "تم تسجيل الحساب بنجاح";
            $response['result_object'] = [$myAccount];
            
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "حدث خطأ أثناء عملية التسجيل";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

}
