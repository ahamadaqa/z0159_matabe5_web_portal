<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Advertisments;
use Illuminate\Http\Request;
include 'ImageResize.php';

/**
 * Description of AdvertismentsController
 *
 * @author Amna
 */
class AdvertismentsController extends Controller {
    public function index(Request $request) {
        $Advertisments = new Advertisments();
        if (isset($request->from_date)) {
            $Advertisments = $Advertisments->whereDate('from_date', '>=', $request->from_date );
        }
        if (isset($request->to_date)) {
            $Advertisments = $Advertisments->whereDate('from_date', '<=', $request->to_date);
        }
        return response()->json($Advertisments->paginate($request->perPage));
    }
    
        public function store(Request $request) {
            $data = $request->toArray();
            
            if(!isset($data['image'])){
              $response["status"] = false;
        $response["message"] = 'يجب إدخال صورة الإعلان !';
        return response()->json($response);  
            }
            
        $image = strip_tags($data['image']);

        if (empty($image)) {
            $response["status"] = false;
            $response["data"] = "يجب تحميل الصورة ";
            return response()->json($response);
        }
//        if(isset($data['from_date']) && isset($data['to_date'])){
//        $fromDT = $data['from_date'];
//        $toDT = $data['to_date'];
//        $ads = Advertisments::where(function($q) use ($fromDT, $toDT) {
//                    $q->whereBetween('from_date', [$fromDT, $toDT])
//                            ->orWhereBetween('to_date', [$fromDT, $toDT]);
//                })->count();
//        if ($ads > 0) {
//            $response["status"] = FALSE;
//            $response["message"] = 'يوجد إعلان بنفس الفترة !';
//            return response()->json($response);
//        }
//        }

        $arr = explode(",", $image);
        $base64 = str_replace($arr[0], '+', $arr[1]);

        $s = strrpos($arr[0], '/') + 1;
        $e = strrpos($arr[0], ';');
        $extension = substr($arr[0], $s, $e - $s);

        if ($extension === 'jpeg' || $extension === 'jpg') {
            $extension = 'jpg';
        } else if ($extension !== 'png') {
            $response["status"] = false;
            $response["data"] = "Unsupported File Type.";
            return response()->json($response);
        }

        $base2image = ImageResize::createFromString(base64_decode($base64));
        
        

        $tmp = '/images/'
                . '_'
                . round(microtime(true) * 1000)
                . '.'
                . $extension;
        $base2image->save(base_path() . '/storage'.$tmp);

        $data['image'] = $tmp;
        Advertisments::create($data);
        $response["status"] = true;
        $response["message"] = 'تم الإضافة بنجاح';
        return response()->json($response);
    }
    
    public function show($id) {

        return response()->json(Advertisments::find($id));
    }
    
    public function update(Request $request, $id) {
        $data = $request->data;
        
//        if(isset($data['from_date']) && isset($data['to_date'])){
//        $fromDT = $data['from_date'];
//        $toDT = $data['to_date'];
//        $ads = Advertisments::where(function($q) use ($fromDT, $toDT) {
//                    $q->whereBetween('from_date', [$fromDT, $toDT])
//                            ->orWhereBetween('to_date', [$fromDT, $toDT]);
//                })->where('id','!=', $id)->count();
//        if ($ads > 0) {
//            $response["status"] = FALSE;
//            $response["message"] = 'يوجد إعلان بنفس الفترة !';
//            return response()->json($response);
//        }
//        }
        
        $image = $data['image'];
        
        $arr = explode(",", $image);
        //dd($arr);
        if(count($arr) > 1){
        $base64 = str_replace($arr[0], '+', $arr[1]);

        $s = strrpos($arr[0], '/') + 1;
        $e = strrpos($arr[0], ';');
        $extension = substr($arr[0], $s, $e - $s);

        if ($extension === 'jpeg' || $extension === 'jpg') {
            $extension = 'jpg';
        } else if ($extension !== 'png') {
            $response["status"] = false;
            $response["data"] = "Unsupported File Type.";
            return response()->json($response);
        }

        $base2image = ImageResize::createFromString(base64_decode($base64));
        
        

        $tmp = '/images/'
                . '_'
                . round(microtime(true) * 1000)
                . '.'
                . $extension;
        $base2image->save(base_path() . '/storage'.$tmp);

        $data['image'] = $tmp;
        }
        $type = Advertisments::find($id);

        $type->image = $data['image'];
        $type->from_date = $data['from_date'];
        $type->to_date = $data['to_date'];
        $type->url = $data['url'];
        $type->mobile = $data['mobile'];

        $type->save();
        $response["status"] = true;
        $response["message"] = 'تم التعديل بنجاح';
        return response()->json($response);
    }
    
    public function destroy($id) {
        Advertisments::destroy($id);
        $response["status"] = true;
        $response["message"] = 'تم الحذف بنجاح';
        return response()->json($response);
    }    
    
        // for mobile
    public function getAdvertisments(Request $request){
        $Advertisments = new Advertisments();
        $Advertisments = $Advertisments->whereDate('from_date', '<=', date('Y-m-d') );
        $Advertisments = $Advertisments->whereDate('to_date', '>=', date('Y-m-d'));
        //return $Advertisments->toSql();
        $result = $Advertisments->paginate($request['per_page']);
        foreach($result as $res){
            if($res->image != null){
                $storage = storage_path(ltrim($res->image, '/'));
                $storage = explode('public_html', $storage);
            $res->image = 'http://'.$request->getHttpHost().$storage[1];
            }
        }
        $count = $Advertisments->count();
        if($count>0){
            $response['code'] = 1;
            $response['result_num'] = $count;
            $response['result_msg'] = 'تم العثور على البيانات المطلوبة';
            $response['result_object'] = $result;
        } 
        else {
            $response['code'] = -1;
            $response['result_num'] = $count;
            $response['result_msg'] = 'لا يوجد نتائج';
            $response['result_object'] = $result;
        }
        return response()->json($response);
    }
}