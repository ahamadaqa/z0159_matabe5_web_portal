<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admins;
use Hash;
use JWTAuth;
use Tymon\JWTAuth\JWTAuthExceptions\JWTException;

/**
 * Description of AuthenticateController
 *
 * @author Amna
 */
class AuthenticateController extends Controller {

    public function index() {
        // TODO: show users
    }

    public function authenticate(Request $request) {

       $credentials = $request->only('username', 'password');
 

        try {
            // verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $user = Admins::where('username', $request->username)->first();
//dd(JWTAuth::attempt($credentials));
        // if no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }

    public function getAuthenticatedUser() {
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());
        }
//dd($user);
        // the token is valid and we have found the user via the sub claim
        return response()->json(['user'=>['id'=>$user->id, 'name'=> $user->name, 'is_super_admin'=> $user->is_super_admin]]);
    }

    public function logout() {
//        $user = JWTAuth::parseToken()->authenticate();
//        $user->lang = 'ar';
//        $user->save();
        $token = JWTAuth::getToken();
        if ($token) {
            JWTAuth::setToken($token)->invalidate();
        }
    }

}
