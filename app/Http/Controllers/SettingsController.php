<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Settings;
use App\Models\City;
use App\Models\Country;
use App\Models\DeliverPricing;
use App\Models\Codes;
use App\Models\CustomerCodes;
use App\Models\Orders;
use Illuminate\Http\Request;

/**
 * Description of AdminsController
 *
 * @author Amna
 */
class SettingsController extends Controller {

    public function index(Request $request) {
        $endCodes = Codes::where('last_date', '<', date('Y-m-d'))->where('active', 1)->get();
        foreach ($endCodes as $code){
            $code->active = 0;
            $code->save();
        }
        
        $response['settings'] = Settings::find(1);
        $response['cities'] = City::paginate($request->perPage);
        $response['countries'] = Country::paginate($request->perPage);
        $response['pricing'] = DeliverPricing::paginate($request->perPage);
        
        $lastItem = DeliverPricing::orderBy('id', 'desc')->first();
        $response['begin'] = $lastItem->dis_to + 0.1;
        $codes = Codes::paginate($request->perPage);

        $codes->map(function ($code) {
            $code['customers'] = CustomerCodes::where('code_id', $code['id'])->count();
        });

        $response['codes'] = $codes;
        return response()->json($response);
    }

    public function store(Request $request) {
        $data = $request->toArray();
        $lastItem = DeliverPricing::orderBy('id', 'desc')->first();

        if ($lastItem->dis_to >= $data['dis_from']) {
            $response["status"] = false;
            $response["message"] = 'يجب أن تكون المسافات متسلسلة !';
            return response()->json($response);
        }

        DeliverPricing::create($data);
        $response["status"] = true;
        $response["message"] = 'تم الإضافة بنجاح';
        return response()->json($response);
    }

    public function show($id) {
        return response()->json(DeliverPricing::find($id));
    }

    public function editPrice(Request $request, $id) {
        $data = $request;
        $settings = DeliverPricing::find($id);
        $settings->dis_from = $data['dis_from'];
        $settings->dis_to = $data['dis_to'];
        $settings->price = $data['price'];
        $settings->save();
        $response["status"] = true;
        $response["message"] = 'تم التعديل بنجاح';
        return response()->json($response);
    }

    public function update(Request $request, $id) {
        $data = $request['data'];
        $settings = Settings::find($id);
        $settings->general_percentage = $data['general_percentage'];
        $settings->max_distance = $data['max_distance'];
        $settings->save();
        $response["status"] = true;
        $response["message"] = 'تم التعديل بنجاح';
        return response()->json($response);
    }

    public function createCity(Request $request) {
        $data = $request->toArray();

        City::create($data);
        $response["status"] = true;
        $response["message"] = 'تم الإضافة بنجاح';
        return response()->json($response);
    }

    public function getCity($id) {
        return response()->json(City::find($id));
    }

    public function edit_city(Request $request, $id) {
        $data = $request;
        $settings = City::find($id);
        $settings->Name = $data['Name'];
        $settings->CountryId = $data['CountryId'];
        $settings->active = $data['active'];
        $settings->save();
        $response["status"] = true;
        $response["message"] = 'تم التعديل بنجاح';
        return response()->json($response);
    }

    public function createCode(Request $request) {
        $data = $request->toArray();
        
        $num = Codes::where('code_id', $data['code_id'])->count();

        if($num > 0) {
           $response["status"] = false;
            $response["message"] = 'معرف الكود موجود مسبقاً';
            return response()->json($response); 
        }
        //dd($data['last_date']);
        if($data['last_date'] < date('Y/m/d')) {
           $response["status"] = false;
            $response["message"] = 'يجب اختيار تاريخ أكبر أو يساوي تاريخ اليوم !';
            return response()->json($response); 
        }

        Codes::create($data);
        $response["status"] = true;
        $response["message"] = 'تم الإضافة بنجاح';
        return response()->json($response);
    }
    
    public function codeViewCustomer($id){
        $code = Codes::find($id);
        $code->customers = CustomerCodes::with('Customer')->where('code_id', $id)->get();
        return response()->json($code);
    }
    
    public function deleteCode($id){
        Codes::destroy($id);
        $response["status"] = true;
        $response["message"] = 'تم الحذف بنجاح';
        return response()->json($response);
        
    }
    
    public function getCountries(Request $request){
      $countries = Country::get();  
      if(count($countries) > 0){
          $response['code'] = 1;
            $response['result_num'] = count($countries);
            $response['result_msg'] = 'تم العثور على البيانات المطلوبة';
            $response['result_object'] = $countries;
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'لا يوجد نتائج';
            $response['result_object'] = [];
        }
        
        return response()->json($response);
      
    }
    
    public function getCities(Request $request){
      $cities = City::where('active', 1);
      if(isset($request['country_id'])){
         $cities = $cities->where('CountryId',$request['country_id']);
     }
      
      $cities = $cities->get();  
      if(count($cities) > 0){
          $response['code'] = 1;
            $response['result_num'] = count($cities);
            $response['result_msg'] = 'تم العثور على البيانات المطلوبة';
            $response['result_object'] = $cities;
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'لا يوجد نتائج';
            $response['result_object'] = [];
        }
        
        return response()->json($response);
      
    }
    
    public function getPercentage($code_id) {
        $code = Codes::where(['code_id'=>$code_id, 'active'=>1])->first();
        if($code){
           $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = 'تم العثور على البيانات المطلوبة';
            $response['result_object'] = [['discount_percentage'=>$code->percentage]]; 
        } else {
           $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'الكود غير موجود أو انتهي تفعيله';
            $response['result_object'] = []; 
        }
        return response()->json($response);
    }
    
    public function getDeliveryCost(Request $request){
        $data = json_decode($request->getContent(), true);
        $distance = Orders::distance($data['from_latitude'], $data['from_longitude'], $data['to_latitude'], $data['to_longitude'], "K");
        $distance = round($distance,3);
        
        $DeliverPricing = DeliverPricing::where('dis_from', '<=', $distance)->Where('dis_to','>=', $distance)->first();

        if($DeliverPricing){
           $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = 'تم العثور على البيانات المطلوبة';
            $response['result_object'] = [['price'=>$DeliverPricing->price]]; 
        } else {
           $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'المسافة غير موجودة ضمن نطاق الأسعار';
            $response['result_object'] = []; 
        }
        return response()->json($response);
    }
    
    public function getSetting(){
        $setting = Settings::find(1);
        
        if($setting){
           $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = 'تم العثور على البيانات المطلوبة';
            $response['result_object'] = [['general_percentage'=>$setting->general_percentage, 'max_distance'=>$setting->max_distance]]; 
        } else {
           $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'الإعدادت غير موجودة';
            $response['result_object'] = []; 
        }
        return response()->json($response);
        
    }

}
