<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Customers;
use App\Models\CustomerEvaluations;
use App\Models\CustomerPayments;
use Illuminate\Http\Request;

/**
 * Description of CustomersController
 *
 * @author Amna
 */
class CustomersController extends Controller {

    public function index(Request $request) {
        $Customers = Customers::with(['Status']);
        if (isset($request->full_name)) {
            $Customers->where('full_name', 'like', '%' . $request->full_name . '%');
        }
        if (isset($request->mobile)) {
            $Customers->where('mobile', 'like', '%' . $request->mobile . '%');
        }
        if (isset($request->email)) {
            $Customers->where('email', 'like', '%' . $request->email . '%');
        }
        if (isset($request->status_id)) {
            $Customers->where('status_id', $request->status_id);
        }
        if (isset($request->from_register_date)) {
            $Customers = $Customers->where('register_date', '>=', $request->from_register_date);
        }
        if (isset($request->to_register_date)) {
            $Customers = $Customers->where('register_date', '<=', $request->to_register_date);
        }
        return response()->json($Customers->paginate($request->perPage));
    }

    public function show($id) {
        $response['data'] = Customers::with(['Country', 'City', 'Orders.Status'])->find($id);
        $CustomerEvaluationsSum = CustomerEvaluations::where('customer_id', $id)->sum('points');
        $CustomerEvaluationscount = CustomerEvaluations::where('customer_id', $id)->count();

        if ($CustomerEvaluationscount == 0)
            $response['evaluation'] = 0;
        else
            $response['evaluation'] = $CustomerEvaluationsSum / $CustomerEvaluationscount;
        
        $response['payments'] = CustomerPayments::with('Order')->where('customer_id', $id)->get();
        return response()->json($response);
    }

    public function getTrip($id) {
        $response['result_num'] = 1;
        $response['result_msg'] = "";
        $response['result_object'] = Customers::with(['User', 'Status'])->find($id);

        return response()->json($response);
    }

    public function shareTrip(Request $request) {
        $data = json_decode($request->getContent(), TRUE);
        $data['status_id'] = 1;
        CustomersOrders::create($data);
        $response["status"] = 200;
        $response["result_msg"] = 'تم الإضافة بنجاح';
        return response()->json($response);
    }

}
