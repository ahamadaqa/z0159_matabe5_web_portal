<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\Providers;
use App\Models\providerProducts;
use App\Models\ProviderEvaluations;
use App\Models\providerPaymentWays;
use App\Models\ProviderFiles;
use App\Models\providerItems;
use App\Models\Country;
use App\Models\City;
use App\Models\Financial;
use App\Models\Orders;
use App\Models\Settings;
use Illuminate\Http\Request;
include 'ImageResize.php';

/**
 * Description of ProvidersController
 *
 * @author Amna
 */
class ProvidersController extends Controller {

    public function index(Request $request) {
        $providers = new Providers();
        if (isset($request->name)) {
            $providers = $providers->where('name', 'like', '%' . $request->name . '%');
        }
        if (isset($request->mobile)) {
            $providers = $providers->where('mobile', 'like', '%' . $request->mobile . '%');
        }
        if (isset($request->email)) {
            $providers = $providers->where('email', 'like', '%' .$request->email. '%');
        }
        if (isset($request->country_id)) {
            $providers = $providers->where('country_id', $request->country_id);
        }
        if (isset($request->city_id)) {
            $providers = $providers->where('city_id', $request->city_id);
        }
        
        if (isset($request->status_id)) {
            $providers = $providers->where('status_id', $request->status_id);
        }
        return response()->json($providers->where('type', $request->type)->paginate($request->perPage));
    }

    public function getOptions(Request $request) {
        $data['countries'] = Country::get();
        if ($request->country_id) {
            $data['cities'] = City::where('CountryId', $request->country_id)->get();
        } else {
             $data['cities'] = City::get();
        }
        return response()->json($data);
    }

    public function show($id) {
        $provider = Providers::find($id);
        if($provider->type == 1){
          $response['data'] = Providers::with(['Orders.Customer', 'Products.Item', 'PaymentWays',
              'Financial'=>function($q){$q->orderBy('pay_date', 'desc');}])->find($id);
          $response['totalIncome'] = Orders::where('provider_id', $id)->sum('total_amount');
        } 
        else {
           $delegate = Providers::with(['PaymentWays', 'Financial'])->find($id); 
           $delegateOrders = Orders::with('Customer')->where('delegate_id', $id)->get();
           $delegate->orders = $delegateOrders;
           $response['data'] = $delegate;
           $response['totalIncome'] = Orders::where('delegate_id', $id)->sum('total_amount');
        }
        
        $ProviderEvaluationsSum = ProviderEvaluations::where('provider_id', $id)->sum('points');
        $ProviderEvaluationscount = ProviderEvaluations::where('provider_id', $id)->count();
        
        if($ProviderEvaluationscount == 0)
            $response['evaluation'] = 0;
        else
        $response['evaluation'] = $ProviderEvaluationsSum/$ProviderEvaluationscount;
        
        $response['files'] = ProviderFiles::where('provider_id', $id)->get();
        
        
        
        $percentage = Settings::find(1);
        $response['profit'] = ($response['totalIncome'] * $percentage->general_percentage)/100;
        
        $response['totalPaied'] = Financial::where('provider_id', $id)->where('status_id',2)->sum('price');
        
        $response['totalRemind'] = $response['profit'] - $response['totalPaied'];

        return response()->json($response);
    }

    public function update(Request $request, $id) {
        $data = $request->data;
        $provider = Providers::find($id);
        $provider->name = $data['name'];
        $provider->mobile = $data['mobile'];
        $provider->phone = $data['phone'];
        $provider->email = $data['email'];
        $provider->address = $data['address'];
        $provider->bank_account = $data['bank_account'];
        $provider->status_id = $data['status_id'];
        if ($data['password'] != '')
            $provider->password = Hash::make($data['password']);

        $provider->save();
        $response["status"] = true;
        $response["message"] = 'تم التعديل بنجاح';
        return response()->json($response);
    }

    public function destroy($id) {
        Providers::destroy($id);
        $response["status"] = true;
        $response["message"] = 'تم الحذف بنجاح';
        return response()->json($response);
    }
    
    public function add_payment_way(Request $request) {
        $data = $request->toArray();
        
        $samePayment = providerPaymentWays::where('provider_id', $data['provider_id'])->where('payment_way_id', $data['payment_way_id'])->count();
        if($samePayment>0){
        $response["status"] = false;
        $response["message"] = 'طريقة الدفع موجودة مسبقاً للمزود';
        return response()->json($response);  
        }
        providerPaymentWays::create($data);
        $response["status"] = true;
        $response["message"] = 'تم الإضافة بنجاح';
        return response()->json($response);
    }
    
    public function getPaymentsWays($id){
        $PaymentsWays = providerPaymentWays::where('provider_id',$id)->get();
        return response()->json($PaymentsWays);
    }
    
    public function delete_payment_way($id){
        providerPaymentWays::destroy($id);
        $response["status"] = true;
        $response["message"] = 'تم الحذف بنجاح';
        return response()->json($response);
        
    }
    
    public function viewProduct($id){
        return response()->json(providerProducts::with('Item')->find($id));
    }
    
    public function deleteFile($id){
        ProviderFiles::destroy($id);
        $response["status"] = true;
        $response["message"] = 'تم الحذف بنجاح';
        return response()->json($response);
    }
    
    public function addFile(Request $request){
        $data = $request->toArray();
        
        $image = strip_tags($data['image']);
        $arr = explode(",", $image);
        $base64 = str_replace($arr[0], '+', $arr[1]);

        $s = strrpos($arr[0], '/') + 1;
        $e = strrpos($arr[0], ';');
        $extension = substr($arr[0], $s, $e - $s);

        if ($extension === 'jpeg' || $extension === 'jpg') {
            $extension = 'jpg';
        } else if ($extension !== 'png') {
            $response["status"] = false;
            $response["data"] = "Unsupported File Type.";
            return response()->json($response);
        }

        $base2image = ImageResize::createFromString(base64_decode($base64));
        
        

        $tmp = '/images/'
                . '_'
                . round(microtime(true) * 1000)
                . '.'
                . $extension;
        $base2image->save(base_path() . '/storage'.$tmp);

        $data['image'] = $tmp;
        ProviderFiles::create($data);
        
        $response["status"] = true;
        return response()->json($response);
    }
    
    public function getProducts($id){
        
    }
    
    public function providersByCategory($id, Request $request){
         $providerItems = Providers::whereHas('Items',function($q) use ($id){
         $q->where('item_type', $id);
     });
     if(isset($request['provider_name'])){
        $providerItems = $providerItems->where('name', 'like', '%'. $request['provider_name'].'%');
     }
        $providerItems = $providerItems->paginate($request['per_page']);
     
      foreach($providerItems as $provider) {
          unset($provider->password);
          unset($provider->type);
          unset($provider->car_type);
          unset($provider->car_model);
          unset($provider->car_year);
          $ProviderEvaluationsSum = ProviderEvaluations::where('provider_id', $provider->id)->sum('points');
            $ProviderEvaluationscount = ProviderEvaluations::where('provider_id', $provider->id)->count();

            if ($ProviderEvaluationscount == 0)
                $provider->rating_average = 0;
            else
                $provider->rating_average = $ProviderEvaluationsSum / $ProviderEvaluationscount;
            
            $provider->categories_count = count($provider->Items);
            $provider->product_count = providerProducts::where('provider_id', $provider->id)->count();
            $categories_name = [];
            foreach($provider->Items as $item){
                array_push($categories_name, $item->name);
            }
            $provider->categories_name = $categories_name;
            
            $country = Country::where('CountryId', $provider->country_id)->first();
            $provider->country_name = $country->Name;
            
            $city = City::where('id', $provider->city_id)->first();
            $provider->city_name = $city->Name;
      }
     
        if(count($providerItems) > 0){
            $response['code'] = 1;
            $response['result_num'] = count($providerItems);
            $response['result_msg'] = 'تم العثور على البيانات المطلوبة';
            $response['result_object'] = [$providerItems];
        } 
        else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'لا يوجد نتائج';
            $response['result_object'] = [];
        }
        return response()->json($response);
    }
    
    public function providerEvaluations($id, Request $request){
       $rates = ProviderEvaluations::with('customer')->where('provider_id', $id)->paginate($request['per_page']); 
       foreach($rates as $rate){
           unset($rate->customer->password);
           
           if ($rate->customer->image != null && $rate->customer->image != "") {
                $storage = storage_path(ltrim($rate->customer->image, '/'));
                $storage = explode('public_html', $storage);
                $rate->customer->image = 'http://' . $request->getHttpHost() . $storage[1];
            }
            if ($rate->customer->id_image != null && $rate->customer->id_image != "") {
                $storage = storage_path(ltrim($rate->customer->id_image, '/'));
                $storage = explode('public_html', $storage);
                $rate->customer->id_image = 'http://' . $request->getHttpHost() . $storage[1];
            }
       }
       if(count($rates) > 0){
            $response['code'] = 1;
            $response['result_num'] = count($rates);
            $response['result_msg'] = 'تم العثور على البيانات المطلوبة';
            $response['result_object'] = [$rates];
        } 
        else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'لا يوجد نتائج';
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

}
