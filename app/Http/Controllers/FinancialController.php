<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Financial;
use Illuminate\Http\Request;
include 'ImageResize.php';

/**
 * Description of FinancialController
 *
 * @author Amna
 */
class FinancialController extends Controller {
    public function index(Request $request) {
        $Financial= Financial::with(['Provider']);
        if (isset($request->name)) {
            $Financial = $Financial->whereHas('Provider', function($x) use ($request) {
                $x->where('name', 'like', '%' . $request->name . '%');
                $x->where('type', 2);
            });
        }
        if (isset($request->provider_name)) {
            $Financial = $Financial->whereHas('Provider', function($x) use ($request) {
                $x->where('name', 'like', '%' . $request->provider_name . '%');
                $x->where('type', 1);
            });
        }
        if (isset($request->order_id)) {
            $Financial->where('order_id', $request->order_id);
        }
        if (isset($request->status_id)) {
            $Financial->where('status_id', $request->status_id);
        }
        if (isset($request->payment_way_id)) {
            $Financial->where('payment_way_id', $request->payment_way_id);
        }
        if (isset($request->send_date_from)) {
            $Financial = $Financial->where('date', '>=', $request->send_date_from);
        }
        if (isset($request->send_date_to)) {
            $Financial = $Financial->where('date', '<=', $request->send_date_to);
        }
        
        if (isset($request->pay_date_from)) {
            $Financial = $Financial->where('pay_date', '>=', $request->pay_date_from);
        }
        if (isset($request->pay_date_to)) {
            $Financial = $Financial->where('pay_date', '<=', $request->pay_date_to);
        }
     
        return response()->json($Financial->groupBy('provider_id')->groupBy('order_payment_way_id')->groupBy('status_id')
                ->groupBy('pay_date')
                ->selectRaw('*, sum(price) as price')->orderBy('pay_date', 'desc')->paginate($request->perPage));
    }
    
    public function store(Request $request){
        if(isset($request->bill)){
             $image = strip_tags($request->bill);
        $arr = explode(",", $image);
        $base64 = str_replace($arr[0], '+', $arr[1]);
        $s = strrpos($arr[0], '/') + 1;
        $e = strrpos($arr[0], ';');
        $extension = substr($arr[0], $s, $e - $s);

        if ($extension === 'jpeg' || $extension === 'jpg') {
            $extension = 'jpg';
        } else if ($extension !== 'png') {
            $response["status"] = false;
            $response["data"] = "Unsupported File Type.";
            return response()->json($response);
        }

        $base2image = ImageResize::createFromString(base64_decode($base64));

        $tmp = '/images/'
                . '_'
                . round(microtime(true) * 1000)
                . '.'
                . $extension;
        $base2image->save(base_path() . '/storage'.$tmp);

        $request->bill = $tmp;
        }
        
        $payments = Financial::where('provider_id', $request->provider_id)
                ->where('order_payment_way_id', $request->order_payment_way_id)
                ->where('status_id',1)->get();
        
        foreach($payments as $pay){
            $pay->payment_way_id = $request->payment_way_id;
            $pay->status_id = 2;
            $pay->bill = $request->bill;
            $pay->pay_date = date('Y-m-d');
            $pay->save();
        }
        
        $response["status"] = true;
        $response["message"] = 'تمت عملية التسديد بنجاح';
        return response()->json($response);
        
    }
}
