<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Items;
use App\Models\ConstantsTable;
use Illuminate\Http\Request;

/**
 * Description of ItemsController
 *
 * @author Amna Hamadaqa
 */
class ItemsController extends Controller {
    public function index(Request $request) {
        $Items = new Items();
        if (isset($request->name)) {
            $Items = $Items->where('name', 'like', '%' . $request->name . '%');
        }
       
        return response()->json($Items->with('Type')->paginate($request->perPage));
    }
    
    public function store(Request $request) {
            $data = $request->toArray();
            
            $items = Items::where('item_order', $data['item_order'])->count();
        if ($items>0) {
            $response["status"] = false;
            $response["message"] = 'لايمكن تكرار نفس الترتيب لأكثر من صنف !';
            return response()->json($response);
        }
            
            if(!isset($data['image'])){
              $response["status"] = false;
        $response["message"] = "يجب تحميل الصورة ";
        return response()->json($response);  
            }
            
        $image = strip_tags($data['image']);

        if (empty($image)) {
            $response["status"] = false;
            $response["data"] = "يجب تحميل الصورة ";
            return response()->json($response);
        }
        $arr = explode(",", $image);
        $base64 = str_replace($arr[0], '+', $arr[1]);

        $s = strrpos($arr[0], '/') + 1;
        $e = strrpos($arr[0], ';');
        $extension = substr($arr[0], $s, $e - $s);

        if ($extension === 'jpeg' || $extension === 'jpg') {
            $extension = 'jpg';
        } else if ($extension !== 'png') {
            $response["status"] = false;
            $response["data"] = "Unsupported File Type.";
            return response()->json($response);
        }

        $base2image = ImageResize::createFromString(base64_decode($base64));
        
        

        $tmp = '/images/'
                . '_'
                . round(microtime(true) * 1000)
                . '.'
                . $extension;
        $base2image->save(base_path() . '/storage'.$tmp);

        $data['image'] = $tmp;
        Items::create($data);
        $response["status"] = true;
        $response["message"] = 'تم الإضافة بنجاح';
        return response()->json($response);
    }
    
        public function show($id) {

        return response()->json(Items::find($id));
    }
    
    public function update(Request $request, $id) {
        $data = $request->data;
        
        $items = Items::where('item_order', $data['item_order'])->where('id','!=',$id)->count();
        if ($items>0) {
            $response["status"] = false;
            $response["message"] = 'لايمكن تكرار نفس الترتيب لأكثر من صنف !';
            return response()->json($response);
        }
        
        $image = $data['image'];
        
        $arr = explode(",", $image);
        //dd($arr);
        if(count($arr) > 1){
        $base64 = str_replace($arr[0], '+', $arr[1]);

        $s = strrpos($arr[0], '/') + 1;
        $e = strrpos($arr[0], ';');
        $extension = substr($arr[0], $s, $e - $s);

        if ($extension === 'jpeg' || $extension === 'jpg') {
            $extension = 'jpg';
        } else if ($extension !== 'png') {
            $response["status"] = false;
            $response["data"] = "Unsupported File Type.";
            return response()->json($response);
        }

        $base2image = ImageResize::createFromString(base64_decode($base64));
        
        

        $tmp = '/images/'
                . '_'
                . round(microtime(true) * 1000)
                . '.'
                . $extension;
        $base2image->save(base_path() . '/storage'.$tmp);

        $data['image'] = $tmp;
        }
        $type = Items::find($id);

        $type->image = $data['image'];
        $type->name = $data['name'];
        $type->item_order = $data['item_order'];

        $type->save();
        $response["status"] = true;
        $response["message"] = 'تم التعديل بنجاح';
        return response()->json($response);
    }
    
    public function getOptions() {
        $data['types'] = ConstantsTable::where('type', 2)->get();
        return response()->json($data);
    }
    
    public function destroy($id) {
        Items::destroy($id);
        $response["status"] = true;
        $response["message"] = 'تم الحذف بنجاح';
        return response()->json($response);
    }
}
