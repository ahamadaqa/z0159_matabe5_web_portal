<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\providerProducts;
use App\Models\Items;
use App\Models\Providers;
use App\Models\ProviderEvaluations;
use App\Models\ProductImages;
use Illuminate\Http\Request;

include 'ImageResize.php';

/**
 * Description of ProductsController
 *
 * @author Amna
 */
class ProductsController extends Controller {

    public function show($id, Request $request) {
        $product = providerProducts::with(['Item', 'Provider' => function($q) {
                        $q->select('id', 'name', 'mobile', 'phone');
                    }])->find($id);
        if ($product) {
//            if ($product->image != null && $product->image != "") {
//                $storage = storage_path(ltrim($product->image, '/'));
//                $storage = explode('public_html', $storage);
//                $product->image = 'http://' . $request->getHttpHost() . $storage[1];
//            }
            unset($product->image);
                $images = ProductImages::where('product_id', $product->id)->get();
                if(count($images) > 0){
                foreach($images as $image){
                if ($image->image != null && $image->image != "") {
                    $storage = storage_path(ltrim($image->image, '/'));
                    $storage = explode('public_html', $storage);
                    $image->image = 'http://' . $request->getHttpHost() . $storage[1];
                }    
                }
                $product->images = $images;
                } else {
                $product->images = [];   
                }
            $ProviderEvaluationsSum = ProviderEvaluations::where('provider_id', $product->provider_id)->sum('points');
            $ProviderEvaluationscount = ProviderEvaluations::where('provider_id', $product->provider_id)->count();

            if ($ProviderEvaluationscount == 0)
                $product->Provider->evaluation = 0;
            else
                $product->Provider->evaluation = $ProviderEvaluationsSum / $ProviderEvaluationscount;
            
            $product->is_favorite = 1;
            $product->share_link = $request->root()."/viweProduct".$product->id.".html";
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = 'تم العثور على البيانات المطلوبة';
            $response['result_object'] = [$product];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'لا يوجد نتائج';
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function providerProducts($id, Request $request) {
        $data = json_decode($request->getContent(), true);
        $provider = Providers::with(['City', 'Country'])->where('status_id', 1);
        if (isset($data['provider_name'])) {
            $provider = $provider->where('name', 'like', '%' . $data['provider_name'] . '%');
        }
        $provider = $provider->find($id);


        if ($provider) {
            if ($provider->image != null && $provider->image != "") {
                $storage = storage_path(ltrim($provider->image, '/'));
                $storage = explode('public_html', $storage);
                $provider->image = 'http://' . $request->getHttpHost() . $storage[1];
            }
            $points = ProviderEvaluations::where('provider_id', $id)->sum('points');
            $count = ProviderEvaluations::where('provider_id', $id)->count();
            $avg = $points / $count;
            $providerData = ['id' => $provider->id, 'name' => $provider->name, 'image' => $provider->image, 'county' => $provider->Country->Name,
                'city' => $provider->City->Name, 'loc_lat' => $provider->loc_lat, 'loc_long' => $provider->loc_long, 'points' => $avg];

            $products = providerProducts::where('provider_id', $id);
            if (isset($data['delivered_date'])) {
                $products = $products->where('delivered_date', $data['delivered_date']);
            }
            if (isset($data['product_name'])) {
                $products = $products->where('name', 'like', '%' . $data['product_name'] . '%');
            }
            $products = $products->paginate($request['per_page']);
            foreach ($products as $product) {
                if ($product->image != null && $product->image != "") {
                    $storage = storage_path(ltrim($product->image, '/'));
                    $storage = explode('public_html', $storage);
                    $product->image = 'http://' . $request->getHttpHost() . $storage[1];
                }
            }
            //$product->image = storage_path($product->image);
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = 'تم العثور على البيانات المطلوبة';
            $response['result_object'] = [['provider' => $providerData, 'products' => $products]];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'لا يوجد نتائج';
            $response['result_object'] = [];
        }

        return response()->json($response);
    }

    public function categoryProducts($id, Request $request) {
        $data = json_decode($request->getContent(), true);
        $products = Items::where('item_type', $id);
        if (isset($data['product_name'])) {
            $products = $products->where('name', 'like', '%' . $data['product_name'] . '%');
        }
        $products = $products->paginate($request['per_page']);

        foreach ($products as $product) {
            if ($product->image != null && $product->image != "") {
                $storage = storage_path(ltrim($product->image, '/'));
                $storage = explode('public_html', $storage);
                $product->image = 'http://' . $request->getHttpHost() . $storage[1];
            }
        }

        if (count($products) > 0) {
            $response['code'] = 1;
            $response['result_num'] = count($products);
            $response['result_msg'] = 'تم العثور على البيانات المطلوبة';
            $response['result_object'] = $products;
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'لا يوجد نتائج';
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

}
