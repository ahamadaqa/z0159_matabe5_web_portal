<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Notifications;
use App\Models\Customers;
use App\Models\Providers;
use Illuminate\Http\Request;
use JWTAuth;

//use Tymon\JWTAuth\JWTAuthExceptions\JWTException;

/**
 * Description of NotificationsController
 *
 * @author Amna
 */
class NotificationsController extends Controller {

    public function index(Request $request) {
        $Notifications = Notifications::with('Admin');
        if (isset($request->name)) {
            $Notifications = $Notifications->whereHas('Admin', function($x) use ($request) {
                $x->where('name', 'like', '%' . $request->name . '%');
            });
        }
        if (isset($request->from_date)) {
            $Notifications = $Notifications->whereDate('date', '>=', $request->from_date);
        }
        if (isset($request->to_date)) {
            $Notifications = $Notifications->whereDate('date', '<=', $request->to_date);
        }
        return response()->json($Notifications->paginate($request->perPage));
    }

    public function store(Request $request) {
        $user = JWTAuth::parseToken()->authenticate();
        //return $user->toArray();
        $data = $request->data;
        $target = [];
        if (isset($data['target4']) && $data['target4'] == true) {
            $data['target'] = 4;
            $user = $data['user_id'];
            if ($user['type'] == 1)
                $data['customer_id'] = $user['id'];
            else
                $data['provider_id'] = $user['id'];
        } else {

            if (isset($data['target1']) && $data['target1'] == true) {
                $target[] = 1;
            } if (isset($data['target2']) && $data['target2'] == true) {
                $target[] = 2;
            } if (isset($data['target3']) && $data['target3'] == true) {
                $target[] = 3;
            }
        }

        $data['notify_text'] = $data['notify_text'];
        $data['admin_id'] = 1;
        $data['date'] = date('Y-m-d');
        if (count($target) > 0){
            foreach ($target as $trgt) {
                $data['target'] = $trgt;
            Notifications::create($data);
            }
        } else {
            Notifications::create($data);
        }
        $response["status"] = true;
        $response["message"] = 'تم الإضافة بنجاح';
        return response()->json($response);
    }

    public function destroy($id) {
        Notifications::destroy($id);
        $response["status"] = true;
        $response["message"] = 'تم الحذف بنجاح';
        return response()->json($response);
    }

    public function selectOptions() {
        $customers = Customers::get();
        $providers = Providers::get();
        $users = [];
        foreach ($customers as $customer) {
            $users[] = ['id' => $customer->id, 'name' => $customer->full_name, 'type' => 1];
        }

        foreach ($providers as $provider) {
            $users[] = ['id' => $provider->id, 'name' => $provider->name, 'type' => 2];
        }

        return response()->json($users);
    }

}
