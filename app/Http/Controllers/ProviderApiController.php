<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Providers;
use App\Models\Orders;
use App\Models\OrdersProviderRequests;
use App\Models\Customers;
use App\Models\CustomerEvaluations;
use App\Models\Financial;
use App\Models\Notifications;
use App\Models\providerItems;
use App\Models\providerProducts;
use App\Models\ProductImages;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\JWTAuthExceptions\JWTException;
use DB;

/**
 * Description of ProviderApiController
 *
 * @author aymh_
 */
class ProviderApiController extends Controller {

    function __construct() {
        \Config::set('jwt.user', 'App\Models\Providers');
        \Config::set('auth.providers.users.table', 'providers');
    }

    public function login(Request $request) {
        $credentials = $request->only('mobile', 'password');
        try {
            // verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                $response["code"] = -1;
                $response['result_num'] = 0;
                $response["result_msg"] = 'اسم المستخدم أو كلمة المرور خطأ';
                $response['result_object'] = [];
                return response()->json($response, 200);
            }
        } catch (JWTException $e) {
            // something went wrong
            $response["code"] = -2;
            $response['result_num'] = 0;
            $response["result_msg"] = 'حدث خطأ ما، لم يتم انشاء التوكن';
            $response['result_object'] = [];
            return response()->json($response, 200);
        }

        $user = Providers::where('mobile', $request->mobile)->where('type', 1)->first();
        if ($user) {
            unset($user->password);

            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = 'تم تسجيل الدخول بنجاح';
            $response['result_object'] = $user;
            $response['result_object']['token'] = $token;
        } else {
            $response["code"] = -1;
            $response['result_num'] = 0;
            $response["result_msg"] = 'اسم المستخدم أو كلمة المرور خطأ';
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function myOrders(Request $request) {
        $all_orders = [];
        $data = json_decode($request->getContent(), true);
        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;

        $orders = Orders::with(['Customer', 'Provider', 'Status', 'Product', 'Product.Item.Type']);
        if (isset($data['status_id'])) {
            $orders = $orders->where('order_status', $data['status_id']);
        }
        if (isset($data['order_id'])) {
            $orders = $orders->where('order_id', $data['order_id']);
        }
        if (isset($data['customer_name'])) {
            $orders = $orders->whereHas('Customer', function($q) use ($data) {
                $q->where('full_name', 'like', '%' . $data['customer_name'] . '%');
            });
        }
        if (isset($data['product_name'])) {
            $orders = $orders->whereHas('Product', function($q) use ($data) {
                $q->where('name', 'like', '%' . $data['product_name'] . '%');
            });
        }
        if (isset($data['category_id'])) {
            $orders = $orders->whereHas('Product.Item', function($q) use ($data) {
                $q->where('item_type', $data['category_id']);
            });
        }
        $orders = $orders->where('provider_id', $provider_id)->orderBy('id', 'desc')->paginate($request['per_page']);
        $i = 0;
        foreach ($orders as $order) {
            if ($order->Customer->image != null) {
                $storage = storage_path(ltrim($order->Customer->image, '/'));
                $storage = explode('public_html', $storage);
                $profileImg = 'http://' . $request->getHttpHost() . $storage[1];
            } else {
                $profileImg = null;
            }
            $all_orders[$i] = [
                'order_id' => $order->id,
                'product_name' => $order->Product->name,
                'customer_name' => $order->Customer->full_name,
                'order_date' => $order->order_date,
                'order_no' => $order->order_id,
                'order_status' => $order->Status->name,
                'order_category' => $order->Product->Item->Type->name,
                'customer_image' => $profileImg
            ];
            $i++;
        }
        $count = count($orders);
        if ($count > 0) {
            $response["code"] = 1;
            $response['result_num'] = $count;
            $response["result_msg"] = "تم العثور على البيانات المطلوبة";
            $response['result_object'] = $all_orders;
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function acceptCancelOrder(Request $request) {
        $data = json_decode($request->getContent(), true);
        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;
        $order = Orders::find($data['order_id']);
        if ($order) {
            if ($provider_id != $order->provider_id) {
                $response['code'] = -2;
                $response['result_num'] = 0;
                $response['result_msg'] = "غير مسموح لك بقبول أو رفض الطلب";
                $response['result_object'] = '';
                return response()->json($response);
            }
            if ($data['status_id'] != 2 && $data['status_id'] != 7) {
                $response['code'] = -3;
                $response['result_num'] = 0;
                $response['result_msg'] = "الحالة المدخلة غير صحيحة";
                $response['result_object'] = [];
                return response()->json($response);
            }
            if ($order->order_status != 1) {
                $response['code'] = -4;
                $response['result_num'] = 0;
                $response['result_msg'] = "يجب أن تكون حالة الطلب جديد حتى يمكن قبوله أو رفضه";
                $response['result_object'] = [];
                return response()->json($response);
            }
            $order->order_status = $data['status_id'];
            $order->save();
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = "تم تغيير الحالة بنجاح";
            $response['result_object'] = [$order];
            return response()->json($response);
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "الطلب غير موجود";
            $response['result_object'] = [];
            return response()->json($response);
        }
    }

    public function finishOrder(Request $request) {
        $data = json_decode($request->getContent(), true);
        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;
        $order = Orders::find($data['order_id']);
        if ($order) {
            if ($provider_id != $order->provider_id) {
                $response['code'] = -2;
                $response['result_num'] = 0;
                $response['result_msg'] = "غير مسموح لك بتغيير حالة الطلب";
                $response['result_object'] = [];
                return response()->json($response);
            }
            if ($data['status_id'] != 3) {
                $response['code'] = -3;
                $response['result_num'] = 0;
                $response['result_msg'] = "الحالة المدخلة غير صحيحة";
                $response['result_object'] = [];
                return response()->json($response);
            }
            if ($order->order_status != 2) {
                $response['code'] = -4;
                $response['result_num'] = 0;
                $response['result_msg'] = "يجب أن تكون حالة الطلب جاري التحضير";
                $response['result_object'] = [];
                return response()->json($response);
            }
            $order->order_status = $data['status_id'];
            $order->save();
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = "تم تغيير الحالة بنجاح";
            $response['result_object'] = [$order];
            return response()->json($response);
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "الطلب غير موجود";
            $response['result_object'] = [];
            return response()->json($response);
        }
    }

    public function chooseDelegate(Request $request) {
        $data = json_decode($request->getContent(), true);
        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;
        $order = Orders::find($data['order_id']);
        if ($order) {
            if ($provider_id != $order->provider_id) {
                $response['code'] = -2;
                $response['result_num'] = 0;
                $response['result_msg'] = "غير مسموح لك باختيار مندوب";
                $response['result_object'] = [];
                return response()->json($response);
            }
            if ($order->order_status != 3) {
                $response['code'] = -3;
                $response['result_num'] = 0;
                $response['result_msg'] = "يجب أن تكون حالة الطلب تم التحضير";
                $response['result_object'] = [];
                return response()->json($response);
            }
            $delegate = Providers::where('type', 2)->find($data['delegate_id']);
            if (!$delegate) {
                $response['code'] = -4;
                $response['result_num'] = 0;
                $response['result_msg'] = "المندوب غير موجود";
                $response['result_object'] = [];
                return response()->json($response);
            }
            //$order->order_status = 4;
            $order->delegate_id = $data['delegate_id'];
            $order->save();
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = "تم اختيار المندوب";
            $response['result_object'] = [$order];
            return response()->json($response);
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "الطلب غير موجود";
            $response['result_object'] = [];
            return response()->json($response);
        }
    }

    public function evaluateCustomer(Request $request) {
        $data = json_decode($request->getContent(), true);
        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;

        if (!$userAuth) {
            $response['code'] = -2;
            $response['result_num'] = 0;
            $response['result_msg'] = "التوكن غير صحيح، يرجى إعادة تسجيل الدخول";
            $response['result_object'] = [];
            return response()->json($response);
        }

        if ($data['customer_id']) {
            $provider = Customers::find($data['customer_id']);
            if (!$provider) {
                $response['code'] = -3;
                $response['result_num'] = 0;
                $response['result_msg'] = "العميل غير موجود يرجى التأكد من المدخلات";
                $response['result_object'] = [];
                return response()->json($response);
            }
        }

        $data['provider_id'] = $provider_id;
        $evaluation = CustomerEvaluations::create($data);
        if ($evaluation) {
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = "تم تقييم العميل بنجاح";
            $response['result_object'] = [$evaluation];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "حدثت مشكلة أثناء التقييم، يرجى إعادة التقييم";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function notifications(Request $request) {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;

        $myNotifications = Notifications::where('provider_id', $provider_id)
                        ->orderBy('date', 'desc')->paginate($request['per_page']);

        if (count($myNotifications) > 0) {
            $response["code"] = 1;
            $response['result_num'] = count($myNotifications);
            $response["result_msg"] = "تم العثور على البيانات المطلوبة";
            $response['result_object'] = $myNotifications;
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function financial() {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;

        $financial = Financial::where('provider_id', $provider_id)->get();

        if (count($financial) > 0) {
            $response["code"] = 1;
            $response['result_num'] = count($financial);
            $response["result_msg"] = "تم العثور على البيانات المطلوبة";
            $response['result_object'] = $financial;
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function myItems(Request $request) {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;

        if (!$userAuth) {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "التوكن غير صحيح، يرجى إعادة تسجيل الدخول";
            $response['result_object'] = [];
            return response()->json($response);
        }

        $data = json_decode($request->getContent(), true);
        $items = $data['items'];

        providerItems::where('provider_id', $provider_id)->delete();

        foreach ($items as $item) {
            providerItems::create(['provider_id' => $provider_id, 'item_id' => $item]);
        }

        $response['code'] = 1;
        $response['result_num'] = count($items);
        $response['result_msg'] = "تم تعديل الأصناف بنجاح";
        $response['result_object'] = providerItems::where('provider_id', $provider_id)->get();
        return response()->json($response);
    }

    public function myAccount(Request $request) {
        $data = json_decode($request->getContent(), true);

        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;

        $provider = Providers::where('type', 1)->where('mobile', $data['mobile'])->first();
        if ($provider) {
            $response['code'] = -2;
            $response['result_num'] = 0;
            $response['result_msg'] = "رقم الجوال مدخل مسبقاً";
            $response['result_object'] = [];
            return response()->json($response);
        }

        $myAccount = Providers::find($provider_id);
        $myAccount->name = $data['name'];
        $myAccount->idno = $data['idno'];
        $myAccount->mobile = $data['mobile'];
        $myAccount->email = $data['email'];
        $myAccount->country_id = $data['country'];
        $myAccount->city_id = $data['city'];
        $myAccount->gender = $data['gender'];
        $myAccount->birthdate = $data['birthdate'];
        $myAccount->loc_lat = $data['latitude'];
        $myAccount->loc_long = $data['longitude'];

        $myAccount->save();
        if ($myAccount) {
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = "تم التعديل بنجاح";
            $response['result_object'] = [$myAccount];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "الحساب غير موجود";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function profile(Request $request) {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;
        $provider = Providers::find($provider_id);

        if ($provider) {
            unset($provider->password);
            if ($provider->image != null && $provider->image != "") {
                $storage = storage_path(ltrim($provider->image, '/'));
                $storage = explode('public_html', $storage);
                $provider->image = 'http://' . $request->getHttpHost() . $storage[1];
            }

            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = 'تم العثور على البيانات المطلوبة';
            $response['result_object'] = [$provider];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'لا توجد نتائج';
            $response['result_object'] = [];
        }

        return response()->json($response);
    }

    public function addProvider(Request $request) {
        $data = json_decode($request->getContent(), true);

        $provider = Providers::where('type', 1)->where('mobile', $data['mobile'])->first();
        if ($provider) {
            $response['code'] = -2;
            $response['result_num'] = 0;
            $response['result_msg'] = "رقم الجوال مدخل مسبقاً";
            $response['result_object'] = [];
            return response()->json($response);
        }

        $data['type'] = 1;
        $myAccount = Providers::create($data);

        if ($myAccount) {
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = "تم تسجيل الحساب بنجاح";
            $response['result_object'] = [$myAccount];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "حدثت مشكلة أثناء تسجيل الحساب";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function myProducts(Request $request) {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;

        $provider = Providers::find($provider_id);
        if ($provider) {
            $products = providerProducts::where('provider_id', $provider_id);
            if (isset($request['active']))
                $products = $products->where('active', $request['active']);

            $products = $products->orderBy('id', 'desc')->paginate($request['per_page']);

            if (count($products) > 0) {
                foreach ($products as $product) {
                    if ($product->image != null && $product->image != "") {
                        $storage = storage_path(ltrim($product->image, '/'));
                        $storage = explode('public_html', $storage);
                        $product->image = 'http://' . $request->getHttpHost() . $storage[1];
                    }
                }
                $response['code'] = 1;
                $response['result_num'] = count($products);
                $response['result_msg'] = 'تم العثور على البيانات المطلوبة';
                $response['result_object'] = [$products];
            } else {
                $response['code'] = -2;
                $response['result_num'] = 0;
                $response['result_msg'] = 'لا توجد نتائج';
                $response['result_object'] = [];
            }
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'المزود غير موجود';
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function viewProduct($id, Request $request) {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;

        $provider = Providers::find($provider_id);
        if ($provider) {
            $product = providerProducts::with('Item')->where('active', 1)->where('provider_id', $provider_id)->find($id);
            if ($product) {
                unset($product->image);
                $images = ProductImages::where('product_id', $product->id)->get();
                if (count($images) > 0) {
                    foreach ($images as $image) {
                        if ($image->image != null && $image->image != "") {
                            $storage = storage_path(ltrim($image->image, '/'));
                            $storage = explode('public_html', $storage);
                            $image->image = 'http://' . $request->getHttpHost() . $storage[1];
                        }
                    }
                    $product->images = $images;
                } else {
                    $product->images = [];
                }
                $product->payments_count = Orders::where('product_id', $product->id)->where('order_status', 6)->count();
                $response['code'] = 1;
                $response['result_num'] = 1;
                $response['result_msg'] = 'تم العثور على البيانات المطلوبة';
                $response['result_object'] = [$product];
            } else {
                $response['code'] = -2;
                $response['result_num'] = 0;
                $response['result_msg'] = 'لا توجد نتائج';
                $response['result_object'] = [];
            }
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'المزود غير موجود';
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function stopProduct($id) {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;

        $provider = Providers::find($provider_id);
        if ($provider) {
            $product = providerProducts::find($id);
            if ($product) {
                if ($product->provider_id != $provider_id) {
                    $response['code'] = -3;
                    $response['result_num'] = 0;
                    $response['result_msg'] = 'لا يمكنك إيقاف منتج ليس لك';
                    $response['result_object'] = [];
                    return response()->json($response);
                }

                if ($product->active == 0) {
                    $response['code'] = -4;
                    $response['result_num'] = 0;
                    $response['result_msg'] = 'لا يمكنك إيقاف منتج موقوف مسبقاً';
                    $response['result_object'] = [];
                    return response()->json($response);
                }

                $product->active = 0;
                $product->save();
                $response['code'] = 1;
                $response['result_num'] = 0;
                $response['result_msg'] = 'تم إيقاف المنتج بنجاح';
                $response['result_object'] = [$product];
            } else {
                $response['code'] = -2;
                $response['result_num'] = 0;
                $response['result_msg'] = 'المنتج غير موجود';
                $response['result_object'] = [];
            }
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'المزود غير موجود';
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function editProduct($id, Request $request) {
        $data = $request->toArray();
        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;
        $product = providerProducts::find($id);
        if ($product) {
            DB::beginTransaction();
            if ((!isset($data['name']) || $data['name'] == '') ||
                    (!isset($data['price']) || $data['price'] == '') ||
                    (!isset($data['item_id']) || $data['item_id'] == '') ||
                    (!isset($data['quantity']) || $data['quantity'] == '')) {
                $response["code"] = -2;
                $response['result_num'] = 0;
                $response["result_msg"] = 'يجب ادخال جميع الحقول المطلوبة';
                $response['result_object'] = [];
                return response()->json($response);
            }
            if ($product->provider_id != $provider_id) {
                $response["code"] = -3;
                $response['result_num'] = 0;
                $response["result_msg"] = ' لا يمكنك تعديل منتج ليس لك !';
                $response['result_object'] = [];
                return response()->json($response);
            }
            $product->name = $data['name'];
            $product->price = $data['price'];
            $product->item_id = $data['item_id'];
            $product->quantity = $data['quantity'];
            if (isset($data['notes']))
                $product->notes = $data['notes'];

            $product->save();
            if (isset($data['images'])) {
                ProductImages::where('product_id', $id)->delete();
                foreach ($data['images'] as $key => $file) {
                    $extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                    $tmp = '/images/'
                            . '_'
                            . round(microtime(true) * 1000)
                            . '.'
                            . $extension;
                    if (move_uploaded_file($_FILES['images']['tmp_name'][$key], base_path() . '/storage' . $tmp)) {
                        $image['product_id'] = $product->id;
                        $image['image'] = $tmp;

                        ProductImages::create($image);
                    }
                }
            }

            DB::commit();
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = 'تم التعديل بنجاح';
            $response['result_object'] = [$product];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'المنتج غير موجود';
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function addProduct(Request $request) {
        $data = $request->toArray();
        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;
        $provider = Providers::find($provider_id);
        if ($provider) {
            $data['provider_id'] = $provider_id;
            $data['active'] = 1;
            DB::beginTransaction();
            if ((!isset($data['name']) || $data['name'] == '') ||
                    (!isset($data['price']) || $data['price'] == '') ||
                    (!isset($data['item_id']) || $data['item_id'] == '') ||
                    (!isset($data['quantity']) || $data['quantity'] == '')) {
                $response["code"] = -2;
                $response['result_num'] = 0;
                $response["result_msg"] = 'يجب ادخال جميع الحقول المطلوبة';
                $response['result_object'] = [];
                return response()->json($response);
            }
            $product = providerProducts::create($data);
            if ($product) {

                foreach ($data['images'] as $key => $file) {
                    $extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                    $tmp = '/images/'
                            . '_'
                            . round(microtime(true) * 1000)
                            . '.'
                            . $extension;
                    if (move_uploaded_file($_FILES['images']['tmp_name'][$key], base_path() . '/storage' . $tmp)) {
                        $image['product_id'] = $product->id;
                        $image['image'] = $tmp;

                        ProductImages::create($image);
                    }
                }
            }
            DB::commit();
            if ($product) {
                $response['code'] = 1;
                $response['result_num'] = 1;
                $response['result_msg'] = 'تم الإضافة بنجاح';
                $response['result_object'] = [$product];
            } else {
                $response['code'] = -3;
                $response['result_num'] = 0;
                $response['result_msg'] = 'حدث مشكلة أثناء عملبة الإضافة';
                $response['result_object'] = [];
            }
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'المزود غير موجود';
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function applayToOrder(Request $request) {
        $data = json_decode($request->getContent(), true);
        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;
        $provider = Providers::find($provider_id);
        if (!$provider) {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'المزود غير موجود، يرجى إعادة تسجيل الدخول مرة أخرى';
            $response['result_object'] = [];
            return response()->json($response);
        }

        $order = Orders::find($data['order_id']);
        if (!$order) {
            $response['code'] = -2;
            $response['result_num'] = 0;
            $response['result_msg'] = 'الطلب غير موجود، يرجى التأكد من المدخلات';
            $response['result_object'] = [];
            return response()->json($response);
        }

        if ($order->order_status != 1) {
            $response['code'] = -3;
            $response['result_num'] = 0;
            $response['result_msg'] = 'لا يمكنك التقدم للطلب لأن حالة الطلب غير جديد';
            $response['result_object'] = [];
            return response()->json($response);
        }

        $data['provider_id'] = $provider_id;
        $data['status_id'] = 1;
        $orderRequest = OrdersProviderRequests::create($data);
        if ($orderRequest) {
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = 'تم إضافة الطلب بنجاح';
            $response['result_object'] = [$orderRequest];
        } else {
            $response['code'] = -4;
            $response['result_num'] = 0;
            $response['result_msg'] = 'حدثت مشكلة أثناء عملية الإضافة';
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function startOrder($id) {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;
        $order = Orders::find($id);
        if ($order) {
            if ($provider_id != $order->provider_id) {
                $response['code'] = -2;
                $response['result_num'] = 0;
                $response['result_msg'] = "غير مسموح لك بتغيير حالة الطلب";
                $response['result_object'] = [];
                return response()->json($response);
            }
            if ($order->order_status != 11) {
                $response['code'] = -3;
                $response['result_num'] = 0;
                $response['result_msg'] = "يجب أن تكون حالة الطلب تم القبول حتى تتمكن من بدء التحضير  ";
                $response['result_object'] = [];
                return response()->json($response);
            }
            $order->order_status = 2;
            $order->save();
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = "تم تغيير الحالة بنجاح";
            $response['result_object'] = [$order];
            return response()->json($response);
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "الطلب غير موجود";
            $response['result_object'] = [];
            return response()->json($response);
        }
    }
    
    public function deliveredOrder($id) {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $provider_id = $userAuth->id;
        $order = Orders::find($id);
        if ($order) {
            if ($provider_id != $order->provider_id) {
                $response['code'] = -2;
                $response['result_num'] = 0;
                $response['result_msg'] = "غير مسموح لك بتغيير حالة الطلب";
                $response['result_object'] = [];
                return response()->json($response);
            }
            if ($order->order_status != 3) {
                $response['code'] = -3;
                $response['result_num'] = 0;
                $response['result_msg'] = "يجب أن تكون حالة الطلب تم التحضير حتى تتمكن من تغييرها الي تم التسليم  ";
                $response['result_object'] = [];
                return response()->json($response);
            }
            $order->order_status = 5;
            $order->save();
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = "تم تغيير الحالة بنجاح";
            $response['result_object'] = [$order];
            return response()->json($response);
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "الطلب غير موجود";
            $response['result_object'] = [];
            return response()->json($response);
        }
    }

}
