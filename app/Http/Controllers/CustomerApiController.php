<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Customers;
use App\Models\Providers;
use App\Models\CustomerEvaluations;
use App\Models\CustomerPayments;
use App\Models\Orders;
use App\Models\OrdersProviderRequests;
use App\Models\ConfirmOrders;
use App\Models\providerProducts;
use App\Models\ProviderEvaluations;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\JWTAuthExceptions\JWTException;
use Hash;

/**
 * Description of CustomerApiController
 *
 * @author Amna
 */
class CustomerApiController extends Controller {

    function __construct() {
        \Config::set('jwt.user', 'App\Models\Customers');
        \Config::set('auth.providers.users.table', 'customers');
    }

    //customer login
    public function login(Request $request) {
        \Config::set('jwt.user', 'App\Models\Providers');
        \Config::set('auth.providers.users.table', 'providers');
        $credentials = $request->only('mobile');
        $user = Providers::where('mobile', $request->mobile)->first();
        if ($user) {
            try {

                // verify the credentials and create a token for the user
                if (!$token = JWTAuth::fromUser($user)) {
                    $response["code"] = -1;
                    $response['result_num'] = 0;
                    $response["result_msg"] = 'اسم المستخدم أو كلمة المرور خطأ';
                    $response['result_object'] = [];
                }
            } catch (JWTException $e) {
                // something went wrong
                $response["code"] = -2;
                $response['result_num'] = 0;
                $response["result_msg"] = 'حدث خطأ ما، لم يتم انشاء التوكن';
                $response['result_object'] = [];
            }

            unset($user->password);
            if ($user->type == 1)
                $user->type = "Provider";
            else if ($user->type == 1)
                $user->type = "Delegate";

            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = 'تم تسجيل الدخول بنجاح';
            $response['result_object'] = $user;
            $response['result_object']['token'] = $token;
        } else {
            \Config::set('jwt.user', 'App\Models\Customers');
            \Config::set('auth.providers.users.table', 'customers');
            $credentials = $request->only('mobile');
            $user = Customers::where('mobile', $request->mobile)->where('status_id', 1)->first();
            if ($user) {
                try {
                    // verify the credentials and create a token for the user
                    if (!$token = JWTAuth::fromUser($user)) {
                        $response["code"] = -1;
                        $response['result_num'] = 0;
                        $response["result_msg"] = 'اسم المستخدم أو كلمة المرور خطأ';
                        $response['result_object'] = [];
                        return response()->json($response, 200);
                    }
                } catch (JWTException $e) {
                    // something went wrong
                    $response["code"] = -2;
                    $response['result_num'] = 0;
                    $response["result_msg"] = 'حدث خطأ ما، لم يتم انشاء التوكن';
                    $response['result_object'] = [];
                    return response()->json($response, 200);
                }

                unset($user->password);
                $user->type = "customer";

                $response["code"] = 1;
                $response['result_num'] = 1;
                $response["result_msg"] = 'تم تسجيل الدخول بنجاح';
                $response['result_object'] = [$user];
                $response['result_object']['token'] = $token;
            } else {
                $response["code"] = -3;
                $response['result_num'] = 0;
                $response["result_msg"] = 'حسابك موقوف، يرجى التواصل مع مدير التطبيق لتفعيل حسابك';
                $response['result_object'] = [];
            }
        }
        return response()->json($response);
    }

    public function addCustomer(Request $request) {
        $data = $request->toArray();
        $oldCustomer = Customers::where('status_id', 1)->where('mobile', $data['mobile'])->first();
        if ($oldCustomer) {
            $response['code'] = -2;
            $response['result_num'] = 0;
            $response['result_msg'] = "رقم الجوال مدخل مسبقاً";
            $response['result_object'] = [];
            return response()->json($response);
        }

        if (isset($data['id_image'])) {
            $extension = strtolower(pathinfo($_FILES['id_image']['name'], PATHINFO_EXTENSION));
            $tmp = '/images/'
                    . '_'
                    . round(microtime(true) * 1000)
                    . '.'
                    . $extension;
            if (move_uploaded_file($_FILES['id_image']['tmp_name'], base_path() . '/storage' . $tmp)) {
                $data['id_image'] = $tmp;
            }
        }
        $customer = Customers::create($data);
        if ($customer) {
            if ($customer->id_image != null && $customer->id_image != "") {
                $storage = storage_path(ltrim($customer->id_image, '/'));
                $storage = explode('public_html', $storage);
                $customer->id_image = 'http://' . $request->getHttpHost() . $storage[1];
            }
            $response["code"] = 1;
            $response['result_num'] = 1;
            $response["result_msg"] = "تم تسجيل الحساب بنجاح";
            $response['result_object'] = [$customer];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "حدثت مشكلة أثناء تسجيل الحساب";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function addOrder(Request $request) {
        $data = json_decode($request->getContent(), true);
        $userAuth = JWTAuth::parseToken()->authenticate();
        $customer_id = $userAuth->id;
        $data['customer_id'] = $customer_id;
        $data['order_date'] = date('Y-m-d');
        $data['order_status'] = 1;
//        dd($data);
        if ($userAuth) {
            $product = providerProducts::with('Item')->find($data['product_id']);
            if (!$product) {
                $response['code'] = -3;
                $response['result_num'] = 0;
                $response['result_msg'] = "المنتج غير موجود يرجى التأكد من المدخلات";
                $response['result_object'] = [];
                return response()->json($response);
            }
            $data['provider_id'] = $product->provider_id;
            $data['arraival_time'] = $product->delivered_date;
            $data['price'] = $product->price;
            $last_order = Orders::orderBy('order_id', 'desc')->first();
            if ($last_order) {
                $data['order_id'] = $last_order->order_id + 1;
            } else {
                $data['order_id'] = 1;
            }
//            $valid_order = Orders::where('customer_id', $customer_id)->where('order_status', 1)->first();
//            if ($valid_order) {
//                $response['code'] = -4;
//                $response['result_num'] = 0;
//                $response['result_msg'] = "لايمكنك تقديم طلب جديد، حيث لديك طلب سابق جديد";
//                $response['result_object'] = '';
//                return response()->json($response);
//            }
            $data['order_type'] = $product->Item->item_type;
            $order = Orders::create($data);
            if ($order) {
                $response["code"] = 1;
                $response['result_num'] = 1;
                $response["result_msg"] = "تم تقديم الطلب بنجاح";
                $response['result_object'] = [$order];
            } else {
                $response["code"] = -2;
                $response['result_num'] = 0;
                $response["result_msg"] = "حدثت مشكلة أثناء تقديم الطلب";
                $response['result_object'] = [];
            }
            return response()->json($response);
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "التوكن غير صحيح، يرجى إعادة تسجيل الدخول";
            $response['result_object'] = [];
            return response()->json($response);
        }
    }

    public function myOrders(Request $request) {
        $all_orders = [];
        $data = json_decode($request->getContent(), true);
        $userAuth = JWTAuth::parseToken()->authenticate();
        $customer_id = $userAuth->id;

        $orders = Orders::with(['Customer', 'Provider', 'Status', 'Product', 'Product.Item.Type']);
        if (isset($data['status_id'])) {
            $orders = $orders->whereIN('order_status', $data['status_id']);
        }
        if (isset($data['order_id'])) {
            $orders = $orders->where('order_id', $data['order_id']);
        }
        if (isset($data['provider_name'])) {
            $orders = $orders->whereHas('Provider', function($q) use ($data) {
                $q->where('name', 'like', '%' . $data['provider_name'] . '%');
            });
        }
        if (isset($data['product_name'])) {
            $orders = $orders->whereHas('Product', function($q) use ($data) {
                $q->where('name', 'like', '%' . $data['product_name'] . '%');
            });
        }
        if (isset($data['order_type'])) {
            $orders = $orders->where('order_type', $data['order_type']);
        }
//        if (isset($data['category_id'])) {
//            $orders = $orders->whereHas('Product.Item', function($q) use ($data) {
//                $q->where('item_type', $data['category_id']);
//            });
//        }
        $orders = $orders->where('customer_id', $customer_id)->orderBy('id', 'desc')->paginate($request['per_page']);
        $i = 0;
        foreach ($orders as $order) {
            if ($order->Provider != null && $order->Provider->image != null) {
                $storage = storage_path(ltrim($order->Provider->image, '/'));
                $storage = explode('public_html', $storage);
                $providerImg = 'http://' . $request->getHttpHost() . $storage[1];
            } else {
                $providerImg = null;
            }
            if ($order->Product != null) {
                $product_name = $order->Product->name;
                $order_category = $order->Product->Item->Type->name;
            } else {
                $product_name = null;
                $item = \App\Models\Items::with('Type')->find($order->item_id);
                $order_category = $item->Type->name;
            }

            if ($order->Provider != null) {
                $provider_name = $order->Provider->name;
            } else {
                $provider_name = null;
            }

            $all_orders[$i] = [
                'order_id' => $order->id,
                'product_name' => $product_name,
                'provider_name' => $provider_name,
                'order_date' => $order->created_at,
                'order_no' => $order->order_id,
                'order_status' => $order->Status->name,
                'order_category' => $order_category,
                'provider_image' => $providerImg
            ];
            $i++;
        }
        $count = count($orders);
        if ($count > 0) {
            $response["code"] = 1;
            $response['result_num'] = $count;
            $response["result_msg"] = "تم العثور على البيانات المطلوبة";
            $response['result_object'] = $all_orders;
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "لاتوجد نتائج";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function cancelOrder(Request $request) {
        $data = json_decode($request->getContent(), true);
        $userAuth = JWTAuth::parseToken()->authenticate();
        $customer_id = $userAuth->id;

        $order = Orders::find($data['order_id']);
        if (!$order) {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "الطلب غير موجود";
            $response['result_object'] = [];
            return response()->json($response);
        }
        if ($customer_id != $order->customer_id) {
            $response['code'] = -2;
            $response['result_num'] = 0;
            $response['result_msg'] = "غير مسموح لك الغاء الطلب";
            $response['result_object'] = [];
            return response()->json($response);
        }
        if ($order->order_status != 1) {
            $response['code'] = -3;
            $response['result_num'] = 0;
            $response['result_msg'] = "يجب أن تكون حالة الطلب جديد حتى تتمكن من إلغاءه";
            $response['result_object'] = [];
            return response()->json($response);
        }

        $order->order_status = 7;
        $order->save();
        $response['code'] = 1;
        $response['result_num'] = 1;
        $response['result_msg'] = "تم إلغاء الطلب بنجاح";
        $response['result_object'] = [$order];
        return response()->json($response);
    }

    public function confirmReceived(Request $request) {
        $data = json_decode($request->getContent(), true);
        $userAuth = JWTAuth::parseToken()->authenticate();
        $customer_id = $userAuth->id;

        $order = Orders::find($data['order_id']);
        if (!$order) {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "الطلب غير موجود";
            $response['result_object'] = [];
            return response()->json($response);
        }
        if ($customer_id != $order->customer_id) {
            $response['code'] = -2;
            $response['result_num'] = 0;
            $response['result_msg'] = "غير مسموح لك بتأكيد الاستلام، لأنك لست مقدم الطلب";
            $response['result_object'] = [];
            return response()->json($response);
        }
        if ($order->order_status != 5) {
            $response['code'] = -3;
            $response['result_num'] = 0;
            $response['result_msg'] = "يجب أن تكون حالة الطلب تم التسليم حتى تتمكن من تأكيد الاستلام";
            $response['result_object'] = [];
            return response()->json($response);
        }

        $order->order_status = 6;
        $order->save();
        $response['code'] = 1;
        $response['result_num'] = 1;
        $response['result_msg'] = "تم تأكيد استلام الطلب بنجاح";
        $response['result_object'] = [$order];
        return response()->json($response);
    }

    public function evaluateProvider(Request $request) {
        $data = json_decode($request->getContent(), true);
        $userAuth = JWTAuth::parseToken()->authenticate();
        $customer_id = $userAuth->id;

        if (!$userAuth) {
            $response['code'] = -2;
            $response['result_num'] = 0;
            $response['result_msg'] = "التوكن غير صحيح، يرجى إعادة تسجيل الدخول";
            $response['result_object'] = [];
            return response()->json($response);
        }

        if ($data['provider_id']) {
            $provider = Providers::find($data['provider_id']);
            if (!$provider) {
                $response['code'] = -3;
                $response['result_num'] = 0;
                $response['result_msg'] = "المزود غير موجود يرجى التأكد من المدخلات";
                $response['result_object'] = [];
                return response()->json($response);
            }
        }

        $data['customer_id'] = $customer_id;
        $evaluation = ProviderEvaluations::create($data);
        if ($evaluation) {
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = "تم تقييم المزود بنجاح";
            $response['result_object'] = [$evaluation];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "حدثت مشكلة أثناء التقييم، يرجى إعادة التقييم";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function profile(Request $request) {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $customer_id = $userAuth->id;
        $customer = Customers::find($customer_id);

        if ($customer) {
            unset($customer->password);
            if ($customer->image != null && $customer->image != "") {
                $storage = storage_path(ltrim($customer->image, '/'));
                $storage = explode('public_html', $storage);
                $customer->image = 'http://' . $request->getHttpHost() . $storage[1];
            }

            if ($customer->id_image != null && $customer->id_image != "") {
                $storage = storage_path(ltrim($customer->id_image, '/'));
                $storage = explode('public_html', $storage);
                $customer->id_image = 'http://' . $request->getHttpHost() . $storage[1];
            }
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = 'تم العثور على البيانات المطلوبة';
            $response['result_object'] = [$customer];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'لا توجد نتائج';
            $response['result_object'] = [];
        }

        return response()->json($response);
    }

    public function joinToProviders() {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $customer_id = $userAuth->id;
        $customer = Customers::find($customer_id);

        if ($customer) {
            $lastOrders = ConfirmOrders::where('customer_id', $customer_id)->where('status_id', 1)->count();
            if ($lastOrders > 0) {
                $response['code'] = -2;
                $response['result_num'] = 0;
                $response['result_msg'] = 'لايمكنك تقديم طلب جديد، مازال لديك طلب حالته جديد';
                $response['result_object'] = [];
                return response()->json($response);
            }
            $data['customer_id'] = $customer_id;
            $data['order_date'] = date('Y-m-d');
            $data['order_type'] = 1;
            $data['status_id'] = 1;
            $order = ConfirmOrders::create($data);
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = 'تم إضافة الطلب بنجاح';
            $response['result_object'] = [$order];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'العميل غير موجود';
            $response['result_object'] = [];
        }

        return response()->json($response);
    }

    public function joinToDelegates() {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $customer_id = $userAuth->id;
        $customer = Customers::find($customer_id);

        if ($customer) {
            $lastOrders = ConfirmOrders::where('customer_id', $customer_id)->where('status_id', 1)->count();
            if ($lastOrders > 0) {
                $response['code'] = -2;
                $response['result_num'] = 0;
                $response['result_msg'] = 'لايمكنك تقديم طلب جديد، مازال لديك طلب حالته جديد';
                $response['result_object'] = [];
                return response()->json($response);
            }
            $data['customer_id'] = $customer_id;
            $data['order_date'] = date('Y-m-d');
            $data['order_type'] = 2;
            $data['status_id'] = 1;
            $order = ConfirmOrders::create($data);
            $response['code'] = 1;
            $response['result_num'] = 1;
            $response['result_msg'] = 'تم إضافة الطلب بنجاح';
            $response['result_object'] = [$order];
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = 'العميل غير موجود';
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function specialOrder(Request $request) {
        $data = json_decode($request->getContent(), true);
        // dd($data);
        $userAuth = JWTAuth::parseToken()->authenticate();

        if ($userAuth) {
            $customer_id = $userAuth->id;
            $data['customer_id'] = $customer_id;
            $data['order_date'] = date('Y-m-d');
            $data['order_status'] = 1;
            $item = \App\Models\Items::find($data['item_id']);

            if (!$item) {
                $response['code'] = -3;
                $response['result_num'] = 0;
                $response['result_msg'] = "الصنف غير موجود يرجى التأكد من المدخلات";
                $response['result_object'] = [];
                return response()->json($response);
            }
            $data['order_type'] = $item->item_type;

            $last_order = Orders::orderBy('order_id', 'desc')->first();
            if ($last_order) {
                $data['order_id'] = $last_order->order_id + 1;
            } else {
                $data['order_id'] = 1;
            }

            $order = Orders::create($data);
            if ($order) {
                $response["code"] = 1;
                $response['result_num'] = 1;
                $response["result_msg"] = "تم تقديم الطلب بنجاح";
                $response['result_object'] = [$order];
            } else {
                $response["code"] = -2;
                $response['result_num'] = 0;
                $response["result_msg"] = "حدثت مشكلة أثناء تقديم الطلب";
                $response['result_object'] = [];
            }
            return response()->json($response);
        } else {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "التوكن غير صحيح، يرجى إعادة تسجيل الدخول";
            $response['result_object'] = [];
            return response()->json($response);
        }
    }

    public function getProvidersList($id) {
        $userAuth = JWTAuth::parseToken()->authenticate();
        $customer_id = $userAuth->id;

        $order = Orders::find($id);
        if (!$order) {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "الطلب غير موجود";
            $response['result_object'] = [];
            return response()->json($response);
        }

        if ($customer_id != $order->customer_id) {
            $response['code'] = -2;
            $response['result_num'] = 0;
            $response['result_msg'] = "غير مسموح لك باستعراض المزودين المتقدمين للطلب، لأنك لست مقدم الطلب";
            $response['result_object'] = [];
            return response()->json($response);
        }
        if ($order->order_status != 1) {
            $response['code'] = -3;
            $response['result_num'] = 0;
            $response['result_msg'] = "يجب أن تكون حالة الطلب جديد حتى تتمكن من استعراض المتقدمين للطلب";
            $response['result_object'] = [];
            return response()->json($response);
        }

        $allRequests = OrdersProviderRequests::where('order_id', $id)->where('status_id', 1)->get();

        if (count($allRequests) > 0) {
            $response['code'] = 1;
            $response['result_num'] = count($allRequests);
            $response['result_msg'] = "تم العثور على النتائج";
            $response['result_object'] = [$allRequests];
        } else {
            $response['code'] = -4;
            $response['result_num'] = 0;
            $response['result_msg'] = "لا توجد نتائج";
            $response['result_object'] = [];
        }
        return response()->json($response);
    }

    public function acceptProvider($id) {
        $userAuth = JWTAuth::parseToken()->authenticate();
        if (!$userAuth) {
            $response['code'] = -1;
            $response['result_num'] = 0;
            $response['result_msg'] = "التوكن غير صحيح، يجب إعادة تسجيل الدخول مرة أخرى";
            $response['result_object'] = [];
            return response()->json($response);
        }
        $customer_id = $userAuth->id;

        $request = OrdersProviderRequests::with('Order')->find($id);

        if (!$request) {
            $response['code'] = -2;
            $response['result_num'] = 0;
            $response['result_msg'] = "طلب المزود غير موجود، يرجى التأكد من البيانات";
            $response['result_object'] = [];
            return response()->json($response);
        }

        if ($request->Order->customer_id != $customer_id) {
            $response['code'] = -3;
            $response['result_num'] = 0;
            $response['result_msg'] = "لا يمكنك قبل طلب المزود لأنه لست مقدم الطلب الرئيسي";
            $response['result_object'] = [];
            return response()->json($response);
        }

        $request->status_id = 2;
        $request->save();
        $request->Order->provider_id = $request->provider_id;
        $request->Order->order_status = 11;
        $request->Order->save();

        $response['code'] = 1;
        $response['result_num'] = 1;
        $response['result_msg'] = "تم قبول طلب المزود بنجاح";
        $response['result_object'] = [$request];
        return response()->json($response);
    }

}
