var MetronicApp = angular.module('MetronicApp', []);
MetronicApp.controller('SettingsController', ['$rootScope', '$scope', 'Settings', '$ngBootbox',
    function($rootScope, $scope, Settings, $ngBootbox) {
    $rootScope.resource = Settings;
    $rootScope.RemoveMsg();
        $rootScope.clear_msg();
        $scope.row = {};
        $scope.editDisable = false;
        $scope.tab1 = $scope.tab2 = $scope.tab3 = $scope.tab4 = false;
        $scope.init = function(){
        Settings.query( function (data) {
            $scope.row = data.settings;
            $scope.pricing = data.pricing.data;
            $scope.cities = data.cities.data;
            $scope.countries = data.countries.data;
            $scope.codes = data.codes.data;
            $scope.x.dis_from = parseFloat(data.begin);
            
                });
            };
        $scope.init();
        $scope._save = function (row){
          Settings.modify({'data': row, id: 1}, function (data) {
              $scope.tab1 = true;
                    $rootScope.check_save_result(data);
                });
        };
        
        $scope.add_price = function(x){
            $rootScope.RemoveMsg();
        $rootScope.clear_msg();
        $scope.tab2 = true;
        if(x.id){
         Settings.editPrice(x, function (data) {
                    $rootScope.check_save_result(data);
                    $scope.x = {};
                    $scope.editDisable = false;
                    $scope.init();
                });   
        } else {
           Settings.create(x, function (data) {
                    $rootScope.check_save_result(data);
                    $scope.x = {};
                    $scope.init();
                }); 
            }
        };
        
        $scope.x = {};
        $scope.editPrice = function(id){
            Settings.get({'id': id}, function (data) {
                $scope.editDisable = true;
                $scope.x = data;
                $scope.x.dis_from = parseFloat(data.dis_from);
                $scope.x.dis_to = parseFloat(data.dis_to);
                $scope.x.price = parseFloat(data.price);
                $scope.x.id = parseInt(data.id);
                
                }); 
        }
                
        $scope.add_city = function(x){
            $rootScope.RemoveMsg();
        $rootScope.clear_msg();
        $scope.tab4 = true;
        if(x.id){
         Settings.edit_city(x, function (data) {
                    $rootScope.check_save_result(data);
                    $scope.y = {};
                    $scope.init();
                });   
        } else {
           Settings.createCity(x, function (data) {
                    $rootScope.check_save_result(data);
                    $scope.y = {};
                    $scope.init();
                }); 
            }
        };
        
                $scope.y = {};
        $scope.editCity = function(id){
            Settings.getCity({'id': id}, function (data) {
                $scope.y = data;
                $scope.y.CountryId = data.CountryId+"";
                $scope.y.active = data.active+"";
                }); 
        }
        
        $scope.add_code = function(x){
            $rootScope.RemoveMsg();
        $rootScope.clear_msg();
        $scope.tab3 = true;
        if(x.id){
         Settings.edit_city(x, function (data) {
                    $rootScope.check_save_result(data);
                    $scope.c = {};
                    $scope.init();
                });   
        } else {
           Settings.createCode(x, function (data) {
                    $rootScope.check_save_result(data);
                    $scope.c = {};
                    $scope.init();
                }); 
            }
        };
        
        $scope.viewCustomers = function (x){
            Settings.codeViewCustomer({'id':x}, function (data) {
                    $scope.codeData = data;
                    $("#customers_modal").modal('show');
                    
                });
        };
        
        $scope.delete_code = function (x){
            $ngBootbox.confirm("هل أنت متأكد من الإلغاء؟")
                .then(function () {
                    $rootScope.clear_msg();
            Settings.deleteCode({'id':x}, function (data) {
                    $rootScope.check_save_result(data);
                    $scope.c = {};
                    $scope.init();
                }); 
            });
        };
}]);