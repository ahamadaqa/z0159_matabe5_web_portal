var MetronicApp = angular.module('MetronicApp', []);
MetronicApp.controller('CustomersController', ['$rootScope', '$scope', 'Customers', function($rootScope, $scope, Customers) {
    $rootScope.resource = Customers;
        $rootScope.init();
        
        $scope._reset = function () {
            $scope.row = {};
            $rootScope.init();
        };
        
        $scope._search = function (row) {
            $rootScope.resource.query(row, function (data) {
                $rootScope.details = data.data;
                $rootScope.CurrentPage = data.current_page;
                $rootScope.TotalItems = data.total;
                $rootScope.ItemsPerPage = data.per_page;
            
        });
        };
}]);

MetronicApp.controller('CustomersCtrlAdd', ['$scope', 'Customers', '$stateParams', '$rootScope', function ($scope, Customers, $stateParams, $rootScope) {

        if ($stateParams.id) {
            Customers.get({'id': $stateParams.id}, function (data) {
                $scope.row = data.data;
                $scope.evaluation = data.evaluation;
                $scope.payments = data.payments;
            });
        }
        
        $scope.open_image = function () {
            $("#id_image_modal").modal('show');
        };
        
        $scope._cancel = function () {
            $rootScope.cancel("/customers");
        };

              

    }]);