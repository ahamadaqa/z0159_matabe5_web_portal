var MetronicApp = angular.module('MetronicApp', []);
MetronicApp.controller('ItemsController', ['$rootScope', '$scope', 'Items', function($rootScope, $scope, Items) {
    $rootScope.resource = Items;
    $rootScope.RemoveMsg();
        $rootScope.clear_msg();
        $rootScope.init();
        
        $scope._reset = function () {
            $scope.row = {};
            $rootScope.init();
        };
        
        $scope._search = function (row) {
            $rootScope.resource.query(row, function (data) {
                $rootScope.details = data.data;
                $rootScope.CurrentPage = data.current_page;
                $rootScope.TotalItems = data.total;
                $rootScope.ItemsPerPage = data.per_page;
            
        });
        };
}]);

MetronicApp.controller('ItemsCtrlAdd', ['$scope', 'Items', '$stateParams', '$rootScope', function ($scope, Items, $stateParams, $rootScope) {


        $rootScope.RemoveMsg();
        
        Items.getOptions(function (data) {
            $scope.types = data.types;
        });

        if ($stateParams.id) {
            $scope.title = 'تعديل';
            Items.get({'id': $stateParams.id}, function (data) {
                $scope._edit(data);
            });
        }
        else {
            $scope.title = 'إضافة';
            $scope.mode = "add";
        }


        $rootScope.clear_msg();
        $scope.row = {};

        $scope._edit = function (item) {

            $scope.mode = 'edit';
            $scope.row = item;
            $scope.row.item_order = parseInt($scope.row.item_order);

        };
        
        $(document).ready(
                function () {
                    $('input:file').change(
                            function () {
                                if ($(this).val()) {
                                    $('#submitBtn').attr('disabled', false);
                                }
                            }
                    );
            
                });

               $scope._save = function (row) {
            var f = document.getElementById('item_image').files[0],
                    r = new FileReader();
            if (f) {
                r.onloadend = function (e) {
                    row.image = e.target.result;
                    if ($scope.mode == 'edit') {
                        Items.modify({'data': row, id: $scope.row.id}, function (data) {
                            $rootScope.check_save_result(data, "/items");
                        });

                    } else {
                        Items.create(row, function (data) {
                            $rootScope.check_save_result(data, "/items");
                        });

                    }

                };
                r.readAsDataURL(f);
            } 
            else {
                if ($scope.mode == 'edit') {
                    Items.modify({'data': row, id: $scope.row.id}, function (data) {
                        $rootScope.check_save_result(data, "/items");
                    });

                } else {
                    Items.create(row, function (data) {
                        $rootScope.check_save_result(data, "/items");
                    });

                }
            }


        };

        $scope._cancel = function () {
            $rootScope.cancel("/items");
        };

    }]);