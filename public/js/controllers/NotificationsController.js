var MetronicApp = angular.module('MetronicApp', []);
MetronicApp.controller('NotificationsController', ['$rootScope', '$scope', 'Notifications', '$filter', function ($rootScope, $scope, Notifications, $filter) {
        $rootScope.resource = Notifications;
        $rootScope.RemoveMsg();
        $rootScope.clear_msg();
        $rootScope.init();
        
        $scope.viewUsers = false;
        
        
        $scope.openUsersList = function(){
            if($scope.viewUsers == false){
               $scope.viewUsers = true; 
            } else {
                $scope.viewUsers = false;
                $scope.sms.user_id = {};
                
            }
            
        }
        
        $rootScope.resource.getSelectOptions(function(data){
            $scope.users = data;
        });

        $scope.open_notify = function () {
            //$scope.sms.notify_text = ' ';
                $rootScope.RemoveMsg();
                $rootScope.clear_msg();
            $("#add_notifications_modal").modal('show');
        };

        $scope._reset = function () {
            $scope.row = {};
            $rootScope.showError1 = false;
            $rootScope.error_msg1 = ' ';
            $rootScope.init();
        };

        $scope._search = function (row) {

            var data = angular.copy(row);
            console.log(row);
            var date1 = new Date(data.to_date);
            var date2 = new Date(data.from_date);
            console.log(date1);
            if (date1 < date2) {
                $rootScope.showError1 = true;
                $rootScope.error_msg1 = 'يجب أن يكون تاريخ إرسال إلى أكبر من تاريخ الارسال من';
                return;
            }

            $rootScope.resource.query(row, function (data) {
                $rootScope.details = data.data;
                $rootScope.CurrentPage = data.current_page;
                $rootScope.TotalItems = data.total;
                $rootScope.ItemsPerPage = data.per_page;

            });
        };

        $scope.save = function (row) {
            $rootScope.resource.create({'data': row}, function (data) {
                $rootScope.check_save_result(data);
                $("#add_notifications_modal").modal('hide');
                $scope.sms.notify_text = '';
                $rootScope.init();
            });
        };
        $scope._resend = function (row) {
            $scope.notify_text = row;
            $("#add_notifications_modal").modal('show');

        };

    }]);