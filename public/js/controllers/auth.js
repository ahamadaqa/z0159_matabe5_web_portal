var AuthModule = angular.module("Auth", ['ngResource', 'satellizer'])



AuthModule.config(['$authProvider', function ($authProvider) {



        $authProvider.loginUrl = 'z0159_matabe5_web_portal/public/authenticate';


        



    }]);





AuthModule.factory('Authenticate', ['$resource', function ($resource) {

        return $resource(':operation/:id', {id: '@id'}, {

            login: {method: 'POST', params: {operation: 'authenticate'}},

            logout: {method: 'POST', params: {operation: 'logout'}},

            user: {method: 'GET', params: {operation: 'user'}}

        });

    }]);





AuthModule.controller("AuthController",

        function ($scope, Authenticate, $auth, $filter,$http) {

            if (localStorage.getItem("logged_user"))

                window.location = 'index.html';

            $scope.row = {};
            $scope.showError = false;

                $scope.error_msg = '';

            $scope.login = function () {

                

                $auth.login({

                    username: $scope.row.username,

                    password: $scope.row.password,

                }).then(function () {

                    Authenticate.user(function (data) {

                        localStorage.setItem("logged_user", JSON.stringify(data.user));

                        localStorage.setItem("authorization", true);

                        window.location = 'index.html';

                    });

                }, function (error) {

                    if (error.status == 401) {

                        $scope.showError = true;

                        $scope.error_msg = 'خطأ في اسم المستخدم أو كلمة المرور';

                    }

                    else if (error.status == 500) {

                        $scope.showError = true;

                        $scope.error_msg = 'حدث خطأ ما';

                    }



                });

            };

            $scope.forget=function () {

                jQuery('.login-form').hide();

                jQuery('.forget-form').show();

                $scope.email=null;

                $scope.message={};

                $scope.failed = false;



            };

            $scope.sendEmail=function () {

                $scope.showError2 = null;

                $scope.error_msg2 = null;

                $scope.showSuccess = null;

                $scope.success_msg = null;



                $http({

                    method: 'POST',

                    url: 'api/v1.0/password/email',

                    data: {

                        email: $scope.email

                    }

                }).then(function (response) {

                    $scope.showSuccess = true;

                    $scope.success_msg = 'تم ارسال رابط تعيين كلمة المرور الى بريدك الالكتروني';

                }, function (error) {

                    if (error.status == 401) {

                        $scope.showError2 = true;

                        $scope.error_msg2 = 'البريد الإلكتروني غير مسجل في النظام';

                    }

                    else if (error.status == 500) {

                        $scope.showError2 = true;

                        $scope.error_msg2 = 'حدث خطأ ما';

                    }

                });

            }

        }

);



AuthModule.controller("ResetPasswordController",

        function ($rootScope, $scope,$filter,$http,$timeout) {



            $rootScope.showError = false;

            $rootScope.showSuccess = false;

            $scope.row = {};



            $scope.resetPassword = function (token) {

                $rootScope.showError = null;

                $rootScope.showSuccess = null;

                $rootScope.error_msg = null;

                $rootScope.success_msg = null;

                $http({

                    method: 'POST',

                    url: '../../api/v1.0/password/reset',

                    data: {

                        token: token,

                        email: $scope.row.email,

                        password: $scope.row.password,

                        password_confirmation: $scope.row.password_confirmation

                    }

                }).then(function (response) {



                    if(response.status){

                        $rootScope.showSuccess = true;

                        $rootScope.success_msg = 'تم حفظ كلمة المرور الجديدة ، سيتم تحويلك خلال  3 ثواني إلى صفحة تسجيل الدخول';

                    }



                        $timeout(function() {

                            window.location = '../../login.html';

                        }, 2000);

                },

                    function (error) {

                        if (error.status == 401) {

                            $rootScope.showError = true;

                            $rootScope.error_msg = 'الاميل غير صحيح أرجو التحقق من الاميل';

                        } else  if (error.status == 402) {

                            $rootScope.showError = true;

                            $rootScope.error_msg = 'كلمة المرور وتأكيدها غير متطابقتان';

                        }

                        else if (error.status == 500) {

                            $rootScope.showError = true;

                            $rootScope.error_msg = 'حدث خطأ ما';

                        }



                    });





            };



        }

);