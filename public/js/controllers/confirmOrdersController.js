var MetronicApp = angular.module('MetronicApp', []);
        MetronicApp.controller('confirmOrdersCtrl', ['$rootScope', '$scope', 'ConfirmOrders', '$ngBootbox', function($rootScope, $scope, ConfirmOrders, $ngBootbox) {
    $rootScope.resource = ConfirmOrders;
    $rootScope.RemoveMsg();
        $rootScope.clear_msg();
        $rootScope.init();
        
        $scope._reset = function () {
            $scope.row = {};
            $rootScope.init();
        };
        
        $scope._search = function (row) {
            $rootScope.resource.query(row, function (data) {
                $rootScope.details = data.data;
                $rootScope.CurrentPage = data.current_page;
                $rootScope.TotalItems = data.total;
                $rootScope.ItemsPerPage = data.per_page;
            
        });
        };
        
        $scope.accept_order = function (id){
            $ngBootbox.confirm("هل أنت متأكد من قبول الطلب؟")
                .then(function () {
                    $rootScope.clear_msg();
                    $rootScope.resource.accept({id: id}, function (data) {
                        if ($rootScope.check_delete_result(data)) {
                            $rootScope.init();
                        }
                    });
                });
        };
        $scope.row = {};
        $scope.row.refuse_text = '';
        $scope.refuse_order = function(id) {
                $rootScope.RemoveMsg();
                $rootScope.clear_msg();
                $scope.row.orderId = id;
            $("#refuse_order_modal").modal('show');
        
        };
        
        $scope.confirm_refuse = function(row){
        $rootScope.resource.refuse({data: row}, function (data) {
            $("#refuse_order_modal").modal('hide');
                        if ($rootScope.check_delete_result(data)) {
                            
                            $rootScope.init();
                        }
                    });    
        };
        
        $scope.viewMore = function(x){
            $rootScope.resource.get({'id':x}, function (data) {
                $scope.orderDetails = data;
                $("#moreDetails_modal").modal('show');
            });
        }
}]);