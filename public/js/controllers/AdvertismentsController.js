var MetronicApp = angular.module('MetronicApp', []);
MetronicApp.controller('AdvertismentsController', ['$rootScope', '$scope', 'Advertisments', function ($rootScope, $scope, Advertisments) {
        $rootScope.resource = Advertisments;
        $rootScope.RemoveMsg();
        $rootScope.clear_msg();
        $rootScope.init();

        $scope._reset = function () {
            $scope.row = {};
            $rootScope.init();
        };

        $scope._search = function (row) {
            $rootScope.resource.query(row, function (data) {
                $rootScope.details = data.data;
                $rootScope.CurrentPage = data.current_page;
                $rootScope.TotalItems = data.total;
                $rootScope.ItemsPerPage = data.per_page;

            });
        };
    }]);

MetronicApp.controller('AdvertismentsCtrlAdd', ['$scope', 'Advertisments', '$stateParams', '$rootScope', function ($scope, Advertisments, $stateParams, $rootScope) {


        $rootScope.RemoveMsg();



        if ($stateParams.id) {
            $scope.title = 'تعديل';
            Advertisments.get({'id': $stateParams.id}, function (data) {
                $scope._edit(data);
            });
        } else {
            $scope.title = 'إضافة';
            $scope.mode = "add";
            $('#submitBtn').attr('disabled', true);
        }


        $rootScope.clear_msg();
        $scope.row = {};

        $scope._edit = function (item) {

            $scope.mode = 'edit';
            $scope.row = item;

        };



        $(document).ready(
                function () {
                    $('input:file').change(
                            function () {
                                if ($(this).val()) {
                                    $('#submitBtn').attr('disabled', false);
                                }
                            }
                    );
            
                });


        $scope._save = function (row) {
            var f = document.getElementById('employee_picture').files[0],
                    r = new FileReader();
            if (f) {
                r.onloadend = function (e) {
                    row.image = e.target.result;
                    if ($scope.mode == 'edit') {
                        Advertisments.modify({'data': row, id: $scope.row.id}, function (data) {
                            $rootScope.check_save_result(data, "/advertisments");
                        });

                    } else {
                        Advertisments.create(row, function (data) {
                            $rootScope.check_save_result(data, "/advertisments");
                        });

                    }

                };
                r.readAsDataURL(f);
            } 
            else {
                if ($scope.mode == 'edit') {
                    Advertisments.modify({'data': row, id: $scope.row.id}, function (data) {
                        $rootScope.check_save_result(data, "/advertisments");
                    });

                } else {
                    Advertisments.create(row, function (data) {
                        $rootScope.check_save_result(data, "/advertisments");
                    });

                }
            }


        };

        $scope._cancel = function () {
            $rootScope.cancel("/advertisments");
        };

    }]);