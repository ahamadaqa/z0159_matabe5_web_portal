var MetronicApp = angular.module('MetronicApp', []);
        MetronicApp.controller('AdminsController', ['$rootScope', '$scope', 'Admins', function($rootScope, $scope, Admins, $filter) {
    $rootScope.resource = Admins;
    $rootScope.RemoveMsg();
        $rootScope.clear_msg();
        $rootScope.init();
        
        $scope._reset = function () {
            $scope.row = {};
            $rootScope.init();
        };
        
        $scope._search = function (row) {
            $rootScope.resource.query(row, function (data) {
                $rootScope.details = data.data;
                $rootScope.CurrentPage = data.current_page;
                $rootScope.TotalItems = data.total;
                $rootScope.ItemsPerPage = data.per_page;
            
        });
        };
}]);

MetronicApp.controller('addAdminCtrl', ['$scope', 'Admins', '$stateParams', '$rootScope', function ($scope, Admins, $stateParams, $rootScope) {


        $rootScope.RemoveMsg();

        if ($stateParams.id) {
            $scope.title = 'تعديل';
            Admins.get({'id': $stateParams.id}, function (data) {
                $scope._edit(data);
            });
        }
        else {
            $scope.title = 'إضافة';
            $scope.mode = "add";
        }


        $rootScope.clear_msg();
        $scope.row = {};

        $scope._edit = function (item) {

            $scope.mode = 'edit';
            $scope.row = item;
            $scope.row.password = '';
            $scope.row.password2 = '';

        };

        $scope._save = function (row) {
            if ($scope.mode == 'edit') {
                Admins.modify({'data': row, id: $scope.row.id}, function (data) {
                    $rootScope.check_save_result(data, "/admins");
                });

            } else {
                Admins.create(row, function (data) {
                    $rootScope.check_save_result(data, "/admins");
                });

            }
        };

        $scope._cancel = function () {
            $rootScope.cancel("/admins");
        };

    }]);