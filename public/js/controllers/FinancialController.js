var MetronicApp = angular.module('MetronicApp', []);
MetronicApp.filter('split', function () {
    return function (input, splitChar, splitIndex) {
        // do some bounds checking here to ensure it has that index
        return input.split(splitChar)[splitIndex];
    }
}).controller('FinancialController', ['$rootScope', '$scope', 'Financial', function ($rootScope, $scope, Financial) {
        $rootScope.resource = Financial;
        $rootScope.RemoveMsg();
        $rootScope.clear_msg();
        $rootScope.init();

        $scope._reset = function () {
            $scope.row = {};
            $rootScope.init();
        };

        $scope._search = function (row) {
            $rootScope.resource.query(row, function (data) {
                $rootScope.details = data.data;
                $rootScope.CurrentPage = data.current_page;
                $rootScope.TotalItems = data.total;
                $rootScope.ItemsPerPage = data.per_page;

            });
        };
        $scope.payRow = {};
        $scope.doPay = function (user, way) {
            $scope.payRow.provider_id = user;
            $scope.payRow.order_payment_way_id = way;
            $("#pay_modal").modal('show');
        };

        $scope.pay = function (row) {
            if(row.payment_way_id == 3){
            var f = document.getElementById('payBill').files[0],
                    r = new FileReader();
            if (f) {
                r.onloadend = function (e) {
                    row.bill = e.target.result;
                    $rootScope.resource.create(row, function (data) {
                $("#pay_modal").modal('hide');
                $rootScope.check_save_result(data);
                $scope.payRow = {};
                $rootScope.init();

            });
                };
                r.readAsDataURL(f);
            } } else {

            $rootScope.resource.create(row, function (data) {
                $("#pay_modal").modal('hide');
                $rootScope.check_save_result(data);
                $scope.payRow = {};
                $rootScope.init();

            });
            }
        };
    }]);