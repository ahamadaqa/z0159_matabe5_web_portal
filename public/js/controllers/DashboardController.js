angular.module('MetronicApp', ['googlechart']).value('googleChartApiConfig', {
    version: '1',
    optionalSettings: {
        packages: ['corechart'],
        language: 'ar'
    }
}).controller('DashboardController', function ($rootScope, $scope, $http, $timeout, Dashboard) {


    $scope._search = function (row) {
        Dashboard.query(row, function (data) {
            $scope.ordersCount = data.ordersCount;
            $scope.ordersCloseCount = data.ordersCloseCount;
            $scope.ordersCancelCount = data.ordersCancelCount;

            $scope.delegatesChart = {
                "type": "ColumnChart",
                "displayed": false,
                "cssStyle": "height:150px;width: 100%;",
                "data": {
                    "cols": [{
                            "label": "المدن",
                            "type": "string"
                        }, {
                            "label": "عدد المناديب",
                            "type": "number"
                        }],
                    "rows": data.cityDelegatesCount
                },
                "options": {
                    "title": "",
                    "isStacked": "true",
                    "fill": 20,
                    "is3D": true,
                    "colors": ["#28a6a8", "#8E44AD", "#e7505a", "#35aa47", "#f3c200"],
                    "font-family":"Cairo",
                    titleTextStyle: {
                        color: 'red', // any HTML string color ('red', '#cc00cc')
                        fontName: 'Cairo', // i.e. 'Times New Roman'
                        fontSize: 12 // 12, 18 whatever you want (don't specify px)

                    },
                    "animation": {
                        "startup": true,
                        "duration": 2000,
                        "easing": "inAndOut"
                    },
                    "displayExactValues": false,
                    "vAxis": {
                        "title": "",
                        "gridlines": {
                            "count": 5
                        }
                    },
                    "hAxis": {
                        "title": ""
                    }
                }
            };

            $scope.customersChart = {
                "type": "PieChart",
                "displayed": false,
                "cssStyle": "height:150px;width: 100%",
                "data": {
                    "cols": [{
                            "label": "City",
                            "type": "string"
                        }, {
                            "label": "Customer",
                            "type": "number"
                        }, {

                            "id": "Walkin",
                            "label": "Walkin",
                            "type": "number"
                        }],
                    "rows": data.cityCustomersCount
                },
                "options": {
                    "title": "",
                    "isStacked": "true",
                    "fill": 20,
                    "is3D": true,
                    "colors": ["#f3c200", "#32c5d2", "#e7505a", "#35aa47", "#8E44AD"],
                    "animation": {
                        "startup": true,
                        "duration": 2000,
                        "easing": "inAndOut"
                    },
                    "displayExactValues": false,
                    "vAxis": {
                        "title": "",
                        "gridlines": {
                            "count": 10
                        }
                    },
                    "hAxis": {
                        "title": "عدد العملاء"
                    }
                }
            };

            $scope.providersChart = {
                "type": "PieChart",
                "displayed": false,
                "cssStyle": "height:200px;width: 100%",
                "data": {
                    "cols": [{
                            "label": "City",
                            "type": "string"
                        }, {
                            "label": "Customer",
                            "type": "number"
                        }, {

                            "id": "Walkin",
                            "label": "Walkin",
                            "type": "number"
                        }],
                    "rows": data.cityProvidersCount
                },
                "options": {
                    "title": "",
                    "isStacked": "true",
                    "fill": 20,
                    "is3D": true,
                    "colors": ["#32c5d2", "#8E44AD", "#e7505a", "#35aa47", "#f3c200"],
                    "animation": {
                        "startup": true,
                        "duration": 2000,
                        "easing": "inAndOut"
                    },
                    "displayExactValues": false,
                    "vAxis": {
                        "title": "No of Bookings",
                        "gridlines": {
                            "count": 10
                        }
                    },
                    "hAxis": {
                        "title": "Products"
                    }
                }
            };

            $scope.ordersChart = {
                "type": "ColumnChart",
                "displayed": false,
                "cssStyle": "height:210px;width: 100%",
                "data": {
                    "cols": [{
                            "label": "المدن",
                            "type": "string"
                        }, {
                            "label": "عدد الطلبات",
                            "type": "number"
                        }],
                    "rows": data.cityDelegatesCount
                },
                "options": {
                    "title": "",
                    "isStacked": "true",
                    "fill": 20,
                    "is3D": true,
                    "colors": ["#f3c200", "rgb(124, 124, 172)", "rgb(0, 227, 253)", "rgb(0, 206, 230)", "rgb(26, 110, 112)"],
                    "animation": {
                        "startup": true,
                        "duration": 2000,
                        "easing": "inAndOut"
                    },
                    "displayExactValues": false,
                    "vAxis": {
                        "title": "",
                        "gridlines": {
                            "count": 5
                        }
                    },
                    "hAxis": {
                        "title": ""
                    }
                }
            };
        });
    }

    $scope.init = function () {


        Dashboard.query(function (data) {
            $scope.ordersCount = data.ordersCount;
            $scope.ordersCloseCount = data.ordersCloseCount;
            $scope.ordersCancelCount = data.ordersCancelCount;

            $scope.delegatesChart = {
                "type": "ColumnChart",
                "displayed": false,
                "cssStyle": "height:150px;width: 100%",
                "data": {
                    "cols": [{
                            "label": "المدن",
                            "type": "string"
                        }, {
                            "label": "عدد المناديب",
                            "type": "number"
                        }],
                    "rows": data.cityDelegatesCount
                },
                "options": {
                    "title": "",
                    "isStacked": "true",
                    "fill": 20,
                    "is3D": true,
                    "colors": ["#28a6a8", "#8E44AD", "#e7505a", "#35aa47", "#f3c200"],
                    "animation": {
                        "startup": true,
                        "duration": 2000,
                        "easing": "inAndOut"
                    },
                    "displayExactValues": false,
                    "vAxis": {
                        "title": "",
                        "gridlines": {
                            "count": 5
                        }
                    },
                    "hAxis": {
                        "title": ""
                    }
                }
            };

            $scope.customersChart = {
                "type": "PieChart",
                "displayed": false,
                "cssStyle": "height:150px;width: 100%",
                "data": {
                    "cols": [{
                            "label": "City",
                            "type": "string"
                        }, {
                            "label": "Customer",
                            "type": "number"
                        }, {

                            "id": "Walkin",
                            "label": "Walkin",
                            "type": "number"
                        }],
                    "rows": data.cityCustomersCount
                },
                "options": {
                    "title": "",
                    "isStacked": "true",
                    "fill": 20,
                    "is3D": true,
                    "colors": ["#f3c200", "#32c5d2", "#e7505a", "#35aa47", "#8E44AD"],
                    "animation": {
                        "startup": true,
                        "duration": 2000,
                        "easing": "inAndOut"
                    },
                    "displayExactValues": false,
                    "vAxis": {
                        "title": "",
                        "gridlines": {
                            "count": 10
                        }
                    },
                    "hAxis": {
                        "title": "عدد العملاء"
                    }
                }
            };

            $scope.providersChart = {
                "type": "PieChart",
                "displayed": false,
                "cssStyle": "height:200px;width: 100%",
                "data": {
                    "cols": [{
                            "label": "City",
                            "type": "string"
                        }, {
                            "label": "Customer",
                            "type": "number"
                        }, {

                            "id": "Walkin",
                            "label": "Walkin",
                            "type": "number"
                        }],
                    "rows": data.cityProvidersCount
                },
                "options": {
                    "title": "",
                    "isStacked": "true",
                    "fill": 20,
                    "is3D": true,
                    "colors": ["#32c5d2", "#8E44AD", "#e7505a", "#35aa47", "#f3c200"],
                    "animation": {
                        "startup": true,
                        "duration": 2000,
                        "easing": "inAndOut"
                    },
                    "displayExactValues": false,
                    "vAxis": {
                        "title": "No of Bookings",
                        "gridlines": {
                            "count": 10
                        }
                    },
                    "hAxis": {
                        "title": "Products"
                    }
                }
            };

            $scope.ordersChart = {
                "type": "ColumnChart",
                "displayed": false,
                "cssStyle": "height:210px;width: 100%",
                "data": {
                    "cols": [{
                            "label": "المدن",
                            "type": "string"
                        }, {
                            "label": "عدد الطلبات",
                            "type": "number"
                        }],
                    "rows": data.cityDelegatesCount
                },
                "options": {
                    "title": "",
                    "isStacked": "true",
                    "fill": 20,
                    "is3D": true,
                    "colors": ["#f3c200", "rgb(124, 124, 172)", "rgb(0, 227, 253)", "rgb(0, 206, 230)", "rgb(26, 110, 112)"],
                    "animation": {
                        "startup": true,
                        "duration": 2000,
                        "easing": "inAndOut"
                    },
                    "displayExactValues": false,
                    "vAxis": {
                        "title": "",
                        "gridlines": {
                            "count": 5
                        }
                    },
                    "hAxis": {
                        "title": ""
                    }
                }
            };
        });
    };

    $scope.init();

    $scope.chartReady = function () {

    };

    $(document).ready(function () {
        $('text').css('font-family', 'Cairo');
    });

});