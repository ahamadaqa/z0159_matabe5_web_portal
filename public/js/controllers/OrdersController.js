var MetronicApp = angular.module('MetronicApp', ['ngMap']);
MetronicApp.filter('split', function () {
    return function (input, splitChar, splitIndex) {
        // do some bounds checking here to ensure it has that index
        return input.split(splitChar)[splitIndex];
    }
}).controller('ordersController', ['$rootScope', '$scope', 'Orders', function ($rootScope, $scope, Orders, $filter) {
        $rootScope.resource = Orders;
        $rootScope.RemoveMsg();
        $rootScope.clear_msg();
        $rootScope.init();

        $scope._reset = function () {
            $scope.row = {};
            $rootScope.init();
        };

        $scope._search = function (row) {
            $rootScope.resource.query(row, function (data) {
                $rootScope.details = data.data;
                $rootScope.CurrentPage = data.current_page;
                $rootScope.TotalItems = data.total;
                $rootScope.ItemsPerPage = data.per_page;

            });
        };
    }]);

MetronicApp.controller('OrdersCtrlAdd', ['$scope', 'Orders', '$stateParams', '$rootScope', function ($scope, Orders, $stateParams, $rootScope) {


        $rootScope.RemoveMsg();

        if ($stateParams.id) {
            $scope.title = 'تعديل';
            Orders.get({'id': $stateParams.id}, function (data) {
                $scope._edit(data);
            });
        }



        $rootScope.clear_msg();
        $scope.row = {};

        $scope._edit = function (item) {

            $scope.mode = 'edit';
            $scope.row = item;
            $scope.row.password = '';

            $scope.markers = [{
                    lat: $scope.row.provider.loc_lat,
                    lng: $scope.row.provider.loc_long,
                    title: "مكان التسليم",
                    id: "deliver1",
                    idwin: "myInfoWindow1"
                },
                {
                    lat: $scope.row.latitude,
                    lng: $scope.row.longitude,
                    title: "مكان الاستلام",
                    id: "deliver2",
                    idwin: "myInfoWindow2"
                }];

            $scope.mapcenter = {
                lat: $scope.markers[0].lat,
                lng: $scope.markers[0].lng
            };

        };
        
        $scope.openMap = function(){
            $("#map_modal").modal('show');
        };

        $scope._cancel = function () {
            $rootScope.cancel("/orders");
        };

    }]);