var MetronicApp = angular.module('MetronicApp', ['ngMap']);
MetronicApp.filter('split', function () {
    return function (input, splitChar, splitIndex) {
        // do some bounds checking here to ensure it has that index
        return input.split(splitChar)[splitIndex];
    }
}).controller('delegatesController', ['$rootScope', '$scope', 'Providers', '$ngBootbox', function ($rootScope, $scope, Providers, $ngBootbox) {
        $rootScope.resource = Providers;
        $rootScope.RemoveMsg();
        $rootScope.clear_msg();

        $scope.init = function () {
            $rootScope.resource.query({page: 1, type: 2}, function (data) {
                $rootScope.details = data.data;
                $rootScope.CurrentPage = data.current_page;
                $rootScope.TotalItems = data.total;
                $rootScope.ItemsPerPage = data.per_page;

            });
        };
        $scope.init();

        $scope._reset = function () {
            $scope.row = {};
            $scope.init();
        };

        $scope._search = function (row) {
            row.type = 2;
            $rootScope.resource.query(row, function (data) {
                $rootScope.details = data.data;
                $rootScope.CurrentPage = data.current_page;
                $rootScope.TotalItems = data.total;
                $rootScope.ItemsPerPage = data.per_page;

            });
        };

        $rootScope._delete = function (id) {
            $ngBootbox.confirm("هل أنت متأكد من الحذف؟")
                    .then(function () {
                        $rootScope.clear_msg();
                        $rootScope.resource.destroy({id: id}, function (data) {
                            if ($rootScope.check_delete_result(data)) {
                                $scope.init();
                            }
                        });
                    });
        };
    }]);

MetronicApp.controller('DelegatesCtrlAdd', ['$scope', 'Providers', '$stateParams', '$rootScope', '$ngBootbox', function ($scope, Providers, $stateParams, $rootScope, $ngBootbox) {

        $rootScope.RemoveMsg();

        if ($stateParams.id) {
            $scope.title = 'تعديل';
            $scope.getProvider = function () {
                Providers.get({'id': $stateParams.id}, function (data) {
                    $scope._edit(data.data);
                    $scope.evaluation = data.evaluation;
                    $scope.evaluation = data.evaluation;
                    $scope.totalIncome = data.totalIncome;
                    $scope.profit = data.profit;
                    $scope.totalPaied = data.totalPaied;
                    $scope.totalRemind = data.totalRemind;
                    $scope.files = data.files;
                });
            };

            $scope.getProvider();

        } else {
            $scope.title = 'إضافة';
            $scope.mode = "add";
        }


        $rootScope.clear_msg();
        $scope.row = {};

        $scope._edit = function (item) {

            $scope.mode = 'edit';
            $scope.row = item;
            $scope.row.password = '';
            $scope.row.password1 = '';

            $scope.markers = [{
                    lat: $scope.row.loc_lat,
                    lng: $scope.row.loc_long
                }];

            $scope.mapcenter = {
                lat: $scope.markers[0].lat,
                lng: $scope.markers[0].lng
            };


        };

        $scope._save = function (row) {
            if ($scope.mode == 'edit') {
                Providers.modify({'data': row, id: $scope.row.id}, function (data) {
                    $rootScope.check_save_result(data, "/delegates");
                });

            } else {
                Providers.create(row, function (data) {
                    $rootScope.check_save_result(data, "/delegates");
                });

            }
        };

        $scope._cancel = function () {
            $rootScope.cancel("/delegates");
        };

        $scope.add_payment_way = function (x) {
            $rootScope.RemoveMsg();
            $rootScope.clear_msg();
            Providers.add_payment_way({'payment_way_id': x, 'provider_id': $stateParams.id}, function (data) {
                $rootScope.check_save_result(data);
                $scope.getProvider();
            });
        };

        $scope.delete_payment_way = function (x) {
            $rootScope.RemoveMsg();
            $rootScope.clear_msg();
            $ngBootbox.confirm("هل أنت متأكد من الحذف؟").then(function () {
                Providers.delete_payment_way({'id': x}, function (data) {
                    $rootScope.check_save_result(data);
                    $scope.getProvider();
                });
            });
        };

        $scope.viewProduct = function (x) {

            Providers.viewProduct({'id': x}, function (data) {
                $scope.product_data = data;
                $("#product_modal").modal('show');
            });
        };

        $scope.deleteFile = function (x) {
            $ngBootbox.confirm("هل أنت متأكد من الحذف؟").then(function () {
                Providers.deleteFile({'id': x}, function () {
                    $scope.getProvider();
                });
            });
        };
        $scope.file = {};
        $(document).ready(
                function () {
                    $('#addFile').change(function () {
                                    $('#addFile').attr('disabled', false);
                                    var f = document.getElementById('addFile').files[0],
                                            r = new FileReader();
                                    if (f) {
                                        r.onloadend = function (e) {
                                            $scope.file.image = e.target.result;
                                            $scope.file.provider_id = $stateParams.id;

                                            Providers.addFile($scope.file, function () {
                                                $scope.getProvider();
                                            });



                                        };
                                        r.readAsDataURL(f);
                                    }
                                
                            }
                    );

                });

    }]);