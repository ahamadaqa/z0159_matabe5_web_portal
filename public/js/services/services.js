var MyAngularServices = angular.module('MyAngularServices', []).constant('API_URL', 'api/v1.0/');

MyAngularServices.factory('Admins', ['$resource', function ($resource) {
        return $resource('Admins/:operation/:id', {id: '@id'}, {
            query: {method: 'GET'},
            create: {method: 'POST'},
            modify: {method: 'PUT'},
            destroy: {method: 'DELETE'}
        });
    }]);

MyAngularServices.factory('Items', ['$resource', function ($resource) {
        return $resource('Items/:operation/:id', {id: '@id'}, {
            query: {method: 'GET'},
            create: {method: 'POST'},
            modify: {method: 'PUT'},
            getOptions: {method: 'GET', params: {operation: 'getOptions'}},
            destroy: {method: 'DELETE'}
        });
    }]);

MyAngularServices.factory('Orders', ['$resource', function ($resource) {
        return $resource('Orders/:operation/:id', {id: '@id'}, {
            query: {method: 'GET'},
            create: {method: 'POST'},
            modify: {method: 'PUT'},
            destroy: {method: 'DELETE'}
        });
    }]);

MyAngularServices.factory('Customers', ['$resource', function ($resource) {
        return $resource('Customers/:operation/:id', {id: '@id'}, {
            query: {method: 'GET'},
            create: {method: 'POST'},
            modify: {method: 'PUT'},
            destroy: {method: 'DELETE'}
        });
    }]);

MyAngularServices.factory('Providers', ['$resource', function ($resource) {
        return $resource('Providers/:operation/:id', {id: '@id'}, {
            query: {method: 'GET'},
            create: {method: 'POST'},
            modify: {method: 'PUT'},
            destroy: {method: 'DELETE'},
            getOptions: {method: 'GET', params: {operation: 'getOptions'}},
            add_payment_way : {method: 'POST', params: {operation: 'add_payment_way'}},
            getPaymentsWays: {method: 'GET', params: {operation: 'getPaymentsWays'}},
            delete_payment_way:{method: 'DELETE', params: {operation: 'delete_payment_way'}},
            viewProduct:{method: 'GET', params: {operation: 'viewProduct'}},
            deleteFile: {method: 'DELETE', params: {operation: 'deleteFile'}},
            addFile: {method: 'POST', params: {operation: 'addFile'}}
        });
    }]);

MyAngularServices.factory('Delegates', ['$resource', function ($resource) {
        return $resource('Delegates/:operation/:id', {id: '@id'}, {
            query: {method: 'GET'},
            create: {method: 'POST'},
            modify: {method: 'PUT'},
            destroy: {method: 'DELETE'}
        });
    }]);

MyAngularServices.factory('ConfirmOrders', ['$resource', function ($resource) {
        return $resource('ConfirmOrders/:operation/:id', {id: '@id'}, {
            query: {method: 'GET'},
            create: {method: 'POST'},
            modify: {method: 'PUT'},
            destroy: {method: 'DELETE'},
            accept: {method: 'PUT', params: {operation: 'accept'}},
            refuse: {method: 'PUT', params: {operation: 'refuse'}}
        });
    }]);
MyAngularServices.factory('Settings', ['$resource', function ($resource) {
        return $resource('Settings/:operation/:id', {id: '@id'}, {
            query: {method: 'GET'},
            create: {method: 'POST'},
            modify: {method: 'PUT'},
            destroy: {method: 'DELETE'},
            editPrice: {method: 'PUT', params: {operation: 'editPrice'}},
            createCity: {method: 'POST', params: {operation: 'createCity'}},
            getCity: {method: 'GET', params: {operation: 'getCity'}},
            edit_city: {method: 'PUT', params: {operation: 'edit_city'}},
            createCode: {method: 'POST', params: {operation: 'createCode'}},
            codeViewCustomer: {method: 'GET', params: {operation: 'codeViewCustomer'}},
            deleteCode: {method: 'DELETE', params: {operation: 'deleteCode'}}            
        });
    }]);


MyAngularServices.factory('Financial', ['$resource', function ($resource) {
        return $resource('Financial/:operation/:id', {id: '@id'}, {
            query: {method: 'GET'},
            create: {method: 'POST'},
            modify: {method: 'PUT'},
            destroy: {method: 'DELETE'},
            accept: {method: 'PUT', params: {operation: 'accept'}},
            refuse: {method: 'PUT', params: {operation: 'refuse'}}
        });
    }]);

MyAngularServices.factory('Advertisments', ['$resource', function ($resource) {
        return $resource('Advertisments/:operation/:id', {id: '@id'}, {
            query: {method: 'GET'},
            create: {method: 'POST'},
            modify: {method: 'PUT'},
            destroy: {method: 'DELETE'}
        });
    }]);

MyAngularServices.factory('Notifications', ['$resource', function ($resource) {
        return $resource('Notifications/:operation/:id', {id: '@id'}, {
            query: {method: 'GET'},
            create: {method: 'POST'},
            modify: {method: 'PUT'},
            destroy: {method: 'DELETE'},
            getSelectOptions : {method: 'GET', params: {operation: 'getSelectOptions'}, isArray: true}
        });
    }]);

MyAngularServices.factory('Authenticate', ['$resource', 'API_URL', function ($resource, API_URL) {
        return $resource(':operation/:id', {id: '@id'}, {
            logout: {method: 'POST', params: {operation: 'logout'}},
            user: {method: 'GET', params: {operation: 'devenv/user'}},
        });
    }]);

MyAngularServices.factory('Dashboard', ['$resource', function ($resource) {
        return $resource('Dashboard/:operation/:id', {id: '@id'}, {
            query: {method: 'GET'},
            create: {method: 'POST'},
            modify: {method: 'PUT'},
            destroy: {method: 'DELETE'}
        });
    }]);



