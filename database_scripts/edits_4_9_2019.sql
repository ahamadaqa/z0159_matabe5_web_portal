ALTER TABLE `items` ADD `item_type` INT(11) NOT NULL AFTER `item_order`;
ALTER TABLE `items` ADD CONSTRAINT `item_type` FOREIGN KEY (`item_type`) REFERENCES `constant_table`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
INSERT INTO `constant_table` (`id`, `name`, `type`) VALUES (NULL, 'أكلة خاصة ومناسبات', '2');
INSERT INTO `constant_table` (`id`, `name`, `type`) VALUES (NULL, 'اكلات جاهزة', '2');
INSERT INTO `constant_table` (`id`, `name`, `type`) VALUES (NULL, 'حلويات', '2');
ALTER TABLE `orders` ADD `product_id` INT(11) NOT NULL AFTER `delegate_id`, ADD `quantity` INT(11) NOT NULL AFTER `product_id`;
ALTER TABLE `orders` ADD CONSTRAINT `product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `provider_products`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `providers` ADD `idno` VARCHAR(20) NULL AFTER `name`;
ALTER TABLE `providers`  ADD `gender` TINYINT(1) NULL COMMENT '1=ذكر, 2=انثى'  AFTER `type`,  ADD `birthdate` DATE NULL  AFTER `gender`,  
ADD `car_type` VARCHAR(20) NULL  AFTER `birthdate`,  ADD `car_model` VARCHAR(20) NULL  AFTER `car_type`,
ADD `car_year` VARCHAR(20) NULL  AFTER `car_model`;
ALTER TABLE `orders` ADD `city_id` INT(11) NOT NULL AFTER `longitude`, ADD `area` VARCHAR(30) NOT NULL AFTER `city_id`, ADD `promocode` VARCHAR(30) NULL AFTER `area`;
ALTER TABLE `customers` ADD `password` VARCHAR(300) NOT NULL AFTER `mobile`;
UPDATE `customers` SET `password` = '$2y$10$FQtb3D/vi1ML/Gzt9F6SkujxQFvbHP13eMcbSIoY0FPxIbiYWFxoK' WHERE `customers`.`id` = 1;
ALTER TABLE `orders` ADD `price` FLOAT NOT NULL AFTER `promocode`;
ALTER TABLE `customer_evaluations` CHANGE `order_id` `order_id` INT(11) NULL;
