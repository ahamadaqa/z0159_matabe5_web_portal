-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 19, 2019 at 06:16 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `matabe5`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `is_super_admin` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `username`, `password`, `mobile`, `email`, `is_super_admin`, `created_at`, `updated_at`) VALUES
(1, 'مدير النظام', 'admin', '$2y$10$st5FYaf0ni8pqsFfPmh.4Oojqf2DAUPoSfPPxdgt4zDUUFRlgF.gy', '(059) 654-1155', NULL, 1, '2019-01-03 09:19:24', '2019-01-03 07:19:24'),
(6, 'amna', 'amna', '$2y$10$SmZTbC5D70XpGEaWirmSOeaPXi5nxpQs2pqdSfE5msqgIqvPcjfu6', '059', 'wael@speedclick.ps2', 0, '2019-02-10 16:22:40', '2019-02-10 14:22:40');

-- --------------------------------------------------------

--
-- Table structure for table `advertisments`
--

CREATE TABLE `advertisments` (
  `id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `advertisments`
--

INSERT INTO `advertisments` (`id`, `image`, `from_date`, `to_date`, `url`, `mobile`, `created_at`, `updated_at`) VALUES
(2, '/images/_1541840594722.jpg', '2018-11-02', '2018-11-16', 'www', NULL, '2018-11-10 07:03:14', '2018-11-10 07:03:14'),
(3, '/images/_1542707260219.jpg', '2018-11-17', '2018-12-31', NULL, NULL, '2018-12-17 17:41:03', '2018-12-17 15:41:03'),
(4, '/images/_1545934468526.jpg', '2018-12-20', '2018-12-29', NULL, '0597667754', '2018-12-27 16:14:28', '2018-12-27 16:14:28');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `Name` varchar(300) NOT NULL,
  `EnglishName` varchar(300) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `CountryId` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `Name`, `EnglishName`, `active`, `CountryId`, `created_at`, `updated_at`) VALUES
(1, 'مكة المكرمة', '', 1, 1, '2019-02-17 04:49:12', '2019-02-17 02:49:12'),
(2, 'المدينة المنورة', '', 1, 1, '2019-02-12 03:34:04', '0000-00-00 00:00:00'),
(3, 'الرياض', NULL, 0, 1, '2019-02-17 02:24:25', '2019-02-17 02:24:25');

-- --------------------------------------------------------

--
-- Table structure for table `codes`
--

CREATE TABLE `codes` (
  `id` int(11) NOT NULL,
  `code_id` int(15) NOT NULL,
  `percentage` int(3) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `last_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `codes`
--

INSERT INTO `codes` (`id`, `code_id`, `percentage`, `active`, `last_date`, `created_at`, `updated_at`) VALUES
(1, 159753, 5, 1, '2019-02-28', '2019-02-17 10:14:03', '0000-00-00 00:00:00'),
(2, 258951, 5, 0, '2019-02-20', '2019-02-17 11:07:02', '2019-02-17 11:07:02'),
(3, 122, 5, 1, '2019-02-12', '2019-02-17 15:14:20', '2019-02-17 15:14:20');

-- --------------------------------------------------------

--
-- Table structure for table `confirm_order`
--

CREATE TABLE `confirm_order` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_date` date NOT NULL,
  `order_type` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `refuse_text` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `confirm_order`
--

INSERT INTO `confirm_order` (`id`, `customer_id`, `order_date`, `order_type`, `status_id`, `refuse_text`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-02-12', 1, 2, '', '2019-02-13 01:50:33', '2019-02-12 23:50:33'),
(2, 1, '2019-02-12', 2, 3, 'kkk', '2019-02-13 02:20:46', '2019-02-13 00:20:46'),
(3, 1, '2019-02-12', 1, 3, 'juu', '2019-02-13 16:04:51', '2019-02-13 14:04:51'),
(4, 1, '2019-02-12', 2, 1, '', '2019-02-13 02:22:04', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `constant_table`
--

CREATE TABLE `constant_table` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `constant_table`
--

INSERT INTO `constant_table` (`id`, `name`, `type`) VALUES
(1, 'جديد', 1),
(2, 'جاري التحضير', 1),
(3, 'تم التحضير', 1),
(4, 'جاري التوصيل', 1),
(5, 'تم التسليم', 1),
(6, 'تم الاستلام', 1),
(7, 'ملغي', 1);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `CountryId` int(11) NOT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `EnglishName` varchar(300) DEFAULT NULL,
  `Nationality` varchar(300) DEFAULT NULL,
  `EnglishNationality` varchar(300) DEFAULT NULL,
  `name3` varchar(300) DEFAULT NULL,
  `name4` varchar(300) DEFAULT NULL,
  `name5` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`CountryId`, `Name`, `EnglishName`, `Nationality`, `EnglishNationality`, `name3`, `name4`, `name5`) VALUES
(1, 'السعودية', 'Saudi Arabia', 'سعودي', NULL, NULL, NULL, NULL),
(2, 'قطر', 'Qatar', 'قطري', NULL, NULL, NULL, NULL),
(3, 'الإمارات العربية المتحدة', 'United Arab Emirates', 'إماراتي', NULL, NULL, NULL, NULL),
(4, 'الكويت', 'Kuwait', 'كويتي', NULL, NULL, NULL, NULL),
(5, 'البحرين', 'Bahrai', 'بحريني', NULL, NULL, NULL, NULL),
(6, 'مصر', 'Egypt', 'مصري', NULL, NULL, NULL, NULL),
(7, 'السودان', 'Suda', 'سوداني', NULL, NULL, NULL, NULL),
(8, 'اليمن', 'Yeme', 'يمني', NULL, NULL, NULL, NULL),
(9, 'العراق', 'Iraq', 'عراقي', NULL, NULL, NULL, NULL),
(10, 'الجزائر', 'Algeria', 'جزائري', NULL, NULL, NULL, NULL),
(11, 'الأردن', 'Jorda', 'أردني', NULL, NULL, NULL, NULL),
(12, 'لبنان', 'Lebano', 'لبناني', NULL, NULL, NULL, NULL),
(13, 'سوريا', 'Syria', 'سوري', NULL, NULL, NULL, NULL),
(14, 'ليبيا', 'Libya', 'ليبيي', NULL, NULL, NULL, NULL),
(15, 'المغرب', 'Morocco', 'مغربي', NULL, NULL, NULL, NULL),
(16, 'موريتانيا', 'Mauritania', 'موريتاني', NULL, NULL, NULL, NULL),
(17, 'عمان', 'Oma', 'عماني', NULL, NULL, NULL, NULL),
(18, 'فلسطين', 'Palestine', 'فلسطيني', NULL, NULL, NULL, NULL),
(19, 'تونس', 'Tunisia', 'تونسي', NULL, NULL, NULL, NULL),
(20, 'الصومال', 'Somalia', 'صومالي', NULL, NULL, NULL, NULL),
(21, 'جيبوتي', 'Djibouti', 'جيبوتي', NULL, NULL, NULL, NULL),
(22, 'جزر القمر', 'Comores', 'جزر القمر', NULL, NULL, NULL, NULL),
(23, 'أندورا', 'Andorra', 'أندوري', NULL, NULL, NULL, NULL),
(24, 'أفغانستان', 'Afghanista', 'أفغانستاني', NULL, NULL, NULL, NULL),
(25, 'أنتيجوا', 'Antigua', 'أنتيجوي', NULL, NULL, NULL, NULL),
(26, 'أنجويلا', 'Anguilla', 'أنجويلي', NULL, NULL, NULL, NULL),
(27, 'ألبانيا', 'Albania', 'ألباني', NULL, NULL, NULL, NULL),
(28, 'أرمينيا', 'Armenia', 'أرميني', NULL, NULL, NULL, NULL),
(29, 'أنجولا', 'Anla', 'أنجولي', NULL, NULL, NULL, NULL),
(30, 'الأرجنتين', 'Argentina', 'أرجنتيني', NULL, NULL, NULL, NULL),
(31, 'ساموا الأمريكية', 'American Samoa', 'ساموي الأمريكي', NULL, NULL, NULL, NULL),
(32, 'النمسا', 'Austria', 'نمسواي', NULL, NULL, NULL, NULL),
(33, 'أستراليا', 'Australia', 'أسترالي', NULL, NULL, NULL, NULL),
(34, 'أروبا', 'Aruba', 'أروبي', NULL, NULL, NULL, NULL),
(35, 'أذربيجان', 'Azerbaija', 'أذربيجاني', NULL, NULL, NULL, NULL),
(36, 'البوسنة والهرسك', 'Bosnia and Herzevina', 'بوسني', NULL, NULL, NULL, NULL),
(37, 'بربادوس', 'Barbados', 'بربادوسي', NULL, NULL, NULL, NULL),
(38, 'بنغلاديش', 'Bangladesh', 'بنغلاديشي', NULL, NULL, NULL, NULL),
(39, 'بلجيكا', 'Belgium', 'بلجيكي', NULL, NULL, NULL, NULL),
(40, 'بوركينا فاسو', 'Burkina Faso', 'بوركيني', NULL, NULL, NULL, NULL),
(41, 'بلغاريا', 'Bulgaria', 'بلغاري', NULL, NULL, NULL, NULL),
(42, 'بوروندي', 'Burundi', 'بوروندي', NULL, NULL, NULL, NULL),
(43, 'بنين', 'Beni', 'بنيني', NULL, NULL, NULL, NULL),
(44, 'سانت بارتيليمي', ' Saint Barthélemy', 'سانت بارتيليمي', NULL, NULL, NULL, NULL),
(45, 'برمودا', 'Bermuda', 'برمودي', NULL, NULL, NULL, NULL),
(46, 'بروناي دار السلام', 'Brunei Darussalam', 'بروناي دار السلام', NULL, NULL, NULL, NULL),
(47, 'بوليفيا', 'Bolivia', 'بوليفي', NULL, NULL, NULL, NULL),
(48, 'البرازيل', 'Brazil', 'برازيلي', NULL, NULL, NULL, NULL),
(49, 'جزر البهاما', 'Bahamas', 'جزر البهاما', NULL, NULL, NULL, NULL),
(50, 'بوتان', 'Bhuta', 'بوتاني', NULL, NULL, NULL, NULL),
(51, 'بوتسوانا', 'Botswana', 'بوتسواني', NULL, NULL, NULL, NULL),
(52, 'روسيا البيضاء', 'Biélorussie', 'روسيا البيضاء', NULL, NULL, NULL, NULL),
(53, 'بليز', 'Belize', 'بليزي', NULL, NULL, NULL, NULL),
(54, 'كندا', 'Canada', 'كندي', NULL, NULL, NULL, NULL),
(55, 'جمهورية الكونغو الديموقراطية', 'Con, Democratic Republic of the (Zaire),', 'كونغولي', NULL, NULL, NULL, NULL),
(56, 'جمهورية أفريقيا الوسطى', 'Central African Republic', 'جمهورية أفريقيا الوسطى', NULL, NULL, NULL, NULL),
(57, 'الكونغو', 'Con', 'كونغولي', NULL, NULL, NULL, NULL),
(58, 'سويسرا', 'Suisse', 'سويسري', NULL, NULL, NULL, NULL),
(59, 'ساحل العاج', 'Ivory Coast', 'ساحل العاج', NULL, NULL, NULL, NULL),
(60, 'جزر كوك', 'Cocos Islands', 'جزر كوك', NULL, NULL, NULL, NULL),
(61, 'تشيلي', 'Chile', 'تشيلي', NULL, NULL, NULL, NULL),
(62, 'الكاميرون', 'Cameroo', 'كاميروني', NULL, NULL, NULL, NULL),
(63, 'الصين', 'China', 'صيني', NULL, NULL, NULL, NULL),
(64, 'كولومبيا', 'Colombia', 'كولومبي', NULL, NULL, NULL, NULL),
(65, 'كوستاريكا', 'Costa Rica', 'كوستاريكي', NULL, NULL, NULL, NULL),
(66, 'كوبا', 'Cuba', 'كوبي', NULL, NULL, NULL, NULL),
(67, 'الرأس الأخضر', 'Cape Verde', 'الرأس الأخضر', NULL, NULL, NULL, NULL),
(68, 'جزيرة الكريسماس', 'Christmas Island', 'جزيرة الكريسماس', NULL, NULL, NULL, NULL),
(69, 'قبرص', 'Cyprus', 'قبرصي', NULL, NULL, NULL, NULL),
(70, 'جمهورية التشيك', 'Czech Republic', 'تشيكي', NULL, NULL, NULL, NULL),
(71, 'ألمانيا', 'Germany', 'ألماني', NULL, NULL, NULL, NULL),
(72, 'الدنمارك', 'Denmark', 'دنماركي', NULL, NULL, NULL, NULL),
(73, 'دومينيكا', 'Dominica', 'دومينيكا', NULL, NULL, NULL, NULL),
(74, 'جمهورية الدومينيكان', 'Dominican Republic', 'دومينيكاني', NULL, NULL, NULL, NULL),
(75, 'الاكوادور', 'Ecuador', 'اكوادوري', NULL, NULL, NULL, NULL),
(76, 'استونيا', 'Estonia', 'استوني', NULL, NULL, NULL, NULL),
(77, 'الصحراء الغربية', 'Western Sahara', 'الصحراء الغربية', NULL, NULL, NULL, NULL),
(78, 'إريتريا', 'Eritrea', 'إريتري', NULL, NULL, NULL, NULL),
(79, 'إسبانيا', 'Spai', 'إسباني', NULL, NULL, NULL, NULL),
(80, 'أثيوبيا', 'Ethiopia', 'أثيوبي', NULL, NULL, NULL, NULL),
(81, 'فنلندا', 'Finland', 'فنلندي', NULL, NULL, NULL, NULL),
(82, 'فيجي', 'Fiji', 'فيجي', NULL, NULL, NULL, NULL),
(83, 'جزر فوكلاند (مالفيناس),', 'Falkland Islands', 'جزر فوكلاند (مالفيناس),', NULL, NULL, NULL, NULL),
(84, 'ميكرونيزيا', 'Micronesia, Federated States of', 'ميكرونيزي', NULL, NULL, NULL, NULL),
(85, 'جزر فارو', 'Faroe Islands', 'جزر فارو', NULL, NULL, NULL, NULL),
(86, 'فرنسا', 'France', 'فرنسي', NULL, NULL, NULL, NULL),
(87, 'الغابون', 'Gabo', 'غابوني', NULL, NULL, NULL, NULL),
(88, 'المملكة المتحدة لبريطانيا العظمى وأيرلندا الشمالية', 'United Kingdom', 'إنجليزي', NULL, NULL, NULL, NULL),
(89, 'غرينادا', 'Grenada', 'غرينادي', NULL, NULL, NULL, NULL),
(90, 'جورجيا', 'Georgia', 'جورجي', NULL, NULL, NULL, NULL),
(91, 'غويانا', 'Guyana', 'غوياني', NULL, NULL, NULL, NULL),
(92, 'غيرنسي', 'Guernsey', 'غيرنسي', NULL, NULL, NULL, NULL),
(93, 'غانا', 'Ghana', 'غاني', NULL, NULL, NULL, NULL),
(94, 'جبل طارق', 'Gibraltar', 'جبل طارق', NULL, NULL, NULL, NULL),
(95, 'جرينلاند', 'Greenland', 'جرينلاندي', NULL, NULL, NULL, NULL),
(96, 'غامبيا', 'Gambia', 'غامبي', NULL, NULL, NULL, NULL),
(97, 'غينيا', 'Guinea', 'غيني', NULL, NULL, NULL, NULL),
(98, 'غوادلوب', 'Guadeloupe', 'غوادلوبي', NULL, NULL, NULL, NULL),
(99, 'غينيا الاستوائية', 'Equatorial Guinea', 'غيني', NULL, NULL, NULL, NULL),
(100, 'اليونان', 'Greece', 'يوناني', NULL, NULL, NULL, NULL),
(101, 'جورجيا الجنوبية وجزر ساندويتش الجنوبية ', 'South Georgia and the South Sandwich Islands', 'جورجيا الجنوبية وجزر ساندويتش الجنوبية ', NULL, NULL, NULL, NULL),
(102, 'غواتيمالا', 'Guatemala', 'غواتيمالي', NULL, NULL, NULL, NULL),
(103, 'غوام', 'Guam', 'غوامي', NULL, NULL, NULL, NULL),
(104, 'غينيا بيساو', 'Guinea-Bissau', 'غيني', NULL, NULL, NULL, NULL),
(105, 'غويانا', 'Guyana', NULL, NULL, NULL, NULL, NULL),
(106, 'هونغ كونغ', 'Hong Kong', 'هونغ كونغ', NULL, NULL, NULL, NULL),
(107, 'كرواتيا', 'Croatia', 'كرواتي', NULL, NULL, NULL, NULL),
(108, 'هايتي', 'Haiti', 'هايتي', NULL, NULL, NULL, NULL),
(109, 'هنغاريا', 'Hungary', 'هنغاري', NULL, NULL, NULL, NULL),
(110, 'أندونيسيا', 'Indonesia', 'أندونيسي', NULL, NULL, NULL, NULL),
(111, 'أيرلندا', 'Ireland', 'أيرلندي', NULL, NULL, NULL, NULL),
(112, 'جزيرة مان', 'Isle of Ma', 'جزيرة مان', NULL, NULL, NULL, NULL),
(113, 'الهند', 'India', 'هندي', NULL, NULL, NULL, NULL),
(114, 'ايران', 'Ira', 'ايراني', NULL, NULL, NULL, NULL),
(115, 'أيسلندا', 'Iceland', 'أيسلندي', NULL, NULL, NULL, NULL),
(116, 'إيطاليا', 'Italy', 'إيطالي', NULL, NULL, NULL, NULL),
(117, 'جيرسي', 'Jersey', 'جيرسي', NULL, NULL, NULL, NULL),
(118, 'جامايكا', 'Jamaica', 'جامايكي', NULL, NULL, NULL, NULL),
(119, 'اليابان', 'Japa', 'ياباني', NULL, NULL, NULL, NULL),
(120, 'كينيا', 'Kenya', 'كيني', NULL, NULL, NULL, NULL),
(121, 'جمهورية قيرغيزستان', 'Kyrgyzsta', 'قيرغيزستاني', NULL, NULL, NULL, NULL),
(122, 'كمبوديا', 'Cambodia', 'كمبودي', NULL, NULL, NULL, NULL),
(123, 'كيريباتي', 'Kiribati', 'كيريباتي', NULL, NULL, NULL, NULL),
(124, 'سانت كيتس ونيفيس', 'Saint Kitts and Nevis', 'سانت كيتس ونيفيس', NULL, NULL, NULL, NULL),
(125, 'كوريا الشمالية', 'North Korea', 'كوري شمالي', NULL, NULL, NULL, NULL),
(126, 'كوريا الجنوبية', 'South Korea', 'كوري جنوبي', NULL, NULL, NULL, NULL),
(127, 'جزر كايمان', 'Cayman Islands', 'جزر كايمان', NULL, NULL, NULL, NULL),
(128, 'كازاخستان', 'Kazakhsta', 'كازاخستاني', NULL, NULL, NULL, NULL),
(129, 'جمهورية لاو الديمقراطية الشعبية', 'Laos', 'جمهورية لاو الديمقراطية الشعبية', NULL, NULL, NULL, NULL),
(130, 'سانت لوسيا', 'Saint Lucia', 'سانت لوسي', NULL, NULL, NULL, NULL),
(131, 'ليختنشتاين', 'Liechtenstei', 'ليختنشتايني', NULL, NULL, NULL, NULL),
(132, 'سريلانكا', 'Sri Lanka', 'سريلانكي', NULL, NULL, NULL, NULL),
(133, 'ليبيريا', 'Liberia', 'ليبيري', NULL, NULL, NULL, NULL),
(134, 'ليسوتو', 'Lesotho', 'ليسوتوي', NULL, NULL, NULL, NULL),
(135, 'ليتوانيا', 'Lithuania', 'ليتواني', NULL, NULL, NULL, NULL),
(136, 'لوكسمبورغ', 'Luxembourg', 'لوكسمبورغي', NULL, NULL, NULL, NULL),
(137, 'لاتفيا', 'Latvia', 'لاتفي', NULL, NULL, NULL, NULL),
(138, 'موناكو', 'Monaco', 'موناكو', NULL, NULL, NULL, NULL),
(139, 'مولدوفا', 'Moldova', 'مولدوفي', NULL, NULL, NULL, NULL),
(140, 'الجبل الأسود', 'Montenegro', 'الجبل الأسود', NULL, NULL, NULL, NULL),
(141, 'مدغشقر', 'Madagascar', 'مدغشقري', NULL, NULL, NULL, NULL),
(142, 'جزر مارشال', 'Marshall Islands', 'مارشالي', NULL, NULL, NULL, NULL),
(143, 'مقدونيا', 'Macedonia', 'مقدوني', NULL, NULL, NULL, NULL),
(144, 'مالي', 'Mali', 'مالي', NULL, NULL, NULL, NULL),
(145, 'ميانمار', 'Myanmar', 'ميانماري', NULL, NULL, NULL, NULL),
(146, 'منغوليا', 'Monlia', 'منغولي', NULL, NULL, NULL, NULL),
(147, 'مارتينيك', 'Martinique', 'مارتينيكي', NULL, NULL, NULL, NULL),
(148, 'مونتسيرات', 'Montserrat', 'مونتسيراتي', NULL, NULL, NULL, NULL),
(149, 'مالطا', 'Malta', 'مالطي', NULL, NULL, NULL, NULL),
(150, 'موريشيوس', 'Mauritius', 'موريشيوسي', NULL, NULL, NULL, NULL),
(151, 'جزر المالديف', 'Maldives', 'مالديفي', NULL, NULL, NULL, NULL),
(152, 'ملاوي', 'Malawi', 'ملاوي', NULL, NULL, NULL, NULL),
(153, 'المكسيك', 'Mexico', 'مكسيكي', NULL, NULL, NULL, NULL),
(154, 'ماليزيا', 'Malaysia', 'ماليزي', NULL, NULL, NULL, NULL),
(155, 'موزمبيق', 'Mozambique', 'موزمبيقي', NULL, NULL, NULL, NULL),
(156, 'ناميبيا', 'Namibia', 'ناميبي', NULL, NULL, NULL, NULL),
(157, 'كاليدونيا الجديدة', 'New Caledonia', 'كاليدوني', NULL, NULL, NULL, NULL),
(158, 'النيجر', 'Niger', 'نيجري', NULL, NULL, NULL, NULL),
(159, 'جزيرة نورفولك', 'Norfolk Island', 'نورفولكي', NULL, NULL, NULL, NULL),
(160, 'نيكاراغوا', 'Nicaragua', 'نيكاراغوي', NULL, NULL, NULL, NULL),
(161, 'هولندا', 'Netherlands', 'هولندي', NULL, NULL, NULL, NULL),
(162, 'النرويج', 'Norway', 'نرويجي', NULL, NULL, NULL, NULL),
(163, 'نيبال', 'Nepal', 'نيبالي', NULL, NULL, NULL, NULL),
(164, 'نيوي', 'Niue', 'نيوي', NULL, NULL, NULL, NULL),
(165, 'نيوزيلندا', 'New Zealand', 'نيوزيلندي', NULL, NULL, NULL, NULL),
(166, 'بناما', 'Panama', 'بنامي', NULL, NULL, NULL, NULL),
(167, 'بيرو', 'Peru', 'بيروي', NULL, NULL, NULL, NULL),
(168, 'بولينيزيا الفرنسية', 'French Polynesia', 'بولينيزي', NULL, NULL, NULL, NULL),
(169, 'بابوا غينيا الجديدة', 'French Guiana', 'بابوا غيني', NULL, NULL, NULL, NULL),
(170, 'الفلبين', 'Philippines', 'فلبيني', NULL, NULL, NULL, NULL),
(171, 'باكستان', 'Pakista', 'باكستاني', NULL, NULL, NULL, NULL),
(172, 'بولندا', 'Poland', 'بولندي', NULL, NULL, NULL, NULL),
(173, 'سان بيار وميكلون', 'Saint Pierre and Miquelo', 'سان بيار وميكلون', NULL, NULL, NULL, NULL),
(174, 'بتكايرن', 'Pitcairn Islands', 'بتكايرني', NULL, NULL, NULL, NULL),
(175, 'بورتوريكو', 'Puerto Rico', 'بورتوريكي', NULL, NULL, NULL, NULL),
(176, 'البرتغال', 'Portugal', 'برتغالي', NULL, NULL, NULL, NULL),
(177, 'بالاو', 'Palau', 'بالاوي', NULL, NULL, NULL, NULL),
(178, 'ريونيون', 'Réunio', 'ريونيوني', NULL, NULL, NULL, NULL),
(179, 'رومانيا', 'Romania', 'روماني', NULL, NULL, NULL, NULL),
(180, 'صربيا', 'Serbia', 'صربي', NULL, NULL, NULL, NULL),
(181, 'الاتحاد الروسي', 'Russia', 'روسي', NULL, NULL, NULL, NULL),
(182, 'رواندا', 'Rwanda', 'رواندي', NULL, NULL, NULL, NULL),
(183, 'جزر سليمان', 'Solomon Islands', 'جزر سليمان', NULL, NULL, NULL, NULL),
(184, 'سيشيل', 'Seychelles', 'سيشيلي', NULL, NULL, NULL, NULL),
(185, 'السويد', 'Swede', 'سويدي', NULL, NULL, NULL, NULL),
(186, 'سنغافورة', 'Singapore', 'سنغافوري', NULL, NULL, NULL, NULL),
(187, 'سانت هيلانة وأسنسيون وتريستان دا كونها', 'Saint Helena', 'سانت هيلانة وأسنسيون وتريستان دا كونها', NULL, NULL, NULL, NULL),
(188, 'سلوفينيا', 'Slovenia', 'سلوفيني', NULL, NULL, NULL, NULL),
(189, 'سلوفاكيا (جمهورية سلوفاكيا),', 'Slovakia', 'سلوفاكي', NULL, NULL, NULL, NULL),
(190, 'سيراليون', 'Sierra Leone', 'سيراليوني', NULL, NULL, NULL, NULL),
(191, 'سان مارينو', 'San Marino', 'سان ماريني', NULL, NULL, NULL, NULL),
(192, 'السنغال', 'Senegal', 'سنغالي', NULL, NULL, NULL, NULL),
(193, 'سورينام', 'Suriname', 'سورينامي', NULL, NULL, NULL, NULL),
(194, 'ساو تومي وبرينسيبي', 'São Tomé and Príncipe', 'ساو تومي وبرينسيبي', NULL, NULL, NULL, NULL),
(195, 'السلفادور', 'El Salvador', 'سلفادوري', NULL, NULL, NULL, NULL),
(196, 'سوازيلاند', 'Switzerland', 'سوازيلاندي', NULL, NULL, NULL, NULL),
(197, 'تشاد', 'Chad', 'تشادي', NULL, NULL, NULL, NULL),
(198, 'توغو', 'To', 'توغولي', NULL, NULL, NULL, NULL),
(199, 'تايلاند', 'Thailand', 'تايلاندي', NULL, NULL, NULL, NULL),
(200, 'طاجيكستان', 'Tajikista', 'طاجيكستاني', NULL, NULL, NULL, NULL),
(201, 'تيمور الشرقية', 'Timor-Leste', 'تيموري', NULL, NULL, NULL, NULL),
(202, 'تركمانستان', 'Turkmenista', 'تركمانستاني', NULL, NULL, NULL, NULL),
(203, 'تونغا', 'Tonga', 'تونغي', NULL, NULL, NULL, NULL),
(204, 'تركيا', 'Turkey', 'تركي', NULL, NULL, NULL, NULL),
(205, 'ترينيداد وتوباغو', 'Trinidad and Toba', 'ترينيدادي', NULL, NULL, NULL, NULL),
(206, 'توفالو', 'Tuvalu', 'توفالي', NULL, NULL, NULL, NULL),
(207, 'تايوان', 'Taiwa', 'تايواني', NULL, NULL, NULL, NULL),
(208, 'تنزانيا', 'Tanzania', 'تنزاني', NULL, NULL, NULL, NULL),
(209, 'الولايات المتحدة', 'United States', 'أمريكي', NULL, NULL, NULL, NULL),
(210, 'أوروغواي', 'Uruguay', 'أوروغواياني', NULL, NULL, NULL, NULL),
(211, 'أوزبكستان', 'Uzbekista', 'أوزبكستاني', NULL, NULL, NULL, NULL),
(212, 'الفاتيكان', NULL, 'فاتيكاني', NULL, NULL, NULL, NULL),
(213, 'سانت فنسنت وجزر غرينادين', 'Saint Vincent and the Grenadines', 'سانت فنسنت وجزر غرينادين', NULL, NULL, NULL, NULL),
(214, 'فنزويلا', 'Venezuela', 'فنزويلي', NULL, NULL, NULL, NULL),
(215, 'جزر فيرجن البريطانية', 'UK Virgin Islands', 'جزر فيرجن البريطانية', NULL, NULL, NULL, NULL),
(216, 'جزر فرجن الأمريكية', 'US Virgin Islands', 'جزر فرجن الأمريكية', NULL, NULL, NULL, NULL),
(217, 'فيتنام', 'Vietnam', 'فيتنامي', NULL, NULL, NULL, NULL),
(218, 'فانواتو', 'Vanuatu', 'فانواتي', NULL, NULL, NULL, NULL),
(219, 'ساموا', 'Samoa', 'سامي', NULL, NULL, NULL, NULL),
(220, 'جنوب أفريقيا', 'South Africa', 'جنوبي أفريقي', NULL, NULL, NULL, NULL),
(221, 'زامبيا', 'Zambia', 'زامبي', NULL, NULL, NULL, NULL),
(222, 'زيمبابوي', 'Zimbabwe', 'زيمبابوياني', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `full_name` varchar(300) NOT NULL,
  `id_no` int(11) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `gender_id` int(11) NOT NULL,
  `birth_date` date NOT NULL,
  `id_image` varchar(300) DEFAULT NULL,
  `image` varchar(300) DEFAULT NULL,
  `status_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1- مفعل / 2- موقوف',
  `register_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `full_name`, `id_no`, `mobile`, `email`, `country_id`, `city_id`, `gender_id`, `birth_date`, `id_image`, `image`, `status_id`, `register_date`, `created_at`, `updated_at`) VALUES
(1, 'محمد على محمد', 123456789, '05995522661', 'ahmed@zedny.com', 1, 2026, 1, '1999-01-01', '/images/_1541840594722.jpg', '', 1, '2019-01-01', '2019-02-11 19:19:46', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `customer_codes`
--

CREATE TABLE `customer_codes` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `code_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_codes`
--

INSERT INTO `customer_codes` (`id`, `customer_id`, `code_id`, `order_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2019-02-18 04:34:42', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `customer_evaluations`
--

CREATE TABLE `customer_evaluations` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `notes` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_evaluations`
--

INSERT INTO `customer_evaluations` (`id`, `customer_id`, `order_id`, `provider_id`, `points`, `notes`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 1, 2, '', '2019-02-11 19:46:06', '0000-00-00 00:00:00'),
(2, 1, 0, 1, 5, NULL, '2019-02-11 19:46:49', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `customer_payments`
--

CREATE TABLE `customer_payments` (
  `id` int(11) NOT NULL,
  `payment_way_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `amount` float NOT NULL,
  `bill` varchar(300) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_payments`
--

INSERT INTO `customer_payments` (`id`, `payment_way_id`, `customer_id`, `date`, `amount`, `bill`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2019-02-14', 100, '/images/_1546517005995.jpg', '2019-02-18 10:43:49', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `deliver_pricing`
--

CREATE TABLE `deliver_pricing` (
  `id` int(11) NOT NULL,
  `dis_from` float NOT NULL,
  `dis_to` float NOT NULL,
  `price` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `deliver_pricing`
--

INSERT INTO `deliver_pricing` (`id`, `dis_from`, `dis_to`, `price`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 9, '2019-02-17 03:39:06', '2019-02-17 01:39:06'),
(2, 2.1, 2.5, 5, '2019-02-13 02:34:37', '2019-02-13 02:34:37'),
(3, 3, 5, 4, '2019-02-13 02:50:05', '2019-02-13 02:50:05'),
(4, 6, 7, 8, '2019-02-13 02:51:48', '2019-02-13 02:51:48'),
(5, 7.1, 8.1, 5, '2019-02-13 15:24:48', '2019-02-13 15:24:48'),
(6, 9, 11, 55, '2019-02-17 01:28:35', '2019-02-17 01:28:35'),
(7, 11.1, 12, 12, '2019-02-18 01:05:23', '2019-02-18 01:05:23'),
(8, 12.1, 12.5, 10, '2019-02-18 01:05:34', '2019-02-18 01:05:34');

-- --------------------------------------------------------

--
-- Table structure for table `financial`
--

CREATE TABLE `financial` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `payment_way_id` int(11) DEFAULT NULL,
  `status_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `date` date NOT NULL,
  `bill` varchar(300) NOT NULL,
  `order_payment_way_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `financial`
--

INSERT INTO `financial` (`id`, `order_id`, `provider_id`, `payment_way_id`, `status_id`, `price`, `date`, `bill`, `order_payment_way_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, 1, 100.5, '2019-02-14', '', 1, '2019-02-18 12:10:35', '0000-00-00 00:00:00'),
(2, 1, 5, 3, 2, 45, '2019-02-14', '/images/_1546517005995.jpg', 1, '2019-02-18 12:10:38', '0000-00-00 00:00:00'),
(3, 2, 1, NULL, 1, 50, '2019-02-19', '', 1, '2019-02-18 12:11:19', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `item_order` int(11) NOT NULL,
  `image` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `item_order`, `image`, `created_at`, `updated_at`) VALUES
(1, 'حلويات 1', 1, '/images/_1546516101147.jpg', '2019-01-03 11:54:26', '2019-01-03 09:54:26'),
(3, 'حلويات2', 2, '/images/_1546517005995.jpg', '2019-01-03 12:03:26', '2019-01-03 10:03:26');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `notify_text` text NOT NULL,
  `admin_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `target` int(11) NOT NULL DEFAULT '0',
  `customer_id` int(11) DEFAULT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `notify_text`, `admin_id`, `date`, `target`, `customer_id`, `provider_id`, `created_at`, `updated_at`) VALUES
(4, 'jkm', 1, '2018-11-09', 0, NULL, NULL, '2018-11-09 13:41:18', '2018-11-09 13:41:18'),
(5, 'jj', 1, '2018-11-09', 0, NULL, NULL, '2018-11-09 13:43:02', '2018-11-09 13:43:02'),
(6, 'jkm', 1, '2018-11-09', 0, NULL, NULL, '2018-11-09 13:43:07', '2018-11-09 13:43:07'),
(7, 'jkm', 1, '2018-11-09', 0, NULL, NULL, '2018-11-09 13:45:42', '2018-11-09 13:45:42'),
(8, 'jkm', 1, '2018-11-09', 0, NULL, NULL, '2018-11-09 13:46:56', '2018-11-09 13:46:56'),
(9, 'jkm', 1, '2018-11-09', 0, NULL, NULL, '2018-11-09 13:47:05', '2018-11-09 13:47:05'),
(10, 'testtt', 1, '2018-11-09', 0, NULL, NULL, '2018-11-09 13:48:01', '2018-11-09 13:48:01'),
(11, 'testtt', 1, '2018-11-09', 0, NULL, NULL, '2018-11-09 13:48:14', '2018-11-09 13:48:14'),
(12, 'jj', 1, '2018-11-09', 0, NULL, NULL, '2018-11-09 14:03:09', '2018-11-09 14:03:09'),
(13, 'yyy', 1, '2018-11-09', 0, NULL, NULL, '2018-11-09 14:03:40', '2018-11-09 14:03:40'),
(14, 'rere', 1, '2019-02-14', 0, NULL, NULL, '2019-02-14 02:28:29', '2019-02-14 02:28:29'),
(15, 'jjj', 1, '2019-02-19', 0, NULL, NULL, '2019-02-19 02:20:41', '2019-02-19 02:20:41'),
(16, 'gg', 1, '2019-02-19', 0, NULL, NULL, '2019-02-19 02:26:01', '2019-02-19 02:26:01'),
(17, 'ggg', 1, '2019-02-19', 0, NULL, NULL, '2019-02-19 02:35:32', '2019-02-19 02:35:32'),
(18, 'gggg', 1, '2019-02-19', 4, NULL, 3, '2019-02-19 02:37:28', '2019-02-19 02:37:28'),
(19, 'ggg', 1, '2019-02-19', 1, NULL, NULL, '2019-02-19 02:43:15', '2019-02-19 02:43:15'),
(20, 'oo', 1, '2019-02-19', 1, NULL, NULL, '2019-02-19 02:44:08', '2019-02-19 02:44:08'),
(21, 'dd', 1, '2019-02-19', 2, NULL, NULL, '2019-02-19 02:54:10', '2019-02-19 02:54:10'),
(22, 'dd', 1, '2019-02-19', 2, NULL, NULL, '2019-02-19 02:54:11', '2019-02-19 02:54:11'),
(23, 'ggg', 1, '2019-02-19', 1, NULL, NULL, '2019-02-19 02:54:36', '2019-02-19 02:54:36'),
(24, 'ggg', 1, '2019-02-19', 2, NULL, NULL, '2019-02-19 02:54:36', '2019-02-19 02:54:36'),
(25, 'ggg', 1, '2019-02-19', 3, NULL, NULL, '2019-02-19 02:54:36', '2019-02-19 02:54:36'),
(26, 'ff', 1, '2019-02-19', 4, 1, NULL, '2019-02-19 02:55:05', '2019-02-19 02:55:05');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `delegate_id` int(11) DEFAULT NULL,
  `order_date` date NOT NULL,
  `order_accept_time` datetime NOT NULL,
  `order_type` int(11) NOT NULL,
  `arraival_time` datetime NOT NULL,
  `order_status` int(11) NOT NULL,
  `payment_way_id` int(11) NOT NULL,
  `total_amount` float NOT NULL,
  `added_value` float NOT NULL,
  `notes` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `customer_id`, `provider_id`, `delegate_id`, `order_date`, `order_accept_time`, `order_type`, `arraival_time`, `order_status`, `payment_way_id`, `total_amount`, `added_value`, `notes`, `created_at`, `updated_at`) VALUES
(1, 123258, 1, 1, 5, '2019-02-13', '2019-02-15 06:30:00', 1, '2019-02-15 06:23:00', 1, 1, 500, 0, 'تم الطلب بواسطة محمد وسيتم تحضيره بأقرب وقت ', '2019-02-17 18:47:59', '0000-00-00 00:00:00'),
(2, 344, 1, 1, NULL, '2019-02-18', '2019-02-18 06:33:33', 2, '2019-02-19 06:00:00', 2, 2, 300, 10, 'تفاصيل المنتج\r\nدجاج بالمشرم وصوص الصويا', '2019-02-18 09:16:18', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `providers`
--

CREATE TABLE `providers` (
  `id` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(300) NOT NULL,
  `status_id` int(11) NOT NULL,
  `address` varchar(300) NOT NULL,
  `bank_account` varchar(30) NOT NULL,
  `image` varchar(50) NOT NULL,
  `loc_lat` float NOT NULL,
  `loc_long` float NOT NULL,
  `country_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `providers`
--

INSERT INTO `providers` (`id`, `name`, `mobile`, `phone`, `email`, `password`, `status_id`, `address`, `bank_account`, `image`, `loc_lat`, `loc_long`, `country_id`, `city_id`, `type`, `created_at`, `updated_at`) VALUES
(1, 'مزود 1', '059955226', '55555555', 'info@dd.ps', '$2y$10$FQtb3D/vi1ML/Gzt9F6SkujxQFvbHP13eMcbSIoY0FPxIbiYWFxoK', 2, 'ببيبي', '123456789', '', 24.4978, 39.4554, 1, 1, 1, '2019-02-14 16:34:49', '2019-02-14 14:34:49'),
(3, 'مزود2', '059955226', '4444', 'info@ddd.ps', '', 2, 'vfff', '234122', '', 24.4978, 39.4554, 1, 2, 1, '2019-02-15 19:41:15', '0000-00-00 00:00:00'),
(5, 'مندوب2', '05995522663', '455666', 'ff@dd.ps', '', 2, 'fdf', '2115', '', 24.4978, 39.4554, 1, 1, 2, '2019-02-15 19:42:55', '2019-02-15 17:42:55');

-- --------------------------------------------------------

--
-- Table structure for table `provider_evaluations`
--

CREATE TABLE `provider_evaluations` (
  `id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `notes` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `provider_evaluations`
--

INSERT INTO `provider_evaluations` (`id`, `provider_id`, `order_id`, `customer_id`, `points`, `notes`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 1, 2, '', '2019-02-13 18:08:01', '0000-00-00 00:00:00'),
(2, 3, NULL, 1, 3, '', '2019-02-14 03:33:22', '0000-00-00 00:00:00'),
(3, 5, 1, 1, 3, '', '2019-02-14 03:43:02', '0000-00-00 00:00:00'),
(4, 5, 1, 1, 5, '', '2019-02-14 03:43:07', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `provider_files`
--

CREATE TABLE `provider_files` (
  `id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `image` varchar(300) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `provider_files`
--

INSERT INTO `provider_files` (`id`, `provider_id`, `image`, `created_at`, `updated_at`) VALUES
(1, 5, '/images/_1546517005995.jpg', '2019-02-18 08:24:02', '0000-00-00 00:00:00'),
(4, 5, '/images/_1550490350607.jpg', '2019-02-18 09:45:50', '2019-02-18 09:45:50');

-- --------------------------------------------------------

--
-- Table structure for table `provider_payment_ways`
--

CREATE TABLE `provider_payment_ways` (
  `id` int(11) NOT NULL,
  `payment_way_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `provider_payment_ways`
--

INSERT INTO `provider_payment_ways` (`id`, `payment_way_id`, `provider_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2019-02-11 04:44:36', '0000-00-00 00:00:00'),
(10, 2, 1, '2019-02-17 13:35:35', '2019-02-17 13:35:35'),
(11, 1, 5, '2019-02-17 23:54:16', '2019-02-17 23:54:16');

-- --------------------------------------------------------

--
-- Table structure for table `provider_products`
--

CREATE TABLE `provider_products` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `image` varchar(300) NOT NULL,
  `item_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `quantity` int(11) NOT NULL,
  `notes` text NOT NULL,
  `delivered_date` datetime NOT NULL,
  `provider_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `provider_products`
--

INSERT INTO `provider_products` (`id`, `name`, `image`, `item_id`, `price`, `quantity`, `notes`, `delivered_date`, `provider_id`, `created_at`, `updated_at`) VALUES
(1, 'منتج 1', '/images/52602698_10156883647048815_7861169269973712896_n.jpg', 1, 50, 5, 'منتج 1 منتج 1', '2019-02-11 00:00:00', 1, '2019-02-17 19:48:34', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `general_percentage` int(11) NOT NULL,
  `max_distance` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `general_percentage`, `max_distance`, `created_at`, `updated_at`) VALUES
(1, 30, 80, '2019-02-13 03:55:16', '2019-02-13 01:55:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advertisments`
--
ALTER TABLE `advertisments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_id_fk` (`CountryId`);

--
-- Indexes for table `codes`
--
ALTER TABLE `codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `confirm_order`
--
ALTER TABLE `confirm_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `constant_table`
--
ALTER TABLE `constant_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`CountryId`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_codes`
--
ALTER TABLE `customer_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_evaluations`
--
ALTER TABLE `customer_evaluations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id_fk` (`customer_id`);

--
-- Indexes for table `customer_payments`
--
ALTER TABLE `customer_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deliver_pricing`
--
ALTER TABLE `deliver_pricing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `financial`
--
ALTER TABLE `financial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item_order` (`item_order`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications$admin_id_fk` (`admin_id`),
  ADD KEY `notifications$customer_id_fk` (`customer_id`),
  ADD KEY `notifications$provider_id_fk` (`provider_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `order_id` (`order_id`),
  ADD KEY `order_customer_fk` (`customer_id`),
  ADD KEY `order_provider_fk` (`provider_id`),
  ADD KEY `order_sataus_id` (`order_status`),
  ADD KEY `delegate_id_fk` (`delegate_id`);

--
-- Indexes for table `providers`
--
ALTER TABLE `providers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `city_id_fk` (`city_id`);

--
-- Indexes for table `provider_evaluations`
--
ALTER TABLE `provider_evaluations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id_fk` (`order_id`);

--
-- Indexes for table `provider_files`
--
ALTER TABLE `provider_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provider_payment_ways`
--
ALTER TABLE `provider_payment_ways`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provider_products`
--
ALTER TABLE `provider_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provider_id_fk` (`provider_id`),
  ADD KEY `item_id_fk` (`item_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `advertisments`
--
ALTER TABLE `advertisments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `codes`
--
ALTER TABLE `codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `confirm_order`
--
ALTER TABLE `confirm_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `constant_table`
--
ALTER TABLE `constant_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `CountryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=223;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customer_codes`
--
ALTER TABLE `customer_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customer_evaluations`
--
ALTER TABLE `customer_evaluations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customer_payments`
--
ALTER TABLE `customer_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `deliver_pricing`
--
ALTER TABLE `deliver_pricing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `financial`
--
ALTER TABLE `financial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `providers`
--
ALTER TABLE `providers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `provider_evaluations`
--
ALTER TABLE `provider_evaluations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `provider_files`
--
ALTER TABLE `provider_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `provider_payment_ways`
--
ALTER TABLE `provider_payment_ways`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `provider_products`
--
ALTER TABLE `provider_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `country_id_fk` FOREIGN KEY (`CountryId`) REFERENCES `country` (`CountryId`);

--
-- Constraints for table `customer_evaluations`
--
ALTER TABLE `customer_evaluations`
  ADD CONSTRAINT `customer_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications$admin_id_fk` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`),
  ADD CONSTRAINT `notifications$customer_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `notifications$provider_id_fk` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `delegate_id_fk` FOREIGN KEY (`delegate_id`) REFERENCES `providers` (`id`),
  ADD CONSTRAINT `order_customer_fk` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `order_provider_fk` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`),
  ADD CONSTRAINT `order_sataus_id` FOREIGN KEY (`order_status`) REFERENCES `constant_table` (`id`);

--
-- Constraints for table `provider_evaluations`
--
ALTER TABLE `provider_evaluations`
  ADD CONSTRAINT `order_id_fk` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);

--
-- Constraints for table `provider_products`
--
ALTER TABLE `provider_products`
  ADD CONSTRAINT `item_id_fk` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `provider_id_fk` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
