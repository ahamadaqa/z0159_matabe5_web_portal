ALTER TABLE `orders` ADD CONSTRAINT `order_customer_fk` FOREIGN KEY (`customer_id`) REFERENCES `customers`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `orders` ADD CONSTRAINT `order_provider_fk` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
ALTER TABLE `orders` ADD CONSTRAINT `order_delegate_fk` FOREIGN KEY (`delegate_id`) REFERENCES `delegates`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `customers` ADD `status_id` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '1- مفعل' AFTER `image`;
ALTER TABLE `customers` CHANGE `status_id` `status_id` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '1- مفعل / 2- موقوف';
ALTER TABLE `customers` ADD `register_date` DATE NOT NULL AFTER `status_id`;