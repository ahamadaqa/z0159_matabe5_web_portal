ALTER TABLE `notifications` ADD `target` INT(11) NOT NULL AFTER `date`;
ALTER TABLE `notifications` ADD `user_id` INT(11) NULL AFTER `target`;
ALTER TABLE `notifications` ADD CONSTRAINT `notifications$user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `admins`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
