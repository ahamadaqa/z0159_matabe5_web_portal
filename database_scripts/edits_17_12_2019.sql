ALTER TABLE `provider_products` ADD `active` TINYINT(1) NOT NULL AFTER `provider_id`;
ALTER TABLE `provider_products` CHANGE `active` `active` TINYINT(1) NOT NULL DEFAULT '1';
ALTER TABLE `provider_products` CHANGE `image` `image` VARCHAR(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `provider_products` CHANGE `notes` `notes` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
CHANGE `delivered_date` `delivered_date` DATETIME NULL;
CREATE TABLE `product_images` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(300) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `product_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `orders` CHANGE `provider_id` `provider_id` INT(11) NULL, CHANGE `order_accept_time` `order_accept_time` DATETIME NULL,
 CHANGE `arraival_time` `arraival_time` DATETIME NULL, CHANGE `payment_way_id` `payment_way_id` INT(11) NULL,
 CHANGE `total_amount` `total_amount` FLOAT NULL, CHANGE `added_value` `added_value` FLOAT NULL,
 CHANGE `notes` `notes` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `orders` CHANGE `price` `price` FLOAT NULL;
ALTER TABLE `orders` CHANGE `product_id` `product_id` INT(11) NULL;

ALTER TABLE `orders` ADD CONSTRAINT `order_type_fk` FOREIGN KEY (`order_type`) REFERENCES `constant_table`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `orders` ADD `item_id` INT(11) NULL AFTER `price`,
ADD `suggested_price` FLOAT NULL AFTER `item_id`, ADD `description` LONGTEXT NULL AFTER `suggested_price`;

INSERT INTO `constant_table` (`id`, `name`, `type`) VALUES ('11', 'تم القبول', '1');

CREATE TABLE `orders_provider_requests` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `notes` text NOT NULL,
  `status_id` tinyint(1) NOT NULL COMMENT '1=new, 2=accept, 3=refuse',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `orders_provider_requests`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `orders_provider_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;