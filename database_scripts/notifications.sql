-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 06, 2019 at 02:40 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `matabe5`
--

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `notify_text` text NOT NULL,
  `admin_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `notify_text`, `admin_id`, `date`, `created_at`, `updated_at`) VALUES
(4, 'jkm', 1, '2018-11-09', '2018-11-09 13:41:18', '2018-11-09 13:41:18'),
(5, 'jj', 1, '2018-11-09', '2018-11-09 13:43:02', '2018-11-09 13:43:02'),
(6, 'jkm', 1, '2018-11-09', '2018-11-09 13:43:07', '2018-11-09 13:43:07'),
(7, 'jkm', 1, '2018-11-09', '2018-11-09 13:45:42', '2018-11-09 13:45:42'),
(8, 'jkm', 1, '2018-11-09', '2018-11-09 13:46:56', '2018-11-09 13:46:56'),
(9, 'jkm', 1, '2018-11-09', '2018-11-09 13:47:05', '2018-11-09 13:47:05'),
(10, 'testtt', 1, '2018-11-09', '2018-11-09 13:48:01', '2018-11-09 13:48:01'),
(11, 'testtt', 1, '2018-11-09', '2018-11-09 13:48:14', '2018-11-09 13:48:14'),
(12, 'jj', 1, '2018-11-09', '2018-11-09 14:03:09', '2018-11-09 14:03:09'),
(13, 'yyy', 1, '2018-11-09', '2018-11-09 14:03:40', '2018-11-09 14:03:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications$admin_id_fk` (`admin_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications$admin_id_fk` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
