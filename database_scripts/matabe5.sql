-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 17, 2019 at 07:46 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `matabe5`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `is_super_admin` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `username`, `password`, `mobile`, `email`, `is_super_admin`, `created_at`, `updated_at`) VALUES
(1, 'مدير النظام', 'admin', '$2y$10$st5FYaf0ni8pqsFfPmh.4Oojqf2DAUPoSfPPxdgt4zDUUFRlgF.gy', '(059) 654-1155', NULL, 1, '2019-01-03 09:19:24', '2019-01-03 07:19:24'),
(6, 'amna', 'amna', '$2y$10$bQYmcLPf0PBXH9EXLnJ6BuHohFJ0A/2MnLPrcdpX7uJ0QoAN60u0K', '059', NULL, 0, '2018-12-26 17:22:11', '2018-12-26 17:22:11'),
(7, 'ahmed', 'ahmed', '$2y$10$Jpi38ukDd/vu83ZOg/E3wu4CrXsVm16ZcT3jYfHE6jAvu.Tp8cqeq', '05966447788', 'wael@speedclick.ps', 0, '2019-01-03 08:55:22', '2019-01-03 06:55:22');

-- --------------------------------------------------------

--
-- Table structure for table `constant_table`
--

CREATE TABLE `constant_table` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `constant_table`
--

INSERT INTO `constant_table` (`id`, `name`, `type`) VALUES
(1, 'مفعل', 1),
(2, 'موقوف', 1),
(3, 'تم حذفه', 1),
(4, 'جديد', 2),
(5, 'معلق', 2),
(6, 'مؤكد', 2),
(7, 'منتهى', 2),
(8, 'ملغي', 2),
(9, 'الأقدام', 3),
(10, 'الدراجة الهوائية', 3),
(11, 'الدراجة النارية', 3),
(12, 'السيارة', 3),
(13, 'الطائرة', 3),
(14, 'الباخرة', 3),
(15, 'القطار', 3),
(16, 'الترام', 3),
(17, 'الحافلة', 3);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `full_name` varchar(300) NOT NULL,
  `id_no` int(11) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `gender_id` int(11) NOT NULL,
  `birth_date` date NOT NULL,
  `id_image` varchar(300) DEFAULT NULL,
  `image` varchar(300) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `full_name`, `id_no`, `mobile`, `email`, `country_id`, `city_id`, `gender_id`, `birth_date`, `id_image`, `image`, `created_at`, `updated_at`) VALUES
(1, 'محمد على محمد', 123456789, '05995522661', 'ahmed@zedny.com', 1, 1, 1, '2019-01-01', '', '', '2019-01-10 04:24:02', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `delegates`
--

CREATE TABLE `delegates` (
  `id` int(11) NOT NULL,
  `name_en` varchar(300) NOT NULL,
  `name_ar` varchar(300) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(30) NOT NULL,
  `status_id` int(11) NOT NULL,
  `address` varchar(300) NOT NULL,
  `bank_account` varchar(30) NOT NULL,
  `image` varchar(50) NOT NULL,
  `loc_lat` float NOT NULL,
  `loc_long` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `item_order` int(11) NOT NULL,
  `image` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `item_order`, `image`, `created_at`, `updated_at`) VALUES
(1, 'حلويات 1', 1, '/images/_1546516101147.jpg', '2019-01-03 11:54:26', '2019-01-03 09:54:26'),
(3, 'حلويات2', 2, '/images/_1546517005995.jpg', '2019-01-03 12:03:26', '2019-01-03 10:03:26');

-- --------------------------------------------------------

--
-- Table structure for table `providers`
--

CREATE TABLE `providers` (
  `id` int(11) NOT NULL,
  `name_en` varchar(300) NOT NULL,
  `name_ar` varchar(300) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `status_id` int(11) NOT NULL,
  `address` varchar(300) NOT NULL,
  `bank_account` varchar(30) NOT NULL,
  `image` varchar(50) NOT NULL,
  `loc_lat` float NOT NULL,
  `loc_long` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `constant_table`
--
ALTER TABLE `constant_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delegates`
--
ALTER TABLE `delegates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item_order` (`item_order`);

--
-- Indexes for table `providers`
--
ALTER TABLE `providers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `constant_table`
--
ALTER TABLE `constant_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `delegates`
--
ALTER TABLE `delegates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `providers`
--
ALTER TABLE `providers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
