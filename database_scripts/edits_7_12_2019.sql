ALTER TABLE `providers` CHANGE `phone` `phone` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
 CHANGE `address` `address` VARCHAR(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
 CHANGE `bank_account` `bank_account` VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
 CHANGE `loc_lat` `loc_lat` FLOAT NULL, CHANGE `loc_long` `loc_long` FLOAT NULL;