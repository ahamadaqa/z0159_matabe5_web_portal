<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

//Route::get('/', function () {
//    return view('welcome');
//});
Route::group(['prefix' => 'api'], function () {
    Route::get('advertisments', 'AdvertismentsController@getAdvertisments');
    Route::post('categoryProducts/{id}', 'ProductsController@categoryProducts');
    Route::post('providerProducts/{id}', 'ProductsController@providerProducts');
    Route::get('getCities', 'SettingsController@getCities');
    Route::get('getCountries', 'SettingsController@getCountries');
    Route::get('getPercentage/{id}', 'SettingsController@getPercentage');
    Route::post('getDelivery/cost', 'SettingsController@getDeliveryCost');
    Route::get('getSetting', 'SettingsController@getSetting');
    Route::resource('Product', 'ProductsController');
    Route::resource('Order', 'OrdersController');
    Route::get('Providers/{id}/rates', 'ProvidersController@providerEvaluations');
    Route::get('Providers/{id}', 'ProvidersController@providersByCategory');

//    Route::post('provider/Login', 'ProviderApiController@login');
//    Route::post('delegate/Login', 'DelegateApiController@login');
    Route::post('Login', 'CustomerApiController@login');
    
    Route::post('delegate/register', 'DelegateApiController@addDelegate');
    Route::post('provider/register', 'ProviderApiController@addProvider');
    Route::post('customer/register', 'CustomerApiController@addCustomer');


    Route::group(['middleware' => ['jwt.auth', 'auth']], function() {
        Route::post('Order', 'CustomerApiController@addOrder');
        Route::post('specialOrder', 'CustomerApiController@specialOrder');
        Route::post('customer/myOrders', 'CustomerApiController@myOrders');
        Route::put('Order/cancel', 'CustomerApiController@cancelOrder');
        Route::put('order/confirmReceived', 'CustomerApiController@confirmReceived');
        Route::post('customer/evaluateProvider', 'CustomerApiController@evaluateProvider');
        Route::get('customer/profile', 'CustomerApiController@profile');
        Route::post('customer/joinToProviders', 'CustomerApiController@joinToProviders');
        Route::post('customer/joinToDelegates', 'CustomerApiController@joinToDelegates');
        Route::post('specialOrder', 'CustomerApiController@specialOrder');
        Route::get('getProvidersList/{id}', 'CustomerApiController@getProvidersList');
        Route::put('acceptProvider/{id}', 'CustomerApiController@acceptProvider');
        
        Route::get('provider/myOrders', 'ProviderApiController@myOrders');
        Route::put('provider/acceptCancelOrder', 'ProviderApiController@acceptCancelOrder');
        Route::put('provider/finishOrder', 'ProviderApiController@finishOrder');
        Route::post('provider/chooseDelegate', 'ProviderApiController@chooseDelegate');
        Route::post('provider/evaluateCustomer', 'ProviderApiController@evaluateCustomer');
        Route::get('provider/notifications', 'ProviderApiController@notifications');
        Route::get('provider/financial', 'ProviderApiController@financial');
        Route::put('provider/myAccount', 'ProviderApiController@myAccount');
        Route::put('provider/myItems', 'ProviderApiController@myItems');
        Route::get('provider/profile', 'ProviderApiController@profile');
        Route::get('provider/myProducts', 'ProviderApiController@myProducts');
        Route::get('provider/product/{id}', 'ProviderApiController@viewProduct');
        Route::post('provider/product/{id}', 'ProviderApiController@editProduct');
        Route::put('provider/product/{id}/stop', 'ProviderApiController@stopProduct');
        Route::post('provider/product', 'ProviderApiController@addProduct');
        Route::post('provider/applayToOrder', 'ProviderApiController@applayToOrder');
        Route::put('provider/startOrder/{id}', 'ProviderApiController@startOrder');
        Route::put('provider/deliveredOrder/{id}', 'ProviderApiController@deliveredOrder');
        
        
        Route::get('delegate/myOrders', 'DelegateApiController@myOrders');
        Route::put('delegate/acceptOrCancelOrder', 'DelegateApiController@acceptCancelOrder');
        Route::put('delegate/deliverOrder', 'DelegateApiController@deliverOrder');
        Route::post('delegate/evaluateCustomer', 'DelegateApiController@evaluateCustomer');
        Route::get('delegate/notifications', 'DelegateApiController@notifications');
        Route::get('delegate/financial', 'DelegateApiController@financial');
        Route::put('delegate/myAccount', 'DelegateApiController@myAccount');
        Route::get('delegate/profile', 'DelegateApiController@profile');
        
    });
});


Route::post('authenticate', 'AuthenticateController@authenticate');
Route::get('user', 'AuthenticateController@getAuthenticatedUser');
Route::post('logout', 'AuthenticateController@logout');

Route::group(['middleware' => ['jwt.auth', 'auth']], function() {
    Route::resource('Dashboard', 'DashboardController');
    Route::resource('Admins', 'AdminsController');
    Route::resource('Orders', 'OrdersController');
    Route::get('Items/getOptions', 'ItemsController@getOptions');
    Route::resource('Items', 'ItemsController');
    Route::resource('Carriers', 'CarriersController');
    Route::resource('Customers', 'CustomersController');

    Route::post('Providers/addFile', 'ProvidersController@addFile');
    Route::delete('Providers/deleteFile/{id}', 'ProvidersController@deleteFile');
    Route::get('Providers/viewProduct/{id}', 'ProvidersController@viewProduct');
    Route::delete('Providers/delete_payment_way/{id}', 'ProvidersController@delete_payment_way');
    Route::get('Providers/getPaymentsWays/{id}', 'ProvidersController@getPaymentsWays');
    Route::post('Providers/add_payment_way', 'ProvidersController@add_payment_way');
    Route::get('Providers/getOptions', 'ProvidersController@getOptions');
    Route::resource('Providers', 'ProvidersController');
    Route::resource('Delegates', 'DelegatesController');
    Route::put('ConfirmOrders/accept/{id}', 'ConfirmOrdersController@accept');
    Route::put('ConfirmOrders/refuse', 'ConfirmOrdersController@refuse');
    Route::resource('ConfirmOrders', 'ConfirmOrdersController');

    Route::delete('Settings/deleteCode/{id}', 'SettingsController@deleteCode');
    Route::get('Settings/codeViewCustomer/{id}', 'SettingsController@codeViewCustomer');
    Route::post('Settings/createCode', 'SettingsController@createCode');
    Route::put('Settings/edit_city/{id}', 'SettingsController@edit_city');
    Route::get('Settings/getCity/{id}', 'SettingsController@getCity');
    Route::post('Settings/createCity', 'SettingsController@createCity');
    Route::put('Settings/editPrice/{id}', 'SettingsController@editPrice');
    Route::resource('Settings', 'SettingsController');
    Route::resource('Financial', 'FinancialController');

    Route::resource('Advertisments', 'AdvertismentsController');

    Route::get('Notifications/getSelectOptions', 'NotificationsController@selectOptions');
    Route::resource('Notifications', 'NotificationsController');
    Route::get('getAdvertisments', 'AdvertismentsController@getAdvertisments');
    Route::get('carriers/trips/{id}', 'CarriersController@getTrip');
    Route::post('carriers/trips/participate', 'CarriersController@shareTrip');
    Route::get('customers/trips/{id}', 'CustomersController@getTrip');
    Route::post('customers/trips/participate', 'CustomersController@shareTrip');
});




